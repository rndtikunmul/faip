
<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?=$save_url?>" method="post" id="form_data" >
                    <div class="kt-portlet__body">
                        <input type="hidden" name="fptIdOld" value="<?=$datas!=false?$datas->aktivitasId:''?>">
                        <input type="hidden" class="form-control" name="aktivitasFdpId" placeholder="fopFdpId" aria-describedby="aktivitasFdpId" value="<?=$datas!=false?$datas->aktivitasFdpId:$fdpNoKta?>">

                        <div class="form-group">
                            <label>Nama Pelatihan</label>
                            <input type="text" class="form-control" name="aktivitasNama" placeholder="Nama Pelatihan" aria-describedby="aktivitasNama" value="<?=$datas!=false?$datas->aktivitasNama:''?>">
                        </div>
                        <div class="form-group">
                            <label>Lembaga Pelatihan</label>
                            <input type="text" class="form-control" name="aktivitasLembaga" placeholder="Lembaga Pelatihan" aria-describedby="aktivitasNama" value="<?=$datas!=false?$datas->aktivitasLembaga:''?>">
                        </div>

                        <div class="form-group">
                            <label>Kota Pelatihan</label>
                            <select class="form-control m-select2" name="aktivitasKota">
                                <option value=""></option>
                                <?php 
                                foreach($ref_kota as $row):
                                    echo '<option value="'.$row->kotaKode.'" ' . ($datas != false ? $datas->aktivitasKota == $row->kotaKode ? 'selected' : '' : '') . '>'.$row->kotaNama.'</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Negara Pelatihan</label>
                            <select class="form-control m-select2" name="aktivitasNegara">
                                <option value=""></option>
                                <?php 
                                foreach($ref_negara as $row):
                                    echo '<option value="'.$row->negKode.'" ' . ($datas != false ? $datas->aktivitasNegara == $row->negKode ? 'selected' : '' : '') . '>'.$row->negNama.'</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                        <label>Tanggal Pelatihan Awal - Akhir</label>
                       <div class="input-group pull-right" id="kt_daterangepicker_4">
                                <input type="text" class="form-control" name="aktivitasPeriode"  placeholder="Tanggal Awal-Tanggal Akhir"  value="<?=$datas!=false?$datas->aktivitasPeriode:''?> / <?=$datas!=false?$datas->aktivitasPeriodeAkhir:''?> " />
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
                                </div>
                            </div>                            
                        </div>

                      
                        <div class="form-group">
                            <label>Jumlah Jam</label>
                            <input type="text" class="form-control" name="aktivitasJumlah" placeholder="Jumlah Jam" aria-describedby="aktivitasJumlah" value="<?=$datas!=false?$datas->aktivitasJumlah:''?>">

                        </div>
                        <div class="form-group">
                            <label>Tingkat Pelatihan</label>
                           <!--  <input type="text" class="form-control" name="aktivitasTahunAwal" placeholder="Tingkat Pelatihan" aria-describedby="aktivitasTahunAwal" value="<?=$datas!=false?$datas->aktivitasTahunAwal:''?>"> -->

                            <select class="form-control m-select2" name="aktivitasTahunAwal">
                                <option value=""></option>
                                <?php 
                                foreach($ref_lvlatih as $row):
                                    echo '<option value="'.$row->lvlatihId.'" ' . ($datas != false ? $datas->aktivitasTahunAwal == $row->lvlatihId ? 'selected' : '' : '') . '>'.$row->lvlatihNama.'</option>';
                                endforeach;
                                ?>
                            </select>


                        </div>
                        <div class="form-group">
                            <label>No Sertifikat</label>
                            <input type="text" class="form-control" name="aktivitasTahunAkhir" placeholder="No Sertifikat" aria-describedby="aktivitasTahunAkhir" value="<?=$datas!=false?$datas->aktivitasTahunAkhir:''?>">

                        </div>

                        <div class="form-group">
                            <label>Uraian Materi Pelatihan</label>
                            <textarea class="form-control" id="exampleTextarea" rows="3" name="aktivitasUraian" placeholder="Uraian  Materi Pelatihan" aria-describedby="aktivitasUraian" maxlength="2000" ><?=$datas!=false?$datas->aktivitasUraian:''?></textarea>
                        </div>

                        <div class="form-group">
                            <label>Berkas</label>
                            <input type="file" class="form-control form-control-lg form-control-solid" type="text" name="uploadfile" placeholder="Foto"  id="custom-file-input" />
                             <span class="text-danger">* Format PDF dan max 700Kb</span></label>
                        </div>

                         <div class="form-group">
                            <label>Uraian Kompetensi</label>

                            <table class="table table-hover">
                                <thead class="thead-light">
                                    <tr>
                                        <th>#</th>
                                        <th>Uraian Kompetensi</th>

                                    </tr>
                                </thead>
                                <tbody>
                                   <?php
                                   foreach ($ref_uraiankompetensi as $row2) {
                                    if (!empty($row2->kompetensiUraianKompetensiId))
                                        $cek = "checked";
                                    else
                                        $cek="";

                                    ?>
                                    <tr>
                                        <th scope="row"><input type="checkbox"class="checked" <?=$cek?> name="cekKompetensi[]" value="<?=$row2->uraianKompetensiId?>" /></th>
                                        <td><?=$row2->uraianKompetensiNama?>  <?=$row2->uraianKompetensiKet?></td>

                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->
