<?php $this->load->view('layouts/subheader'); ?>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title"> <?= strtoupper($page_judul) ?></h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <form class="kt-form" action="<?= $response_url ?>" method="post" id="form_show">

                        <?php
                        if ($susrSgroupNama != 'Mhs') {
                        ?>
                            <?php ?>
                            <div class="form-group">
                                <label>Insinyur</label>
                                <select class="form-control m-select2" name="fdpNoKta" id="pegmNIP">
                                    <option value="">Pilih</option>
                                    <?php
                                    if ($fdpNoKta != false) {
                                        foreach ($fdpNoKta as $row) {
                                            echo '<option value="' . $row->fdpNim . '">' . $row->fdpNim . ' - ' . $row->fdpNama.'</option>';
                                        }
                                    }
                                    ?>
                                </select>

                              
                            </div>
                        <?php
                        } else {
                        ?>
                            <div class="form-group">
                                <label>Insinyur</label>
                                <input type="text" readonly value="<?= $susrSgroupNama_ori ?>" class="form-control" name="fdpNoKta" />
                            </div>
                        <?php
                        }
                        ?>


                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" id="btn_save" class="btn btn-primary">Show</button>
                    </div>
                </div>
                </form>

            </div>
        </div>
    </div>
</div>
<div id="response"></div>