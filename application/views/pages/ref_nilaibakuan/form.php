            <!-- BEGIN: Subheader -->
            <?php $this->load->view('layouts/subheader'); ?>
            <!-- END: Subheader -->

            <!--Begin::Row-->
            <!-- begin:: Content -->
            <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div id="response"></div>
                        <!--begin::Portlet-->
                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        <?=strtoupper($page_judul)?>
                                    </h3>
                                </div>
                            </div>

                            <!--begin::Form-->
                            <form class="kt-form" action="<?=$save_url?>" method="post" id="form_form">
                                <div class="kt-portlet__body">
                                    <input type="hidden" name="nilaiIdOld" value="<?=$datas!=false?$datas->nilaiId:''?>">

                                <div class="form-group">
                                    <label>nilaiKode</label>
                                    <input type="text" class="form-control" name="nilaiKode" placeholder="nilaiKode" aria-describedby="nilaiKode" value="<?=$datas!=false?$datas->nilaiKode:''?>">
                                </div>

                                <div class="form-group">
                                    <label>nilaiNama</label>
                                    <input type="text" class="form-control" name="nilaiNama" placeholder="nilaiNama" aria-describedby="nilaiNama" value="<?=$datas!=false?$datas->nilaiNama:''?>">
                                </div>
                            
                                <div class="form-group">
                                    <label>nilaiAngka</label>
                                    <input type="text" class="form-control" name="nilaiAngka" placeholder="nilaiAngka" aria-describedby="nilaiAngka" value="<?=$datas!=false?$datas->nilaiAngka:''?>">
                                </div>
                            
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                                        <button type="reset" class="btn btn-secondary">Cancel</button>
                                    </div>
                                </div>
                            </form>

                            <!--end::Form-->
                        </div>

                        <!--end::Portlet-->
                    </div>
                </div>
            </div>
            <!--End::Row-->