
<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?=$save_url?>" method="post" id="form_data">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="bimbinganIdOld" value="<?=$datas!=false?$datas->bimbinganId:''?>">

                        <div class="form-group">
                            <label>Insinyur</label>
                              <select class="form-control m-select2" name="bimbinganFdpId">
                                <option value=""></option>
                                <?php 
                                foreach($fdpNoKta as $row):
                                    echo '<option value="'.$row->fdpNim.'" ' . ($datas != false ? $datas->bimbinganFdpId == $row->fdpNim ? 'selected' : '' : '') . '>'. $row->fdpNim .' - '.$row->fdpNama.'</option>';
                                endforeach;

                                ?>
                            </select>
                    </div>

                    <div class="form-group">
                        <label>Dosen Pembimbing</label>
                        <!-- <input type="text" class="form-control" name="bimbinganNip" placeholder="bimbinganNip" aria-describedby="bimbinganNip" value="<?=$datas!=false?$datas->bimbinganNip:''?>"> -->
                         <select class="form-control m-select2" name="bimbinganNip">
                                <option value=""></option>
                                <?php 
                                foreach($pembimbing as $row):
                                    echo '<option value="'.$row->susrProfil.'" ' . ($datas != false ? $datas->bimbinganNip == $row->susrProfil ? 'selected' : '' : '') . '>'. $row->susrProfil .' - '.$row->susrNama.'</option>';
                                endforeach;

                                ?>
                                  </select>
                    </div>

                    <div class="form-group">
                        <label>Status</label>
                         <select class="form-control m-select2" name="bimbinganStatus">
                             <option value="">Pilih</option>
                             <option value="Pembimbing"<?=$datas==FALSE?'':($datas->bimbinganStatus=='Pembimbing'?'selected':'')?>>Pembimbing</option>
                             <option value="Penguji"<?=$datas==FALSE?'':($datas->bimbinganStatus=='Penguji'?'selected':'')?>>Penguji</option>
                             </select>
                    </div>

                    <div class="form-group">
                        <label>Keterangan</label>
                        <input type="text" class="form-control" name="bimbinganKet" placeholder="bimbinganKet" aria-describedby="bimbinganKet" value="<?=$datas!=false?$datas->bimbinganKet:''?>">
                    </div>

                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                        <button type="reset" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

        <!--end::Portlet-->
    </div>
</div>
</div>
<!--End::Row-->
