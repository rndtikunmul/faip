<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col-md-12">
			<div id="response"></div>
			<!--begin::Portlet-->
			<!--begin::Entry-->
			<div class="d-flex flex-column-fluid">
				<!--begin::Container-->
				<div class="container">
					<!--begin::Profile Personal Information-->
					<div class="d-flex flex-row">
						<!--begin::Aside-->
						<div class="flex-row-auto offcanvas-mobile w-250px w-xxl-350px" id="kt_profile_aside">
							<!--begin::Profile Card-->
							<div class="card card-custom card-stretch">
								<!--begin::Body-->
								<div class="card-body pt-4">
									<!--begin::Toolbar-->
									<div class="d-flex justify-content-end">
										<div class="dropdown dropdown-inline">
											<a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="ki ki-bold-more-hor"></i>
											</a>
										</div>
									</div>
									<!--end::Toolbar-->
									<!--begin::User-->
									<div class="d-flex align-items-center">
										<div>
											<img src="public/assets/berkas/<?=$datas!=false?$datas->fdpFoto:''?>" width="200px" height="240px" align="center" />
										</div>
									</div>
									<div>
											<a href="#" class="font-weight-bolder font-size-h5 text-dark-75 text-hover-primary"><?=$fdpNama?></a>
											<div class="text-muted"><?=$fdpNoKta?></div>
										</div>
									<!--end::User-->
									<!--begin::Contact-->
									<div class="py-9">
										<div class="d-flex align-items-center justify-content-between mb-2">
											<span class="font-weight-bold mr-2">Email:</span>
											<a href="#" class="text-muted text-hover-primary"><?=$datas!=false?$datas->fdpEmail:''?></a>
										</div>
										<div class="d-flex align-items-center justify-content-between mb-2">
											<span class="font-weight-bold mr-2">Phone:</span>
											<span class="text-muted"><?=$datas!=false?$datas->fdpTelpon:''?></span>
										</div>
									</div>
									<!--end::Contact-->
									<!--begin::Nav-->
									<div class="navi navi-bold navi-hover navi-active navi-link-rounded">

									</div>
									<!--end::Nav-->
								</div>
								<!--end::Body-->
							</div>
							<!--end::Profile Card-->
						</div>
						<!--end::Aside-->
						<!--begin::Content-->
						<div class="flex-row-fluid ml-lg-8">
							<!--begin::Card-->
							<div class="card card-custom card-stretch">
								<!--begin::Header-->
								<div class="card-header py-3">
									<div class="card-title align-items-start flex-column">
										<h3 class="card-label font-weight-bolder text-dark">Data Pribadi</h3>
										<span class="text-muted font-weight-bold font-size-sm mt-1">FORMULIR APLIKASI INSINYUR PROFESIONAL</span>
									</div>
									
								</div>
								<!--end::Header-->
								<!--begin::Form-->
								<form class="form" action="<?=$save_url?>" method="post" id="form_data" >
									<input type="hidden" name="fdpIdOld" value="<?=$datas!=false?$datas->fdpNim:''?>">
									<!--begin::Body-->
									<div class="card-body">
										<div class="row">
											<label class="col-xl-3"></label>
											<div class="col-lg-9 col-xl-6">
												<h5 class="font-weight-bold mb-6">Informasi Data Pribadi</h5>
											</div>
										</div>
										
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Nama</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpNama" placeholder="Nama" value="<?=$datas!=false?$datas->fdpNama:$fdpNama?>" />
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Nim</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpNim" placeholder="Nim" value="<?=$datas!=false?$datas->fdpNim:$fdpNoKta?>" />
											</div>
										</div>

										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">No KTA</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpNoKta" placeholder="No KTA" value="<?=$datas!=false?$datas->fdpNoKta:''?>" />
												<label><span class="text-danger">* HANYA ANGKA TANPA TANDA BACA APAPUN</span></label>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Tempat Lahir</label>
											<div class="col-lg-9 col-xl-6">
												<select class="form-control m-select2" name="fdpTempatLahir">
													<option value=""></option>
													<?php 
													foreach($ref_kota as $row):
														echo '<option value="'.$row->kotaKode.'" ' . ($datas != false ? $datas->fdpTempatLahir == $row->kotaKode ? 'selected' : '' : '') . '>'.$row->kotaNama.'</option>';
													endforeach;
													?>
												</select>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Tanggal Lahir</label>

											<div class="col-lg-6 col-xl-6" >
												<input type="text" class="form-control m-input" placeholder="Tanggal Lahir" id="m_datetimepicker_2" name="fdpTanggalLahir"   value="<?=$datas!=false?$datas->fdpTanggalLahir:''?>"/>
												
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Jenis Kelamin</label>
											<div class="col-lg-9 col-xl-6">
												 <select class="form-control m-select2" name="fdpJenKel">
					                                <option value=""></option>
					                                <option value="Male"<?=$datas==FALSE?'':($datas->fdpJenKel=='Male'?'selected':'')?>>Laki-Laki</option>
					                                <option value="Female"<?=$datas==FALSE?'':($datas->fdpJenKel=='Female'?'selected':'')?>>Perempuan</option>
                           					    </select>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Email</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpEmail" placeholder="Email" value="<?=$datas!=false?$datas->fdpEmail:''?>" />
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Periode Awal</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpTahunAwal" placeholder="Tahun Awal" value="<?=$datas!=false?$datas->fdpTahunAwal:''?>" />
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Periode Akhir</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpTahunAkhir" placeholder="Tahun Akhir" value="<?=$datas!=false?$datas->fdpTahunAkhir:''?>" />
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Faksimil Rumah</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpFaksimilrumah" placeholder="Faksimil Rumah" value="<?=$datas!=false?$datas->fdpFaksimilrumah:''?>" />
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Telex Rumah</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpTelexRumah" placeholder="TelexRumah" value="<?=$datas!=false?$datas->fdpTelexRumah:''?>" />
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Badan Kejuruan</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpBadanKejuruan" placeholder="Badan Kejuruan" value="<?=$datas!=false?$datas->fdpBadanKejuruan:''?>" />
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Tahun Lulus</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpTahunLulus" placeholder="Tahun Lulus" value="<?=$datas!=false?$datas->fdpTahunLulus:''?>" />
											</div>
										</div>

										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Insinyur Profesional PII</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpInsinyurProfesionalPII" placeholder="Insinyur" value="<?=$datas!=false?$datas->fdpInsinyurProfesionalPII:''?>" />
											</div>
										</div>

										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Alamat Rumah</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpAlamatRumah" placeholder="Alamat Rumah" value="<?=$datas!=false?$datas->fdpAlamatRumah:''?>" />
											</div>
										</div>

										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Kota Rumah</label>
											<div class="col-lg-9 col-xl-6">
												<select class="form-control m-select2" name="fdpKota">
													<option value=""></option>
													<?php 
													foreach($ref_kota as $row):
														echo '<option value="'.$row->kotaKode.'" ' . ($datas != false ? $datas->fdpKota == $row->kotaKode ? 'selected' : '' : '') . '>'.$row->kotaNama.'</option>';
													endforeach;
													?>
												</select>
											</div>
										</div>

										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Kode Pos Rumah</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpKodePos" placeholder="Kode Pos" value="<?=$datas!=false?$datas->fdpKodePos:''?>" />
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Telpon Rumah</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpTelpon" placeholder="Telpon" value="<?=$datas!=false?$datas->fdpTelpon:''?>" />
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Telpon Selular</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpNoSeluler" placeholder="Telpon Selular" value="<?=$datas!=false?$datas->fdpNoSeluler:''?>" />
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Nama Lembaga</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpNamaLembaga" placeholder="NamaLembaga" value="<?=$datas!=false?$datas->fdpNamaLembaga:''?>" />
											</div>
										</div>

										

										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Jabatan</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpJabatan" placeholder="Jabatan" value="<?=$datas!=false?$datas->fdpJabatan:''?>" />
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Jabatan Lain</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpJabatanLain" placeholder="Jabatan Lain" value="<?=$datas!=false?$datas->fdpJabatanLain:''?>" />
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Alamat Lembaga</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpAlamatLembaga" placeholder="Alamat Lembaga" value="<?=$datas!=false?$datas->fdpAlamatLembaga:''?>" />
											</div>
										</div>

										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Kota Lembaga</label>
											<div class="col-lg-9 col-xl-6">
												<select class="form-control m-select2" name="fdpKotaLembaga">
													<option value=""></option>
													<?php 
													foreach($ref_kota as $row):
														echo '<option value="'.$row->kotaKode.'" ' . ($datas != false ? $datas->fdpKotaLembaga == $row->kotaKode ? 'selected' : '' : '') . '>'.$row->kotaNama.'</option>';
													endforeach;
													?>
												</select>
											</div>
										</div>

										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">KodePos Lembaga</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpKodePosLembaga" placeholder="KodePos Lembaga" value="<?=$datas!=false?$datas->fdpKodePosLembaga:''?>" />
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Telpon Lembaga</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpTelponLembaga" placeholder="TelponLembaga" value="<?=$datas!=false?$datas->fdpTelponLembaga:''?>" />
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Faksimil Lembaga</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpFaksimilLembaga" placeholder="Faksimil Lembaga" value="<?=$datas!=false?$datas->fdpFaksimilLembaga:''?>" />
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Telex Lembaga</label>
											<div class="col-lg-9 col-xl-6">
												<input class="form-control form-control-lg form-control-solid" type="text" name="fdpTelexLembaga" placeholder="Faksimil Lembaga" value="<?=$datas!=false?$datas->fdpTelexLembaga:''?>" />
											</div>
										</div>
										<div class="form-group row">
											<label class="col-xl-3 col-lg-3 col-form-label text-right">Foto</label>
											<div class="col-lg-9 col-xl-6">
												<input type="file" class="form-control form-control-lg form-control-solid" name="uploadfile" placeholder="Foto"  id="custom-file-input" />
												<label><span class="text-danger">* Format .jpg dan max 700Kb</span></label>
												
											</div>
										</div>


										<div class="card-toolbar">
											<button  type="submit" id="btn_save" class="btn btn-success mr-2">Save Changes</button>
											<button type="reset" class="btn btn-secondary">Cancel</button>
										</div>
									</div>
									<!--end::Body-->
								</form>
								<!--end::Form-->
							</div>
						</div>
						<!--end::Content-->
					</div>
					<!--end::Profile Personal Information-->
				</div>
				<!--end::Container-->
			</div>
			<!--end::Entry-->
		</div>
		<!--end::Content-->

		<!--end::Page-->
	</div>
	<!--end::Portlet-->
</div>
</div>
</div>
<!--End::Row-->
