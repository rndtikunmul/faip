<?php $this->load->view('layouts/subheader'); ?>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title"> <?= strtoupper($page_judul) ?></h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-actions">
                            <a href="<?= $create_url?>" class="btn btn-outline-primary">
                                <span><i class="flaticon2-plus"></i><span>Create</span> </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <form class="kt-form" action="<?= $response_url ?>" method="post" id="form_show">

                        <?php
                        if ($susrSgroupNama != 'Mhs') {
                        ?>
                            <?php /**<div class="form-group">
                                <label>Unit</label>
                                <select class="form-control m-select2" name="unitKode" id="unitKode">
                                    <option value=""></option>
                                    <?php
                                    foreach ($unit as $row) {
                                        if (strlen($row->unitKode) == 2)
                                            echo '<optgroup label="' . $row->unitNama . '">';

                                        echo '<option value="' . $row->unitKode . '">' . $row->unitKode . ' - ' . $row->unitNama . ' (' . $row->unitParent . ')</option>';
                                    }
                                    ?>
                                </select>
                            </div>**/ ?>

                              <div class="form-group">
                                <label>Tahun Angkatan </label>
                               <select class="form-control m-select2" name="tahun" id="fdpNim">
                                <option value=""></option>
                                <?php for ($x = date('Y'); $x >= 2019; $x--) { ?>
                                    <option value="<?= $x ?>"><?= $x ?></option>
                                <?php } ?>
                            </select>
                            </div>

                             <div class="form-group">
                                <label>NIM</label>
                                <input type="text" value="" class="form-control" name="fdpNim" />
                            </div>
                            
                           <!--  <div class="form-group">
                                <label>Insinyur</label>
                                <select class="form-control m-select2" name="fdpNoKta" >
                                    <option value="">Pilih</option>
                                    <?php
                                    if ($fdatapribadi != false) {
                                        foreach ($fdatapribadi as $row) {
                                            echo '<option value="' . $row->fdpNim . '">' . $row->fdpNim . ' - ' . $row->fdpNama.'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            -->
                        <?php
                        } else {
                        ?>
                            <div class="form-group">
                                <label>Insinyur</label>
                                <input type="text" readonly value="<?= $susrSgroupNama_ori ?>" class="form-control" name="fdpNoKta" />
                            </div>
                        <?php
                        }
                        ?>


                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" id="btn_save" class="btn btn-primary">Show</button>
                    </div>
                </div>
                </form>

            </div>
        </div>
    </div>
</div>
<div id="response"></div>