
<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?=$save_url?>" method="post" id="form_data">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="fdpIdOld" value="<?=$datas!=false?$datas->fdpId:''?>">
                        <div class="form-group">
                            <label>Nim</label>
                            <input type="text" class="form-control" name="fdpNim" placeholder="Nim" aria-describedby="fdpNim" value="<?=$datas!=false?$datas->fdpNim:''?>">

                        </div>

                        <div class="form-group">
                            <label>NoKta</label>
                            <input type="text" class="form-control" name="fdpNoKta" placeholder="Nomor KTA" aria-describedby="fdpNoKta" value="<?=$datas!=false?$datas->fdpNoKta:''?>">
                        </div>

                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="fdpNama" placeholder="Nama" aria-describedby="fdpNama" value="<?=$datas!=false?$datas->fdpNama:''?>">
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" name="fdpEmail" placeholder="Email" aria-describedby="fdpEmail" value="<?=$datas!=false?$datas->fdpEmail:''?>">
                        </div>

                        <div class="form-group">
                            <label>Tempat Lahir</label>
                            <input type="text" class="form-control" name="fdpTempatLahir" placeholder="Tempat Lahir" aria-describedby="fdpTempatLahir" value="<?=$datas!=false?$datas->fdpTempatLahir:''?>">
                        </div>

                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <input type="text" class="form-control" name="fdpTanggalLahir" placeholder="Tanggal Lahir" id="m_datetimepicker_2" aria-describedby="fdpTanggalLahir" value="<?=$datas!=false?$datas->fdpTanggalLahir:''?>">
                        </div>

                         <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <select class="form-control m-select2" name="fdpJenKel">
                                <option value=""></option>
                                <option value="Male"<?=$datas==FALSE?'':($datas->fdpJenKel=='Male'?'selected':'')?>>Laki-Laki</option>
                                <option value="Female"<?=$datas==FALSE?'':($datas->fdpJenKel=='Female'?'selected':'')?>>Perempuan</option>
                            </select>
                        </div>

                      
                        <div class="form-group">
                            <label>Periode Tahun Awal</label>
                            <input type="text" class="form-control" name="fdpTahunAwal" placeholder="Tahun Awal" aria-describedby="fdpTahunAwal" value="<?=$datas!=false?$datas->fdpTahunAwal:''?>">
                        </div>

                        <div class="form-group">
                            <label>Periode Tahun Akhir</label>
                            <input type="text" class="form-control" name="fdpTahunAkhir" placeholder="Tahun Akhir" aria-describedby="fdpTahunAkhir" value="<?=$datas!=false?$datas->fdpTahunAkhir:''?>">
                        </div>

                        <div class="form-group">
                            <label>Badan Kejuruan</label>
                            <input type="text" class="form-control" name="fdpBadanKejuruan" placeholder="Badan Kejuruan" aria-describedby="fdpBadanKejuruan" value="<?=$datas!=false?$datas->fdpBadanKejuruan:''?>">
                        </div>

                        <div class="form-group">
                            <label>Tahun Lulus</label>
                            <input type="text" class="form-control" name="fdpTahunLulus" placeholder="Tahun Lulus" aria-describedby="fdpTahunLulus" value="<?=$datas!=false?$datas->fdpTahunLulus:''?>">
                        </div>

                        <div class="form-group">
                            <label>Insinyur Profesional PII</label>
                            <input type="text" class="form-control" name="fdpInsinyurProfesionalPII" placeholder="Insinyur Profesional PII" aria-describedby="fdpInsinyurProfesionalPII" value="<?=$datas!=false?$datas->fdpInsinyurProfesionalPII:''?>">
                        </div>

                        <div class="form-group">
                            <label>Alamat Rumah</label>
                            <input type="text" class="form-control" name="fdpAlamatRumah" placeholder="Alamat Rumah" aria-describedby="fdpAlamatRumah" value="<?=$datas!=false?$datas->fdpAlamatRumah:''?>">
                        </div>

                        <div class="form-group">
                            <label>Kota Rumah</label>
                            <select class="form-control m-select2" name="fdpKota">
                                <option value=""></option>
                                <?php 
                                foreach($ref_kota as $row):
                                    echo '<option value="'.$row->kotaKode.'" ' . ($datas != false ? $datas->fdpKota == $row->kotaKode ? 'selected' : '' : '') . '>'.$row->kotaNama.'</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Kode Pos Rumah </label>
                            <input type="text" class="form-control" name="fdpKodePos" placeholder="Kode Pos Rumah" aria-describedby="fdpKodePos" value="<?=$datas!=false?$datas->fdpKodePos:''?>">
                        </div>

                        <div class="form-group">
                            <label>Telpon Rumah </label>
                            <input type="text" class="form-control" name="fdpTelpon" placeholder="Telpon Rumah" aria-describedby="fdpTelpon" value="<?=$datas!=false?$datas->fdpTelpon:''?>">
                        </div>

                        <div class="form-group">
                            <label>Faksimil Rumah </label>
                            <input type="text" class="form-control" name="fdpFaksimilrumah" placeholder="Faksimil rumah" aria-describedby="fdpFaksimilrumah" value="<?=$datas!=false?$datas->fdpFaksimilrumah:''?>">
                        </div>

                          <div class="form-group">
                            <label>Telex Rumah </label>
                            <input type="text" class="form-control" name="fdpTelexRumah" placeholder="Telex rumah" aria-describedby="fdpTelexRumah" value="<?=$datas!=false?$datas->fdpTelexRumah:''?>">
                        </div>

                          <div class="form-group">
                            <label>Telpon Seluler </label>
                            <input type="text" class="form-control" name="fdpNoSeluler" placeholder="No hp seluler" aria-describedby="fdpNoSeluler" value="<?=$datas!=false?$datas->fdpNoSeluler:''?>">
                        </div>

                        <div class="form-group">
                            <label>Nama Lembaga</label>
                            <input type="text" class="form-control" name="fdpNamaLembaga" placeholder="Nama Lembaga" aria-describedby="fdpNamaLembaga" value="<?=$datas!=false?$datas->fdpNamaLembaga:''?>">
                        </div>

                        <div class="form-group">
                            <label> Jabatan</label>
                            <input type="text" class="form-control" name="fdpJabatan" placeholder="Jabatan" aria-describedby="fdpJabatan" value="<?=$datas!=false?$datas->fdpJabatan:''?>">
                        </div>

                        <div class="form-group">
                            <label>Jabatan Lain</label>
                            <input type="text" class="form-control" name="fdpJabatanLain" placeholder="Jabatan Lain" aria-describedby="fdpJabatanLain" value="<?=$datas!=false?$datas->fdpJabatanLain:''?>">
                        </div>

                        <div class="form-group">
                            <label>AlamatLembaga</label>
                            <input type="text" class="form-control" name="fdpAlamatLembaga" placeholder="Alamat Lembaga" aria-describedby="fdpAlamatLembaga" value="<?=$datas!=false?$datas->fdpAlamatLembaga:''?>">
                        </div>

                        <div class="form-group">
                            <label>Kota Lembaga</label>
                            <select class="form-control m-select2" name="fdpKotaLembaga">
                                <option value=""></option>
                                <?php 
                                foreach($ref_kota as $row):
                                    echo '<option value="'.$row->kotaKode.'" ' . ($datas != false ? $datas->fdpKotaLembaga == $row->kotaKode ? 'selected' : '' : '') . '>'.$row->kotaNama.'</option>';
                                endforeach;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Pos Lembaga</label>
                            <input type="text" class="form-control" name="fdpKodePosLembaga" placeholder="Kode Pos Lembaga" aria-describedby="fdpKodePosLembaga" value="<?=$datas!=false?$datas->fdpKodePosLembaga:''?>">
                        </div>

                        <div class="form-group">
                            <label>Telpon Lembaga</label>
                            <input type="text" class="form-control" name="fdpTelponLembaga" placeholder="Telpon Lembaga" aria-describedby="fdpTelponLembaga" value="<?=$datas!=false?$datas->fdpTelponLembaga:''?>">
                        </div>

                         <div class="form-group">
                            <label>Faksimil Lembaga </label>
                            <input type="text" class="form-control" name="fdpFaksimilLembaga" placeholder="Faksimil Lembaga" aria-describedby="fdpFaksimilLembaga" value="<?=$datas!=false?$datas->fdpFaksimilLembaga:''?>">
                        </div>

                          <div class="form-group">
                            <label>Telex Lembaga </label>
                            <input type="text" class="form-control" name="fdpTelexLembaga" placeholder="Telex Lembaga" aria-describedby="fdpTelexLembaga" value="<?=$datas!=false?$datas->fdpTelexLembaga:''?>">
                        </div>

                        <div class="form-group">
                            <label>Foto</label>
                            <input type="file" class="form-control form-control-lg form-control-solid" type="text" name="uploadfile" placeholder="Foto"  id="custom-file-input" />
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->
