
<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?=$save_url?>" method="post" id="form_data" >
                    <div class="kt-portlet__body">
                        <input type="hidden" name="fpeIdOld" value="<?=$datas!=false?$datas->aktivitasId:''?>">
                        <input type="hidden" class="form-control" name="aktivitasFdpId" placeholder="fopFdpId" aria-describedby="aktivitasFdpId" value="<?=$datas!=false?$datas->aktivitasFdpId:$fdpNoKta?>">
                        <div class="form-group">
                            <label>Bulan-Tahun</label>
                            <input type="text" class="form-control" name="aktivitasTahunAwal" placeholder="Bulan Tahun" aria-describedby="aktivitasTahunAwal" value="<?=$datas!=false?$datas->aktivitasTahunAwal:''?>">
                        </div>

                        <div class="form-group">
                            <label> Nama Seminar/Lokakarya Keinsinyuran </label>
                            <input type="text" class="form-control" name="aktivitasNama" placeholder=" Nama Seminar/Lokakarya Keinsinyuran" aria-describedby="aktivitasNama" value="<?=$datas!=false?$datas->aktivitasNama:''?>">
                        </div>
                        <div class="form-group">
                            <label>Media Publikasi</label>
                            <input type="text" class="form-control" name="aktivitasLembaga" placeholder="Media Publikasi" aria-describedby="aktivitasLembaga" value="<?=$datas!=false?$datas->aktivitasLembaga:''?>">
                        </div>

                        <div class="form-group">
                            <label>Uraian</label>
                            <textarea class="form-control" id="exampleTextarea" rows="3" name="aktivitasUraian" placeholder="aktivitasUraian" aria-describedby="aktivitasUraian" maxlength="2000"><?=$datas!=false?$datas->aktivitasUraian:''?> </textarea>
                              <span class="text-danger">*Min 100 Kata. Max 200 Kata</span>
                        </div>

                        <div class="form-group">
                            <label>Berkas</label>
                            <input type="file" class="form-control form-control-lg form-control-solid" type="text" name="uploadfile" placeholder="Foto"  id="custom-file-input" />
                            <span class="text-danger">* Format PDF dan max 700Kb</span></label>
                        </div>

                        <div class="form-group">
                            <label>Uraian Kompetensi</label>

                            <table class="table table-hover">
                                <thead class="thead-light">
                                    <tr>
                                        <th>#</th>
                                        <th>Uraian Kompetensi</th>

                                    </tr>
                                </thead>
                                <tbody>
                                   <?php
                                   foreach ($ref_uraiankompetensi as $row2) {
                                    if (!empty($row2->kompetensiUraianKompetensiId))
                                        $cek = "checked";
                                    else
                                        $cek="";

                                    ?>
                                    <tr>
                                        <th scope="row"><input type="checkbox"class="checked" <?=$cek?> name="cekKompetensi[]" value="<?=$row2->uraianKompetensiId?>" /></th>
                                        <td><?=$row2->uraianKompetensiNama?>  <?=$row2->uraianKompetensiKet?></td>

                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->
