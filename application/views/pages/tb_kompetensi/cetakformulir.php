<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="UTF-8">

	<title>Cetak Formulir</title>

	<style>

		table, th, td {
			font-size: 12pt;
			font-style: normal;
			border-collapse: collapse;
		}

	</style>

</head>


<body >
	<div align="center"><b>FORMULIR APLIKASI INSINYUR PROFESIONAL</b></div>
	<br>
	<div align="center"><b>I. DATA PRIBADI</b></div>
	<br>
	<table border="0"  width="95%" align="center">
		<tr>
			<th align="left">I.1 U m u m</th>
			<th align="right"><b><u>PERIODA : <?=$insinyur->fdpTahunAwal?> s/d <?=$insinyur->fdpTahunAkhir?></u></b></th>
		</tr>
	</table>
	<div class="table-responsive">
		<table border="1"  width="95%" align="center">
			<thead>
				<tr>
					<td width= "25%" style="padding-left: 5px">Nama  Lengkap</td>
					<td width= "50%" colspan ="4" style="padding-left: 5px"><?=$insinyur->fdpNama?></td>
					<!-- <td width= "25%" colspan ="1" rowspan="5" align="center"><img src="public/assets/berkas/<?=$insinyur!=false?$insinyur->fdpFoto:''?>" width="120px" height="140px" align="center" /></td> -->
					<td width= "25%" colspan ="1" rowspan="5" align="center"><img src="<?php echo base_url(); ?>public/assets/berkas/<?=$insinyur!=false?$insinyur->fdpFoto:''?>" width="120px" height="140px" align="center" /></td>
				</tr>
				<tr>
					<td style="padding-left: 5px">Tempat & Tanggal Lahir</td>
					<td colspan ="4" style="padding-left: 5px"><?=$insinyur->tempatlahir?>, 	
						<?=date('d-m-Y ',strtotime ($insinyur->fdpTanggalLahir))?>
					</td>

				</tr>
				<tr>
					<td style="padding-left: 5px">No KTA</td>
					<td colspan ="4" style="padding-left: 5px"><?=$insinyur->fdpNoKta?></td>

				</tr>
				<tr>
					<td style="padding-left: 5px">Badan Kejuruan</td>
					<td colspan ="4" style="padding-left: 5px"><?=$insinyur->fdpBadanKejuruan?></td>

				</tr>
			</thead>
			<tbody>
				<tr>
					<td rowspan="5" width= "30%" style="padding-left: 5px">Alamat</td>
					<td colspan="2" width= "60%" height="50px" align="center" style="padding-left: 5px">Rumah</td>
					<td colspan="2" width= "30%" height="50px" align="center" style="padding-left: 5px">Lembaga (Instansi / Perusahaan)</td>
				</tr>
				<tr>
					<td colspan="2"  rowspan="3" style="padding-left: 5px"><?=$insinyur->fdpAlamatRumah?> </td>
					<td colspan="3" height="25px" style="padding-left: 5px">Nama Lembaga : <?=$insinyur->fdpNamaLembaga?> </td>
				</tr>
				<tr>
					<td colspan="3" height="25px" style="padding-left: 5px">Jabatan Lembaga : <?=$insinyur->fdpJabatan?>  </td>
				</tr>
				<tr>
					<td colspan="3" height="100px" style="padding-left: 5px">Alamat Lembaga :  <?=$insinyur->fdpAlamatLembaga?></td>

				</tr>
				<tr>
					<td style="padding-left: 5px">Kota :  <?=$insinyur->kota?> </td>
					<td style="padding-left: 5px">Kode Pos :  <?=$insinyur->fdpKodePos?> </td>
					<td width= "40%" style="padding-left: 5px">Kota :  <?=$insinyur->KotaLembaga?> </td>
					<td colspan="2" style="padding-left: 5px">Kode Pos :  <?=$insinyur->fdpKodePosLembaga?> </td>
				</tr>
				<tr>
					<td rowspan="2" style="padding-left: 5px">Komunikasi</td>
					<td style="padding-left: 5px"> Telpon : <?=$insinyur->fdpTelpon?> </td>
					<td style="padding-left: 5px"> Faksimil : <?=$insinyur->fdpFaksimilrumah?></td>
					<td style="padding-left: 5px"> Telpon : <?=$insinyur->fdpTelponLembaga?></td>
					<td colspan="2" style="padding-left: 5px"> Faksimil : <?=$insinyur->fdpFaksimilLembaga?></td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left: 5px">Telex : <?=$insinyur->fdpTelexRumah?></td>
					<td colspan="3" style="padding-left: 5px"> Telex : <?=$insinyur->fdpTelexLembaga?></td>
				</tr>
				<tr>
					<td style="padding-left: 5px">Komunikasi Lainnya</td>
					<td  colspan="2" style="padding-left: 5px">Telepon Seluler : <?=$insinyur->fdpNoSeluler?></td>
					<td  colspan="3" style="padding-left: 5px"> Email : <?=$insinyur->fdpEmail?></td>
				</tr>
			</tbody>
		</table>
	</div>
</body>

<body >
	<div >
		<table border="0"  width="95%" align="center">
			<tr>
				<th  align="left">I.2 Pendidikan Formal</th>
				<th align="right">W2</th>
			</tr>
		</table>
		<table border="1"  width="95%" align="center">
			<thead align="left">
				<tr>
					
					<td style="padding-left: 5px">Jenjang</td>
					<?php
					if($penformal!=false) {
						$i = 1;
						foreach($penformal as $row) {
							$key = $this->encryptions->encode($row->aktivitasId,$this->config->item('encryption_key'));
							echo '<td style="padding-left: 5px" align="center">'.$row->jenjangNama.'</td>';
						}  
					} ?>
				</tr>
				<tr>
					
					<td style="padding-left: 5px">Nama Perguruan Tinggi</td>
					<?php
					if($penformal!=false) {
						$i = 1;
						foreach($penformal as $row) {
							$key = $this->encryptions->encode($row->aktivitasId,$this->config->item('encryption_key')); 
							echo '<td style="padding-left: 5px">'.$row->aktivitasNama.'</td>';
						}  
					} ?>
				</tr>
				<tr>
					
					<td style="padding-left: 5px">Fakultas</td>
					<?php
					if($penformal!=false) {
						$i = 1;
						foreach($penformal as $row) {
							$key = $this->encryptions->encode($row->aktivitasId,$this->config->item('encryption_key'));
							echo '<td style="padding-left: 5px">'.$row->aktivitasJumlah.'</td>';
						}  
					} ?>
				</tr>
				<tr>
					
					<td style="padding-left: 5px">Jurusan</td>
					<?php
					if($penformal!=false) {
						$i = 1;foreach($penformal as $row) {
							$key = $this->encryptions->encode($row->aktivitasId,$this->config->item('encryption_key'));
							echo '<td style="padding-left: 5px">'.$row->aktivitasTahunAwal.'</td>';
						}  
					} ?>
				</tr>
				<tr>
					
					<td style="padding-left: 5px">Kota</td>
					<?php
					if($penformal!=false) {
						$i = 1;
						foreach($penformal as $row) {
							$key = $this->encryptions->encode($row->aktivitasId,$this->config->item('encryption_key'));
							echo '<td style="padding-left: 5px">'.$row->kotaNama.'</td>';
						}  
					} ?>
				</tr>
				<tr>
					
					<td style="padding-left: 5px">Negara</td>
					<?php
					if($penformal!=false) {
						$i = 1;
						foreach($penformal as $row) {
							$key = $this->encryptions->encode($row->aktivitasId,$this->config->item('encryption_key')); 
							echo '<td style="padding-left: 5px">'.$row->negNama.'</td>';
						}  
					} ?>
				</tr>
				<tr>
					
					<td style="padding-left: 5px">Tahun Lulus</td>
					<?php
					if($penformal!=false) {
						$i = 1;
						foreach($penformal as $row) {
							$key = $this->encryptions->encode($row->aktivitasId,$this->config->item('encryption_key')); 
							echo '<td style="padding-left: 5px">'.$row->aktivitasTahunlulus.'</td>';
						}  
					} ?>
				</tr>
				<tr>
					
					<td style="padding-left: 5px">Gelar</td>
					<?php
					if($penformal!=false) {
						$i = 1;
						foreach($penformal as $row) {
							$key = $this->encryptions->encode($row->aktivitasId,$this->config->item('encryption_key')); 
							echo '<td style="padding-left: 5px">'.$row->aktivitasTahunAkhir.'</td>';
						}  
					} ?>
				</tr>
				<tr>
					
					<td style="padding-left: 5px">Judul TugasAkhir/ Skripsi/ Tesis/Disertasi </td>
					<?php
					if($penformal!=false) {
						$i = 1;
						foreach($penformal as $row) {
							$key = $this->encryptions->encode($row->aktivitasId,$this->config->item('encryption_key')); 
							echo '<td style="padding-left: 5px">'.$row->aktivitasJudulTa.'</td>';
						}  
					} ?>
				</tr>
				<tr>
					
					<td style="padding-left: 5px">Uraian Singkat Tentang Materi, TugasAkhir/ Skripsi/ Tesis/ Disertasi</td>
					<?php
					if($penformal!=false) {
						$i = 1;
						foreach($penformal as $row) {
							$key = $this->encryptions->encode($row->aktivitasId,$this->config->item('encryption_key')); 
							echo '<td style="padding-left: 5px">'.$row->aktivitasUraian.'</td>';
						}  
					} ?>
				</tr>
				<tr>
					
					<td style="padding-left: 5px">Nilai Akademik Rata-rata</td>
					<?php
					if($penformal!=false) {
						$i = 1;
						foreach($penformal as $row) {
							$key = $this->encryptions->encode($row->aktivitasId,$this->config->item('encryption_key')); 
							echo '<td style="padding-left: 5px">'.$row->aktivitasNilai.'</td>';
						}  
					} ?>
				</tr>
				<tr>
					<td style="padding-left: 5px">Judicium</td>
					<?php
					if($penformal!=false) {
						$i = 1;
						foreach($penformal as $row) {
							$key = $this->encryptions->encode($row->aktivitasId,$this->config->item('encryption_key')); 
							echo '<td style="padding-left: 5px">'.date('d-m-Y ',strtotime ($row->aktivitasPeriode)).'</td>';
						}  
					} ?>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</body>

<body>
	<div class="table-responsive">

		<table border="0"  width="95%" align="center">
			<tr>
				<th  align="left">I.3   Organisasi Profesi & Organisasi Lainnya Yang Dimasuki  </th>
				<th align="right">W1</th>
			</tr>
		</table>     
		<table border="1"  width="95%" align="center">
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">Nama Organisasi dan Kota/Negara</th>
					<th rowspan="2">Periode</th>
					<th rowspan="2">Jabatan</th>
					<th rowspan="2">Aktifitas & Jabatan Dalam Organisasi</th>
					<th rowspan="2">Kompetensi *)</th>
					<th colspan="4">Nilai</th>
				</tr> 
				<tr>
					<th>P</th>
					<th>Q</th>
					<th>R</th>
					<th>T</th>
				</tr> 
			</thead>
			<tbody>
				<?php if($orprof!=false) { $i = 1; foreach($orprof as $row) { ?>
					<tr>
						<th scope="row"><?=$i++?></th>
						<td style="padding-left: 5px"> 
							<?=$row->aktivitasNama?><br>
							<?=$row->kotaNama?><br>
							<?=$row->negNama?>
						</td>
						<td style="padding-left: 5px"><?=$row->aktivitasTahunAwal?>
						- <?=$row->aktivitasTahunAkhir?></td>
						<td style="padding-left: 5px"><?=$row->aktivitasJumlah?></td>
						<td style="padding-left: 5px"><?=$row->aktivitasUraian?></td>
						<td align="left" style="padding-left: 15px">
							<?php if($orprof1!=false) { foreach($orprof1 as $name => $data) { foreach($data as $name2 => $value) { ?> <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->uraianKompetensiNama?></br> <?php } else { }?><?php } } }?>
						</td>
						<td>
							<!-- <?php if($orprof1!=false) { foreach($orprof1 as $name => $data) { foreach($data as $name2 => $value) {?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiP?></br> <?php }else {}?> <?php } } }?> -->
						</td>
						<td>
							<!--  <?php if($orprof1!=false) { foreach($orprof1 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiQ?></br> <?php }else {}?> <?php } } }?> -->
						</td>
						<td>
							<!--  <?php if($orprof1!=false) { foreach($orprof1 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiR?></br> <?php }else {}?> <?php } } }?> -->
						</td>
						<td>
							<!--  <?php if($orprof1!=false) {foreach($orprof1 as $name => $data) {foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiT?></br> <?php }else {}?> <?php } } }?> -->
						</td>
						</tr> <?php }  } ?>
					</tbody>
				</table>
			</div>
		</body>

		<body>
			<div class="table-responsive">   

				<table border="0"  width="95%" align="center">
					<tr>
						<th align="left">I.4 Tanda Penghargaan yang Diterima  </th>
						<th align="right">W1</th>
					</tr>
				</table>     
				<table border="1"  width="95%" align="center">
					<thead>
						<tr>
							<th rowspan="2">No</th>
							<th rowspan="2">Tahun Penghargaan</th>
							<th rowspan="2">Nama Tanda Penghargaan</th>
							<th rowspan="2">Uraian Singkat, Lembaga Yang Memberikan, Lokasi/ Negara</th>
							<th rowspan="2">Kompetensi *)</th>
							<th colspan="4">Nilai</th>
						</tr> 
						<tr>
							<th>P</th>
							<th>Q</th>
							<th>R</th>
							<th>T</th>
						</tr> 
					</thead>
					<tbody>
						<?php if($fpenghargaan1!=false) { $i = 1; foreach($fpenghargaan1 as $row) { ?>
							<tr>
								<th scope="row"><?=$i++?></th>
								<td style="padding-left: 10px"><?=$row->aktivitasTahunAwal?></td>
								<td style="padding-left: 10px"><?=$row->aktivitasNama?></td>
								<td style="padding-left: 10px"><?=$row->aktivitasUraian?> <br><?=$row->kotaNama?>- <?=$row->negNama?></td>
								<td style="padding-left: 15px" align="left">
									<?php if($fpenghargaan2!=false){ foreach($fpenghargaan2 as $name => $data) { foreach($data as $name2 => $value)  { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->uraianKompetensiNama?></br>  <?php }else {}?> <?php } } }?>
								</td>
								<td>
                   <!--  <?php if($fpenghargaan2!=false){ foreach($fpenghargaan2 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId ) { ?> <br><?=$value->kompetensiNilaiP?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                   <!--  <?php if($fpenghargaan2!=false) { foreach($fpenghargaan2 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiQ?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                <!--     <?php if($fpenghargaan2!=false) { foreach($fpenghargaan2 as $name => $data) {foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiR?></br> <?php }else {}?>
                <?php } } }?> -->
            </td>
            <td>
                   <!--  <?php if($fpenghargaan2!=false){foreach($fpenghargaan2 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiT?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               </tr>  <?php }  } ?>
           </tbody>
       </table>
   </div>
</body>

<body>
	<div class="table-responsive">   
		<table border="0" width="95%" align="center">
			<tr>
				<th align="left"> I.5 Pendidikan/Pelatihan Teknik/Pertanian  dan Profesi Keinsinyuran Yang Diikuti </th>
				<th align="right">W2</th>
			</tr>
		</table>  
		<table border="1" width="95%" align="center">
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">Nama Pendidikan, Penyelenggara, Lokasi</th>
					<th rowspan="2">Bulan - Tahun</th>
					<th rowspan="2">Jumlah Jam</th>
					<th rowspan="2">Uraian Singkat, Tingkat Pendidikan, Sertifikat</th>
					<th rowspan="2">Kompetensi *)</th>
					<th colspan="4">Nilai</th>
				</tr> 
				<tr>
					<th>P</th>
					<th>Q</th>
					<th>R</th>
					<th>T</th>
				</tr> 
			</thead>
			<tbody>
				<?php if($fpendteknik1!=false) { $i = 1; foreach($fpendteknik1 as $row) { ?>
					<tr>
						<th scope="row"><?=$i++?></th>
						<td style="padding-left: 10px"><?=$row->aktivitasNama?><br><?=$row->aktivitasLembaga?><br><?=$row->kotaNama?>- <?=$row->negNama?></td>
						<td style="padding-left: 10px"><?=date('d-m-Y ',strtotime ($row->aktivitasPeriode))?> s/d <?=date('d-m-Y ',strtotime ($row->aktivitasPeriodeAkhir))?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasJumlah?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasUraian?> <br> Pelatihan <?=$row->lvlatihNama?></td>
						<td style="padding-left: 20px" align="left">
							<?php if($fpendteknik2!=false){ foreach($fpendteknik2 as $name => $data) { foreach($data as $name2 => $value)  { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->uraianKompetensiNama?></br>  <?php }else {}?> <?php } } }?>
						</td>
						<td>
                   <!--  <?php if($fpendteknik2!=false){ foreach($fpendteknik2 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId ) { ?> <br><?=$value->kompetensiNilaiP?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                   <!--  <?php if($fpendteknik2!=false) { foreach($fpendteknik2 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiQ?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                   <!--  <?php if($fpendteknik2!=false) { foreach($fpendteknik2 as $name => $data) {foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiR?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                   <!--  <?php if($fpendteknik2!=false){foreach($fpendteknik2 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiT?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               </tr>  <?php }  } ?>
           </tbody>
       </table>
   </div>
</body>

<body>
	<div class="table-responsive">   

		<table border="0"  width="95%" align="center">
			<tr>
				<th  align="left">1.6   Pendidikan/Pelatihan Manajemen dan Bidang Lainnya (yang Relevan) yang Diikuti ( # )</th>
				<th align="right">W4, P10</th>
			</tr>
		</table>  
		<table border="1"  width="95%" align="center">
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">Nama Pendidikan, Penyelenggara, Lokasi</th>
					<th rowspan="2">Bulan - Tahun</th>
					<th rowspan="2">Jumlah Jam</th>
					<th rowspan="2">Uraian Singkat, Tingkat Pendidikan, Sertifikat</th>
					<th rowspan="2">Kompetensi *)</th>
					<th colspan="4">Nilai</th>
				</tr> 
				<tr>
					<th>P</th>
					<th>Q</th>
					<th>R</th>
					<th>T</th>
				</tr> 
			</thead>
			<tbody>
				<?php if($fpendmanaj1!=false) { $i = 1; foreach($fpendmanaj1 as $row) { ?>
					<tr>
						<th scope="row"><?=$i++?></th>
						<td style="padding-left: 10px"><?=$row->aktivitasNama?><br><?=$row->aktivitasLembaga?><br><?=$row->kotaNama?>- <?=$row->negNama?></td>
						<td style="padding-left: 10px"><?=date('d-m-Y ',strtotime ($row->aktivitasPeriode))?> s/d <?=date('d-m-Y ',strtotime ($row->aktivitasPeriodeAkhir))?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasJumlah?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasUraian?> <br> Pelatihan <?=$row->lvlatihNama?></td>
						<td style="padding-left: 20px" align="left">
							<?php if($fpendmanaj2!=false){ foreach($fpendmanaj2 as $name => $data) { foreach($data as $name2 => $value)  { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br> <?=$value->uraianKompetensiNama?> </br>  <?php }else {}?> <?php } } }?>
						</td>
						<td>
                   <!--  <?php if($fpendmanaj2!=false){ foreach($fpendmanaj2 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId ) { ?> <br><?=$value->kompetensiNilaiP?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                  <!--   <?php if($fpendmanaj2!=false) { foreach($fpendmanaj2 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiQ?></br> <?php }else {}?>
                  <?php } } }?> -->
              </td>
              <td>
                    <!-- <?php if($fpendmanaj2!=false) { foreach($fpendmanaj2 as $name => $data) {foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiR?></br> <?php }else {}?>
                    <?php } } }?> -->
                </td>
                <td>
                   <!--  <?php if($fpendmanaj2!=false){foreach($fpendmanaj2 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiT?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               </tr>  <?php }  } ?>
           </tbody>
       </table>
   </div>
</body>

<body>
	<div class="table-responsive">   
		<div align="center"> II.   KUALIFIKASI KODE ETIK INSINYUR INDONESIA  dan ETIKA PROFESIONAL</div>
		<br>
		<table border="0"  width="95%" align="center">
			<tr>
				<th  align="left">II.1   Referensi Kode Etik dan Etika Profesi (#)   </th>
				<th align="right">W1</th>
			</tr>
		</table>   
		<table border="1"  width="95%" align="center">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama</th>
					<th>Alamat</th>
					<th>No.Telpon</th>
					<th>Hubungan</th>
				</tr>
			</thead>
			<tbody>
				<?php if($kodeetikII1!=false) { $i = 1; foreach($kodeetikII1 as $row) { ?>
					<tr>
						<th scope="row"><?=$i++?></th>
						<td style="padding-left: 10px"><?=$row->aktivitasNama?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasJumlah?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasTahunAwal?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasUraian?></td>

						</tr>  <?php }  } ?>
					</tbody>
				</table>
			</div>
		</body>

		<body>
			<div class="table-responsive">   

				<table border="0"  width="100%" >
					<tr>
						<th  align="left"> II.2  Pengertian, Pendapat dan Pengalaman Sendiri</th>
						<th align="right">  </th>
					</tr>
					<tr>
						<th  align="left" style="font-size: 7pt;">
						Tuliskan dengan kata-kata sendiri apa pengertian dan pendapat Anda tentang Kode Etik Insinyur serta  pengalaman Anda tentang Etika Profesi.</th>
						<th align="right">W1</th>
					</tr>
				</table>  
				<table border="1"  width="100%" >
					<thead>
						<tr>
							<th rowspan="2">Uraian Pendapat</th>
							<th rowspan="2">Kompetensi *)</th>
							<th colspan="4">Nilai</th>
						</tr> 
						<tr>
							<th>P</th>
							<th>Q</th>
							<th>R</th>
							<th>T</th>
						</tr> 
					</thead>
					<tbody>
						<?php if($kodeetikII21!=false) { $i = 1; foreach($kodeetikII21 as $row) { ?>
							<tr>
								<td style="padding-left: 10px; padding-right: 10px" align="justify" ><?=$row->aktivitasUraian?></td>
								<td style="padding-left: 20px" align="left">
									<?php if($kodeetikII22!=false){ foreach($kodeetikII22 as $name => $data) { foreach($data as $name2 => $value)  { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->uraianKompetensiNama?></br>  <?php }else {}?> <?php } } }?>
								</td>
								<td>
                   <!--  <?php if($kodeetikII22!=false){ foreach($kodeetikII22 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId ) { ?> <br><?=$value->kompetensiNilaiP?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                    <!-- <?php if($kodeetikII22!=false) { foreach($kodeetikII22 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiQ?></br> <?php }else {}?>
                    <?php } } }?> -->
                </td>
                <td>
                   <!--  <?php if($kodeetikII22!=false) { foreach($kodeetikII22 as $name => $data) {foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiR?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                   <!--  <?php if($kodeetikII22!=false){foreach($kodeetikII22 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiT?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               </tr>  <?php }  } ?>
           </tbody>
       </table>
   </div>
</body>

<body>
	<div class="table-responsive">   
		<div align="center">III.   KUALIFIKASI PROFESIONAL </div>
		<br>
		<table border="0"  width="100%" >
			<tr>
				<th align="left">III.1 Pengalaman Dalam  Perencanaan & Perancangan dan/atau  Pengalaman Dalam Pengelolaan Tugas-Tugas Keinsinyuran</th>
				<th align="right">W2, W3, P7 </th>
			</tr>
		</table>  
		<table border="1"  width="100%" >
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">Periode</th>
					<th rowspan="2">Instansi / Perusahaan, Lokasi Tempat Tugas, Nama Proyek </th>
					<th rowspan="2">Posisi Tugas/Jabatan </th>
					<th rowspan="2">Uraian Singkat Tugas Dan Tanggungjawab Profesional</th>
					<th rowspan="2">Kompe tensi *)</th>
					<th colspan="4">Nilai</th>
				</tr> 
				<tr>
					<th>P</th>
					<th>Q</th>
					<th>R</th>
					<th>T</th>
				</tr> 
			</thead>
			<tbody>
				<?php if($kualifikasiprof11!=false) { $i = 1; foreach($kualifikasiprof11 as $row) { ?>
					<tr>
						<th scope="row"><?=$i++?></th>
						<td style="padding-left: 10px"><?=$row->aktivitasTahunAwal?>-<?=$row->aktivitasTahunAkhir?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasLembaga?> <br><?=$row->kotaNama?>- <?=$row->negNama?><br>Nama Proyek : <?=$row->aktivitasNama?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasJumlah?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasUraian?></td>
						<td style="padding-left: 20px" align="left">
							<?php if($kualifikasiprof12!=false){ foreach($kualifikasiprof12 as $name => $data) { foreach($data as $name2 => $value)  { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->uraianKompetensiNama?></br>  <?php }else {}?> <?php } } }?>
						</td>
						<td>
                   <!--  <?php if($kualifikasiprof12!=false){ foreach($kualifikasiprof12 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId ) { ?> <br><?=$value->kompetensiNilaiP?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                   <!--  <?php if($kualifikasiprof12!=false) { foreach($kualifikasiprof12 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiQ?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                   <!--  <?php if($kualifikasiprof12!=false) { foreach($kualifikasiprof12 as $name => $data) {foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiR?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                  <!--   <?php if($kualifikasiprof12!=false){foreach($kualifikasiprof12 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiT?></br> <?php }else {}?>
                  <?php } } }?> -->
              </td>
              </tr>  <?php }  } ?>
          </tbody>
      </table>
  </div>
</body>

<body>
	<div class="table-responsive">   
		<table border="0"  width="100%" >
			<tr>
				<th align="left">III.2 Pengalaman Mengajar Pelajaran Keinsinyuran dan/atau Manajemen dan/atau Pengalaman Mengembangkan Pendidikan/Pelatihan Keinsinyuran dan/atau  Manajemen </th>
				<th align="right">P5</th>
			</tr>
		</table>  
		<table border="1"  width="100%" >
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">Periode</th>
					<th rowspan="2">Nama Perguruan Tinggi/Lembaga Dan Lokasi</th>
					<th rowspan="2">Jumlah Jam atau S.K.S </th>
					<th rowspan="2">Nama Mata Ajaran Atau Uraian Singkat Yang Diajarkan/Dikembangkan </th>
					<th rowspan="2">Kompetensi *)</th>
					<th colspan="4">Nilai</th>
				</tr> 
				<tr>
					<th>P</th>
					<th>Q</th>
					<th>R</th>
					<th>T</th>
				</tr> 
			</thead>
			<tbody>
				<?php if($kualifikasiprof21!=false) { $i = 1; foreach($kualifikasiprof21 as $row) { ?>
					<tr>
						<th scope="row"><?=$i++?></th>
						<td style="padding-left: 10px"><?=$row->aktivitasTahunAwal?> - <?=$row->aktivitasTahunAkhir?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasLembaga?> <br><?=$row->kotaNama?>- <?=$row->negNama?><br>Nama Proyek : <?=$row->aktivitasNama?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasJumlah?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasUraian?></td>
						<td style="padding-left: 20px" align="left">
							<?php if($kualifikasiprof22!=false){ foreach($kualifikasiprof22 as $name => $data) { foreach($data as $name2 => $value)  { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->uraianKompetensiNama?></br>  <?php }else {}?> <?php } } }?>
						</td>
						<td>
                  <!--   <?php if($kualifikasiprof22!=false){ foreach($kualifikasiprof22 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId ) { ?> <br><?=$value->kompetensiNilaiP?></br> <?php }else {}?>
                  <?php } } }?> -->
              </td>
              <td>
                   <!--  <?php if($kualifikasiprof22!=false) { foreach($kualifikasiprof22 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiQ?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                    <!-- <?php if($kualifikasiprof22!=false) { foreach($kualifikasiprof22 as $name => $data) {foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiR?></br> <?php }else {}?>
                    <?php } } }?> -->
                </td>
                <td>
                   <!--  <?php if($kualifikasiprof22!=false){foreach($kualifikasiprof22 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiT?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               </tr>  <?php }  } ?>
           </tbody>
       </table>
   </div>
</body>

<body>
	<div class="table-responsive">   
		<table border="0"  width="100%" >
			<tr>
				<th  align="left">III.3 Pengalaman  Dalam Penelitian, Pengembangan dan Komersialisasi dan/atau Pengalaman Menangani  Bahan Material dan Komponen </th>
				<th align="right">P6, P9</th>
			</tr>
		</table>  
		<table border="1"  width="100%" >
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">Periode</th>
					<th rowspan="2">Instansi/Perusahaan, Nama Proyek/Produk Dan Lokasi </th>
					<th rowspan="2">Posisi Tugas Jabatan</th>
					<th rowspan="2">Uraian Singkat Kegiatan</th>
					<th rowspan="2">Kompetensi *)</th>
					<th colspan="4">Nilai</th>
				</tr> 
				<tr>
					<th>P</th>
					<th>Q</th>
					<th>R</th>
					<th>T</th>
				</tr> 
			</thead>
			<tbody>
				<?php if($kualifikasiprof31!=false) { $i = 1; foreach($kualifikasiprof31 as $row) { ?>
					<tr>
						<th scope="row"><?=$i++?></th>
						<td style="padding-left: 10px"><?=$row->aktivitasTahunAwal?> - <?=$row->aktivitasTahunAkhir?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasLembaga?> <br><?=$row->kotaNama?>- <?=$row->negNama?><br>Nama Proyek : <?=$row->aktivitasNama?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasJumlah?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasUraian?></td>
						<td style="padding-left: 20px" align="left">
							<?php if($kualifikasiprof32!=false){ foreach($kualifikasiprof32 as $name => $data) { foreach($data as $name2 => $value)  { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->uraianKompetensiNama?></br>  <?php }else {}?> <?php } } }?>
						</td>
						<td>
                  <!--   <?php if($kualifikasiprof32!=false){ foreach($kualifikasiprof32 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId ) { ?> <br><?=$value->kompetensiNilaiP?></br> <?php }else {}?>
                  <?php } } }?> -->
              </td>
              <td>
                   <!--  <?php if($kualifikasiprof32!=false) { foreach($kualifikasiprof32 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiQ?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                  <!--   <?php if($kualifikasiprof32!=false) { foreach($kualifikasiprof32 as $name => $data) {foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiR?></br> <?php }else {}?>
                  <?php } } }?> -->
              </td>
              <td>
                   <!--  <?php if($kualifikasiprof32!=false){foreach($kualifikasiprof32 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiT?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               </tr>  <?php }  } ?>
           </tbody>
       </table>
   </div>
</body>

<body>
	<div class="table-responsive">   
		<table border="0"  width="100%" >
			<tr>
				<th align="left">III.4 Pengalaman Dalam Konsultansi Perekayasaan dan/atau Konstruksi/Instalasi  dan/atau Pengalaman  Dalam Pekerjaan Manufaktur atau Produksi </th>
				<th align="right">P7, P8</th>
			</tr>
		</table>  
		<table border="1"  width="100%" >
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">Periode</th>
					<th rowspan="2">Instansi/Perusahaan, Nama Proyek/Produk Dan Lokasi </th>
					<th rowspan="2">Posisi Tugas Jabatan</th>
					<th rowspan="2">Uraian Singkat Kegiatan</th>
					<th rowspan="2">Kompetensi *)</th>
					<th colspan="4">Nilai</th>
				</tr> 
				<tr>
					<th>P</th>
					<th>Q</th>
					<th>R</th>
					<th>T</th>
				</tr> 
			</thead>
			<tbody>
				<?php if($kualifikasiprof41!=false) { $i = 1; foreach($kualifikasiprof41 as $row) { ?>
					<tr>
						<th scope="row"><?=$i++?></th>
						<td style="padding-left: 10px"><?=$row->aktivitasTahunAwal?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasLembaga?> <br><?=$row->kotaNama?>- <?=$row->negNama?><br>Nama Proyek : <?=$row->aktivitasNama?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasJumlah?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasUraian?></td>
						<td style="padding-left: 20px" align="left">
							<?php if($kualifikasiprof42!=false){ foreach($kualifikasiprof42 as $name => $data) { foreach($data as $name2 => $value)  { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->uraianKompetensiNama?></br>  <?php }else {}?> <?php } } }?>
						</td>
						<td>
                   <!--  <?php if($kualifikasiprof42!=false){ foreach($kualifikasiprof42 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId ) { ?> <br><?=$value->kompetensiNilaiP?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                   <!--  <?php if($kualifikasiprof42!=false) { foreach($kualifikasiprof42 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiQ?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                    <!-- <?php if($kualifikasiprof42!=false) { foreach($kualifikasiprof42 as $name => $data) {foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiR?></br> <?php }else {}?>
                    <?php } } }?> -->
                </td>
                <td>
                   <!--  <?php if($kualifikasiprof42!=false){foreach($kualifikasiprof42 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiT?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               </tr>  <?php }  } ?>
           </tbody>
       </table>
   </div>
</body>

<body>
	<div class="table-responsive">   
		<table border="0"  width="100%" >
			<tr>
				<th  align="left">III.5 Pengalaman Dalam Manajemen Usaha dan Pemasaran Teknik   dan/atau Pengalaman Dalam Manajemen Pembangunan dan Pemeliharaan Asset </th>
				<th align="right">P10, P11 </th>
			</tr>
		</table>  
		<table border="1"  width="100%" >
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">Periode</th>
					<th rowspan="2">Instansi/Perusahaan, Nama Proyek/Produk Dan Lokasi </th>
					<th rowspan="2">Posisi Tugas Jabatan</th>
					<th rowspan="2">Uraian Singkat Kegiatan</th>
					<th rowspan="2">Kompetensi *)</th>
					<th colspan="4">Nilai</th>
				</tr> 
				<tr>
					<th>P</th>
					<th>Q</th>
					<th>R</th>
					<th>T</th>
				</tr> 
			</thead>
			<tbody>
				<?php if($kualifikasiprof51!=false) { $i = 1; foreach($kualifikasiprof51 as $row) { ?>
					<tr>
						<th scope="row"><?=$i++?></th>
						<td align="center"><?=$row->aktivitasTahunAwal?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasLembaga?> <br><?=$row->kotaNama?>- <?=$row->negNama?><br>Nama Proyek : <?=$row->aktivitasNama?></td>
						<td align="center"><?=$row->aktivitasJumlah?></td>
						<td style="padding-left: 10px"><?=$row->aktivitasUraian?></td>
						<td style="padding-left: 20px" align="left">
							<?php if($kualifikasiprof52!=false){ foreach($kualifikasiprof52 as $name => $data) { foreach($data as $name2 => $value)  { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->uraianKompetensiNama?></br>  <?php }else {}?> <?php } } }?>
						</td>
						<td>
                   <!--  <?php if($kualifikasiprof52!=false){ foreach($kualifikasiprof52 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId ) { ?> <br><?=$value->kompetensiNilaiP?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                  <!--   <?php if($kualifikasiprof52!=false) { foreach($kualifikasiprof52 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiQ?></br> <?php }else {}?>
                  <?php } } }?> -->
              </td>
              <td>
                  <!--   <?php if($kualifikasiprof52!=false) { foreach($kualifikasiprof52 as $name => $data) {foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiR?></br> <?php }else {}?>
                  <?php } } }?> -->
              </td>
              <td>
                  <!--   <?php if($kualifikasiprof42!=false){foreach($kualifikasiprof42 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiT?></br> <?php }else {}?>
                  <?php } } }?> -->
              </td>
              </tr>  <?php }  } ?>
          </tbody>
      </table>
  </div>
</body>


<body>
	<div class="table-responsive">   
		<div align="center">IV. PUBLIKASI, KOMUNIKASI DAN TEMUAN/INOVASI DI BIDANG KEINSINYURAN</div>
		<br>
		<table border="0"  width="100%" >
			<tr>
				<th  align="left">IV.1 Karya Tulis di Bidang Keinsinyuran yang Dipublikasikan</th>
				<th align="right">W4 </th>
			</tr>
		</table>  
		<table border="1"  width="100%" >
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">Bulan - Tahun</th>
					<th rowspan="2">Judul Karya Tulis</th>
					<th rowspan="2">Media Publikasi/ Lokasi</th>
					<th rowspan="2">Uraian Singkat Materi Yang Dipublikasikan </th>
					<th rowspan="2">Kompetensi *)</th>
					<th colspan="4">Nilai</th>
				</tr> 
				<tr>
					<th>P</th>
					<th>Q</th>
					<th>R</th>
					<th>T</th>
				</tr> 
			</thead>
			<tbody>
				<?php if($publikasi11!=false) { $i = 1; foreach($publikasi11 as $row) { ?>
					<tr>
						<th scope="row"><?=$i++?></th>
						<td style="text-align: center;"><?=$row->aktivitasTahunAwal?></td>
						<td style="padding-left: 10px;"><?=$row->aktivitasNama?> <br><?=$row->kotaNama?>- <?=$row->negNama?></td>
						<td style="padding-left: 10px;"><?=$row->aktivitasLembaga?></td>
						<td style="padding-left: 10px; padding-right: 10px;" align="justify"><?=$row->aktivitasUraian?></td>
						<td style="padding-left: 5px" align="left">
							<?php if($publikasi12!=false){ foreach($publikasi12 as $name => $data) { foreach($data as $name2 => $value)  { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->uraianKompetensiNama?></br>  <?php }else {}?> <?php } } }?>
						</td>
						<td>
							<?php if($publikasi12!=false){ foreach($publikasi12 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId ) { ?> <br><?=$value->kompetensiNilaiP?></br> <?php }else {}?>
						<?php } } }?>
					</td>
					<td>
						<?php if($publikasi12!=false) { foreach($publikasi12 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiQ?></br> <?php }else {}?>
					<?php } } }?>
				</td>
				<td>
					<?php if($publikasi12!=false) { foreach($publikasi12 as $name => $data) {foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiR?></br> <?php }else {}?>
				<?php } } }?>
			</td>
			<td>
				<?php if($publikasi12!=false){foreach($publikasi12 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiT?></br> <?php }else {}?>
			<?php } } }?>
		</td>
		</tr>  <?php }  } ?>
	</tbody>
</table>
</div>
</body>


<body>
	<div class="table-responsive">   
		<table border="0"  width="100%" >
			<tr>
				<th  align="left">IV.2 Makalah/Tulisan Yang Disajikan Dalam Seminar/Lokakarya Keinsinyuran</th>
				<th align="right">W4</th>
			</tr>
		</table>  
		<table border="1"  width="100%" >
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">Bulan -Tahun</th>
					<th rowspan="2">Nama Seminar/Loka Karya, Penyelenggara, Lokasi </th>
					<th rowspan="2">Judul Makalah/ Tulisan </th>
					<th rowspan="2">Uraian Singkat Materi Makalah/Tulisan </th>
					<th rowspan="2">Kompetensi *)</th>
					<th colspan="4">Nilai</th>
				</tr> 
				<tr>
					<th>P</th>
					<th>Q</th>
					<th>R</th>
					<th>T</th>
				</tr> 
			</thead>
			<tbody>
				<?php if($publikasi21!=false) { $i = 1; foreach($publikasi21 as $row) { ?>
					<tr>
						<th scope="row"><?=$i++?></th>
						<td align="center"><?=$row->aktivitasTahunAwal?></td>
						<td style="padding-left: 10px;"><?=$row->aktivitasNama?> <br><?=$row->aktivitasLembaga?>
						<br><?=$row->kotaNama?>- <?=$row->negNama?></td>
						<td style="padding-left: 10px;"><?=$row->aktivitasJumlah?></td>
						<td style="padding-left: 10px; padding-right: 10px;" align="justify"><?=$row->aktivitasUraian?></td>
						<td style="padding-left: 5px" align="left">
							<?php if($publikasi22!=false){ foreach($publikasi22 as $name => $data) { foreach($data as $name2 => $value)  { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->uraianKompetensiNama?></br>  <?php }else {}?> <?php } } }?>
						</td>
						<td>
                   <!--  <?php if($publikasi22!=false){ foreach($publikasi22 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId ) { ?> <br><?=$value->kompetensiNilaiP?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                   <!--  <?php if($publikasi22!=false) { foreach($publikasi22 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiQ?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                   <!--  <?php if($publikasi22!=false) { foreach($publikasi22 as $name => $data) {foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiR?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                  <!--   <?php if($publikasi22!=false){foreach($publikasi22 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiT?></br> <?php }else {}?>
                  <?php } } }?> -->
              </td>
              </tr>  <?php }  } ?>
          </tbody>
      </table>
  </div>
</body>
<body>
	<div class="table-responsive">   
		<table border="0"  width="100%" >
			<tr>
				<th  align="left">IV.3 Seminar/Lokakarya Keinsinyuran Yang Diikuti</th>
				<th align="right">W2 </th>
			</tr>
		</table>  
		<table border="1"  width="100%" >
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">Bulan -Tahun</th>
					<th rowspan="2">Nama Seminar / Loka Karya </th>
					<th rowspan="2">Nama Penyelenggara, Lokasi</th>
					<th rowspan="2">Uraian Singkat Seminar / Loka Karya  </th>
					<th rowspan="2">Kompetensi *)</th>
					<th colspan="4">Nilai</th>
				</tr> 
				<tr>
					<th>P</th>
					<th>Q</th>
					<th>R</th>
					<th>T</th>
				</tr> 
			</thead>
			<tbody>
				<?php if($publikasi31!=false) { $i = 1; foreach($publikasi31 as $row) { ?>
					<tr>
						<th scope="row"><?=$i++?></th>
						<td align="center"><?=$row->aktivitasTahunAwal?></td>
						<td style="padding-left: 10px;"><?=$row->aktivitasNama?></td>
						<td style="padding-left: 10px;"><?=$row->aktivitasLembaga?>
						<br><?=$row->kotaNama?>- <?=$row->negNama?></td>
						<td style="padding-left: 10px; padding-right: 10px;" align="justify"><?=$row->aktivitasUraian?></td>
						<td style="padding-left: 5px" align="left">
							<?php if($publikasi32!=false){ foreach($publikasi32 as $name => $data) { foreach($data as $name2 => $value)  { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->uraianKompetensiNama?></br>  <?php }else {}?> <?php } } }?>
						</td>
						<td>
							<?php if($publikasi32!=false){ foreach($publikasi32 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId ) { ?> <br><?=$value->kompetensiNilaiP?></br> <?php }else {}?>
						<?php } } }?>
					</td>
					<td>
						<?php if($publikasi32!=false) { foreach($publikasi32 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiQ?></br> <?php }else {}?>
					<?php } } }?>
				</td>
				<td>
					<?php if($publikasi32!=false) { foreach($publikasi32 as $name => $data) {foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiR?></br> <?php }else {}?>
				<?php } } }?>
			</td>
			<td>
				<?php if($publikasi32!=false){foreach($publikasi32 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiT?></br> <?php }else {}?>
			<?php } } }?>
		</td>
		</tr>  <?php }  } ?>
	</tbody>
</table>
</div>
</body>

<body>
	<div class="table-responsive">   
		<table border="0"  width="100%" >
			<tr>
				<th  align="left">IV.4 Karya Temuan/Inovasi/Paten dan Implementasi Teknologi  Baru</th>
				<th align="right">P6</th>
			</tr>
		</table>  
		<table border="1"  width="100%" >
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">Bulan -Tahun</th>
					<th rowspan="2">Judul /Nama Karya </th>
					<th rowspan="2">Media Publikasi </th>
					<th rowspan="2">Uraian Singkat Materi Makalah/Tulisan </th>
					<th rowspan="2">Kompetensi *)</th>
					<th colspan="4">Nilai</th>
				</tr> 
				<tr>
					<th>P</th>
					<th>Q</th>
					<th>R</th>
					<th>T</th>
				</tr> 
			</thead>
			<tbody>
				<?php if($publikasi41!=false) { $i = 1; foreach($publikasi41 as $row) { ?>
					<tr>
						<th scope="row"><?=$i++?></th>
						<td align="center"><?=$row->aktivitasTahunAwal?></td>
						<td style="padding-left: 10px;"><?=$row->aktivitasNama?></td>
						<td style="padding-left: 10px;"><?=$row->aktivitasLembaga?></td>
						<td style="padding-left: 10px; padding-right: 10px;" align="justify"><?=$row->aktivitasUraian?></td>
						<td style="padding-left: 5px;" align="left">
							<?php if($publikasi42!=false){ foreach($publikasi42 as $name => $data) { foreach($data as $name2 => $value)  { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->uraianKompetensiNama?></br>  <?php }else {}?> <?php } } }?>
						</td>
						<td>
                   <!--  <?php if($publikasi42!=false){ foreach($publikasi42 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId ) { ?> <br><?=$value->kompetensiNilaiP?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                   <!--  <?php if($publikasi42!=false) { foreach($publikasi42 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiQ?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                    <!-- <?php if($publikasi42!=false) { foreach($publikasi42 as $name => $data) {foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiR?></br> <?php }else {}?>
                    <?php } } }?> -->
                </td>
                <td>
                   <!--  <?php if($publikasi42!=false){foreach($publikasi42 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiT?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               </tr>  <?php }  } ?>
           </tbody>
       </table>
   </div>
</body>

<body>
	<div class="table-responsive">   
		<table border="0"  width="100%" >
			<tr>
				<th  align="left">V. Bahasa Yang Dikuasai</th>
				<th align="right">P6</th>
			</tr>
		</table>  
		<table border="1"  width="100%" >
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">Bahasa</th>
					<th rowspan="2">Kemampuan Verbal Aktif/Pasif </th>
					<th rowspan="2">Jenis Tulisan Yang Mampu Disusun </th>
					<th rowspan="2">Nilai TOEFL Atau Yang Sejenis </th>
					<th rowspan="2">Kompetensi *)</th>
					<th colspan="4">Nilai</th>
				</tr> 
				<tr>
					<th>P</th>
					<th>Q</th>
					<th>R</th>
					<th>T</th>
				</tr> 
			</thead>
			<tbody>
				<?php if($bahasa1!=false) { $i = 1; foreach($bahasa1 as $row) { ?>
					<tr>
						<th scope="row"><?=$i++?></th>
						<td align="center"><?=$row->bahasaNama?></td>
						<td align="center"><?=$row->aktivitasUraian?></td>
						<td align="center"><?=$row->aktivitasLembaga?></td>
						<td align="center"><?=$row->aktivitasJumlah?></td>
						<td style="padding-left: 20px;" align="left">
							<?php if($bahasa2!=false){ foreach($bahasa2 as $name => $data) { foreach($data as $name2 => $value)  { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->uraianKompetensiNama?></br>  <?php }else {}?> <?php } } }?>
						</td>
						<td>
                   <!--  <?php if($bahasa2!=false){ foreach($bahasa2 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId ) { ?> <br><?=$value->kompetensiNilaiP?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                  <!--   <?php if($bahasa2!=false) { foreach($bahasa2 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiQ?></br> <?php }else {}?>
                  <?php } } }?> -->
              </td>
              <td>
                   <!--  <?php if($bahasa2!=false) { foreach($bahasa2 as $name => $data) {foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiR?></br> <?php }else {}?>
                   <?php } } }?> -->
               </td>
               <td>
                  <!--   <?php if($bahasa2!=false){foreach($bahasa2 as $name => $data) { foreach($data as $name2 => $value) { ?>  <?php if ($row->aktivitasId == $value->kompetensiAktivitasId) { ?> <br><?=$value->kompetensiNilaiT?></br> <?php }else {}?>
                  <?php } } }?> -->
              </td>
              </tr>  <?php }  } ?>
          </tbody>
      </table>
  </div>
</body>

<body>
	<div class="table-responsive">   
		<table border="0"  width="100%">
			<tr>
				<th align="center" style="padding-bottom: 20px">VII. PERNYATAAN</th>
			</tr>
		</table>  
		<table border="0"  width="100%">
			<thead>
				<tr>
					<td>
						
					</td>
					<td>
						<p>Dengan ini saya menyatakan bahwa seluruh keterangan di atas adalah benar. Bersama ini saya lampirkan fotokopi dokumen pendukung yang dapat membantu penilaian. Apabila perlu saya bersedia diundang untuk wawancara dengan Majelis Penilai PII.</p>
					</td>
				</tr>
			</thead>
		</table>
		<table border="0"  width="100%">
			<tbody >
				<tr>
					
					<td width="33%"></td>
					<td width="33%"></td>
					<td align="center">Samarinda, <?=date_indo(date('Y-m-d',strtotime($tgl!=false?$tgl->tanggal:'')))?></td>
				</tr>
				<tr>
					
					<td width="33%"></td>
					<td width="33%"></td>
					<td align="center"style="padding-top: 50px; font-size: 10px; padding-left: 50px; padding-right: 200px">Materai Rp10.000</td>
				</tr>
				<tr>
					
					<td width="33%"></td>
					<td width="33%"></td>
					<td align="center" style="padding-top: 50px">(<?=$insinyur->fdpNama?>)</td>
				</tr>
			</tbody>
		</table>
	</div>
</body>