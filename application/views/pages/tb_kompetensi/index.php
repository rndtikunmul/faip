<?php $this->load->view('layouts/subheader'); ?>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title"> <?= strtoupper($page_judul) ?></h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <form class="kt-form" action="<?= $response_url ?>" method="post" id="form_show">

                        <?php
                        if ($susrSgroupNama != 'Mhs') {
                            ?>
                            <?php ?>
                                <div class="form-group">
                                    <label>Insinyur</label>
                                    <select class="form-control m-select2" name="fdpNoKta" id="pegmNIP">
                                        <option value="">Pilih</option>
                                        <?php
                                        if ($fdpNoKta != false) {
                                            foreach ($fdpNoKta as $row) {
                                                echo '<option value="' . $row->fdpNim . '">' . $row->fdpNim . ' - ' . $row->fdpNama.'</option>';
                                            }
                                        }
                                        ?>
                                    </select>

                                    <label>Formulir</label>
                                    <select class="form-control m-select2" name="matakuliah" id="">
                                        <option value="">Pilih</option>
                                        <option value="I.2">I.2 PENDIDIKAN FORMAL</option>
                                        <option value="I.3">I.3 ORGANISASI PROFESI & ORGANISASI LAINNYA YANG DIMASUKI </option>
                                        <option value="I.4">I.4 TANDA PENGHARGAAN YANG DITERIMA </option>
                                        <option value="I.5">I.5 PENDIDIKAN/PELATIHAN TEKNIK/PERTANIAN & PROFESI KEINSINYURAN YANG DIIKUTI</option>
                                        <option value="I.6">I.6 PENDIDIKAN/PELATIHAN MANAJEMEN DAN BIDANG LAINNYA (YANG RELEVAN) YANG DIIKUTI </option>
                                        <option value="II.1"> II.1 REFERENSI KODE ETIK DAN ETIKA PROFESI </option>
                                        <option value="II.2">II.2 PENGERTIAN, PENDAPAT DAN PENGALAMAN SENDIRI </option>
                                         <option value="III.1">III.1 PENGALAMAN DALAM PERENCANAAN & PERANCANGAN </option>
                                        <option value="III.2">III.2 PENGALAMAN MENGAJAR PELAJARAN KEINSINYURAN </option>
                                        <option value="III.3">III.3 PENGALAMAN DALAM PENELITIAN </option>
                                        <option value="III.4">III.4 PENGALAMAN DALAM KONSULTANSI PEREKAYASAAN </option>
                                        <option value="III.5">III.5 PENGALAMAN DALAM MANAJEMEN USAHA DAN PEMASARAN </option>
                                         <option value="IV.1">IV.1 KARYA TULIS DI BIDANG KEINSINYURAN YANG DIPUBLIKASIKAN </option>
                                        <option value="IV.2">IV.2 MAKALAH/TULISAN YANG DISAJIKAN DALAM SEMINAR </option>
                                        <option value="IV.3">IV.3 SEMINAR/LOKAKARYA KEINSINYURAN YANG DIIKUTI </option>
                                        <option value="IV.4">IV.4 KARYA TEMUAN/INOVASI/PATEN DAN IMPLEMENTASI TEKNOLOGI BARU </option>
                                        <option value="V">V. BAHASA YANG DIKUASAI</option>
                                        
                                    </select>


                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="form-group">
                                    <label>Insinyur</label>
                                    <input type="text" readonly value="<?= $susrSgroupNama_ori ?>" class="form-control" name="fdpNoKta" />
                                </div>
                                <?php
                            }
                            ?>


                        </div>
                        <div class="kt-portlet__foot">
                            <div class="kt-form__actions">
                                <button type="submit" id="btn_save" class="btn btn-primary">Show</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <div id="response"></div>