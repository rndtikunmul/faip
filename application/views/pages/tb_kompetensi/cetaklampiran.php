<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="UTF-8">

	<title>Cetak Lampiran</title>

	<style>

		table, th, td {
			font-size: 12pt;
			font-style: normal;
			border-collapse: collapse;
		}

	</style>

</head>

<body>
	<div class="table-responsive">   
		<table border="0"  width="70%" >
			<tr>
				<th align="center">LAMPIRAN  I (Diisi cukup 1 lampiran) </th>
				
			</tr>
		</table>  
		<table border="1" >
			<thead>
				
				
				
			</thead>
			<tbody>
				<tr>
					<td width= "15%" colspan="2" ></td>
					<td height="5%" width= "40%" colspan="3" style="padding-left: 50px">Lembar ini adalah Lampiran Pelengkap Aktifitas  No. :</td>
				
					<td width= "30%"  ><?=$lampiran->lampiranAktifitas?></td>

				</tr>

				<tr>
					<td  align="center" colspan="6" >LEMBAR DOKUMENTASI PENGALAMAN KERJA</td>
					
				</tr>
				
				<tr>
					<td  style="padding-left: 5px">Nama</td>
					<td  style="padding-left: 5px ">:</td>
					<td  colspan="4" ><?=$insinyur->fdpNama?></td>
				</tr>
				<tr>
					<td  style="padding-left: 5px">Nama Proyek</td>
					<td style="padding-left: 5px">:</td>
					<td colspan="4" ><?=$lampiran->lampiranNamaProyek?></td>
				</tr>
					<tr>
					<td  style="padding-left: 5px">Jangka Waktu Proyek </td>
					<td style="padding-left: 5px">:</td>
					<td colspan="4" ><?=$lampiran->lampiranWaktu?></td>
				</tr>
					<tr>
					<td  style="padding-left: 5px">Nama Atasan/Pengawas/Supervisor</td>
					<td  style="padding-left: 5px">:</td>
					<td colspan="4"><?=$lampiran->lampiranAtasan?></td>
				</tr>
				<tr>
					<td height="3%" colspan="6" style="padding-left: 5px"></td>
					
				</tr>
					<tr>
					<td height="20%" colspan="6"  style="padding-top: 0px">Uraian tugas yang dilaksanakan :
						<br>
						<?=$lampiran->lampiranUraian?>
					</td>
					
				</tr>

					<tr>
					<td height="20%" colspan="6" style="padding-left: 5px">Putusan keinsinyuran yang diambil (kalau ada) :
						<br>
						<?=$lampiran->lampiranPutusan?>
					</td>
					
				</tr>
				<tr>
					<td height="3%" colspan="6" style="padding-left: 5px"></td>
					
				</tr>
					<tr>
					<td height="20%" colspan="6" style="padding-left: 5px">Bagan organisasi yang menunjukkan posisi dan pertanggungjawaban Anda (Bila perlu pergunakan lembar kertas baru) :
						<br>
						<img src="public/assets/berkas/<?=$lampiran!=false?$lampiran->lampiranBagan:''?>" width="150px" height="140px" align="center" />
					</td>
					
				</tr>
				
			
			</tbody>
		</table>
  </div>
</body>
