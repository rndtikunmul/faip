<?php $this->load->view('layouts/subheader'); ?>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?> <span class="text-danger">*Penilai Hanya Mengisi Nilai P,Q,R</span>AAAAAAAAAAAA
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                         <div class="kt-portlet__head-actions">
                            <a href="<?=$cetaknilai_url.$nip?>" target="_blank" class="btn btn-outline-primary">
                                <span>
                                    <i class="flaticon-graphic-1"></i>
                                    <span>TotalNilai</span>
                                </span>
                            </a>

                             <a href="<?=$cetakformulir_url.$nip?>" target="_blank" class="btn btn-outline-primary">
                                <span>
                                  <i class="flaticon2-image-file kt-font-succes"></i>
                                    <span>Cetak Formulir</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="kt-portlet__body">

                    <div class="kt-section">
                        <div class="kt-section__content">

                            <!--begin::Card-->
                            <div class="card card-custom">
                                <!--begin::Header-->
                                <div class="card-header card-header-tabs-line">
                                    <ul class="nav nav-dark nav-bold nav-tabs nav-tabs-line" data-remember-tab="tab_id" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#kt_builder_themes">Kode Etik</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#kt_builder_page">Profesionalisme</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#kt_builder_header">K3LH</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#kt_builder_subheader">Seminar</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#kt_builder_content">Studi Kasus</a>
                                        </li>
                                    </ul>
                                </div>
                                <!--end::Header-->
                                <!--begin::Form-->
                                <form class="kt-form" action="<?=$save_url?>" method="post" id="form_custom" >
                                    <!--begin::Body-->
                                    <div class="card-body">
                                        <div class="tab-content pt-3">
                                            <!--begin::Tab Pane-->
                                            <div class="tab-pane active" id="kt_builder_themes">
                                                <div class="kt-section__content">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover myTable">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <!-- <th>No</th> -->
                                                                    <th>Periode/ Instansi/Perusahaan, Organisasi, Lokasi Tempat Kegiatan/ berkas </th>
                                                                    <th>Uraian Kegiatan</th>
                                                                    <th>Uraian Kompetensi</th>
                                                                    <th>Nilai P</th>
                                                                    <th>Nilai Q</th>
                                                                    <th>Nilai R</th>
                                                                    <th>Nilai T</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                if($datasnilai!=false)
                                                                {
                                                                    $i = 1;
                                                                    foreach($datasnilai as $row)
                                                                    {
                                                                        if (!empty($row->kompetensiId))
                                                                            $cek = "checked";
                                                                        else
                                                                            $cek="";
                                                                        $key = $this->encryptions->encode($row->kompetensiId,$this->config->item('encryption_key'));
                                                                        ?>
                                                                        <tr>
                                                                         <!--  <td><?=$row->aktivitasId==$row->kompetensiAktivitasId?$i++:''?></td> -->

                                                                            <td>
                                                                               
                                                                 <?php if($row->aktivitasIdForm=="II.1"){?>  <?php } else { ?> 
                                                                    <?=$row->aktivitasTahunAwal?> 
                                                                         <?php if($row->aktivitasTahunAkhir==true){?> - <?=$row->aktivitasTahunAkhir?> <?php } else{ } ?> 
                                                                 <?php } ?> 

                                                                            <br>
                                                                            <?=$row->aktivitasNama?>
                                                                            <br>
                                                                            <?=$row->aktivitasJumlah?>
                                                                            <br>
                                                                            <a class="red" href="<?php echo base_url();?>public/assets/berkas/<?=$row->aktivitasBerkas?>" target="_blank"><i class="flaticon-file-1"></i></a>

                                                                       
                                                                        </td>
                                                                        <td><?=$row->aktivitasUraian?></td>
                                                                        <td><?=$row->uraianKompetensiNama?> <?=$row->uraianKompetensiKet?></td>
                                                                     

                                                                       <input type="hidden" name="cekKomp[]" value="<?=$row->kompetensiId?>" />
                                                                        <td width="85"><input type="text" class="form-control" class="checked" name="kompetensiNilaiP[<?=$row->kompetensiId?>]" placeholder="P" aria-describedby="" value="<?=$row->kompetensiNilaiP?>"></td>
                                                                        <td width="85" ><input type="text" class="form-control" name="kompetensiNilaiQ[<?=$row->kompetensiId?>]" placeholder="Q" aria-describedby="" value="<?=$row->kompetensiNilaiQ?>"></td>
                                                                        <td width="85"><input type="text" class="form-control" name="kompetensiNilaiR[<?=$row->kompetensiId?>]" placeholder="R" aria-describedby="" value="<?=$row->kompetensiNilaiR?>"></td>
                                                                           <td width="85"><input type="text" class="form-control" readonly name="kompetensiNilaiT[<?=$row->kompetensiId?>]" placeholder="R" aria-describedby="" value="<?=$row->kompetensiNilaiT?>"></td>
                                                                        <td>
                                                                            <?php if($ref_revisi == true) {?>
                                                                                <?php if ($ref_revisi->revisiket == 1 and $susrSgroupNama == 'Dosen') { ?>
                                                                                 <a href="<?=$update_url.$key?>" title="Update" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                                                    <span>
                                                                                        <i class="fa fa-pencil-alt"></i>
                                                                                    </span>
                                                                                </a>
                                                                                <a href="<?=$delete_url.$key?>" title="Delete" id='ts_remove_row<?= $i; ?>' class="ts_remove_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                                                    <span>
                                                                                        <i class="fa fa-trash-alt"></i>
                                                                                    </span>
                                                                                </a>

                                                                            <?php  } } else{?> 
                                                                             <a href="<?=$update_url.$key?>" title="Update" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                                                <span>
                                                                                    <i class="fa fa-pencil-alt"></i>
                                                                                </span>
                                                                            </a>
                                                                            <a href="<?=$delete_url.$key?>" title="Delete" id='ts_remove_row<?= $i; ?>' class="ts_remove_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                                                <span>
                                                                                    <i class="fa fa-trash-alt"></i>
                                                                                </span>
                                                                            </a>

                                                                        <?php }?>
                                                                    </td>
                                                                </tr>


                                                            <?php } } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end::Tab Pane-->
                                        <!--begin::Tab Pane-->
                                        <div class="tab-pane" id="kt_builder_page">

                                            <div class="table-responsive">
                                                <table class="table table-hover myTable">
                                                    <thead class="thead-light">
                                                        <tr>
                                                            <!-- <th>No</th> -->
                                                            <th>Periode/ Instansi/Perusahaan, Organisasi, Lokasi Tempat Kegiatan/ berkas </th>
                                                            <th>Uraian Kegiatan</th>
                                                            <th>Uraian Kompetensi</th>
                                                            <th>Nilai P</th>
                                                            <th>Nilai Q</th>
                                                            <th>Nilai R</th>
                                                            <th>Nilai T</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        if($datasnilai2!=false)
                                                        {
                                                            $i = 1;
                                                            foreach($datasnilai2 as $row)
                                                            {
                                                                if (!empty($row->kompetensiId))
                                                                    $cek = "checked";
                                                                else
                                                                    $cek="";
                                                                $key = $this->encryptions->encode($row->kompetensiId,$this->config->item('encryption_key'));
                                                                ?>
                                                                <tr>
                                                                    <!-- <th scope="row"><?=$i++?></th> -->
                                                                    <td>
                                                                         <?php if($row->aktivitasIdForm=="I.5"){?> 
                                                                            <?=date('d-m-Y ',strtotime ($row->aktivitasPeriode))?> s/d <?=date('d-m-Y ',strtotime ($row->aktivitasPeriodeAkhir))?>
                                                                          <?php } else { ?> 
                                                                        <?=$row->aktivitasTahunAwal?> 
                                                                         <?php if($row->aktivitasTahunAkhir==true){?> - <?=$row->aktivitasTahunAkhir?> <?php } else{ } ?> 
                                                                 <?php } ?> 
                                                                    <br>
                                                                    <?=$row->aktivitasNama?>
                                                                    <br>
                                                                    <?=$row->aktivitasJumlah?>
                                                                    <br>
                                                                    <a class="red" href="<?php echo base_url();?>public/assets/berkas/<?=$row->aktivitasBerkas?>" ><i class="flaticon-file-1"></i></a>

                                                                </td>
                                                                <td><?=$row->aktivitasUraian?></td>
                                                                <td><?=$row->uraianKompetensiNama?> <?=$row->uraianKompetensiKet?></td>


                                                              <input type="hidden" name="cekKomp[]" value="<?=$row->kompetensiId?>" />
                                                                <td width="85"><input type="text" class="form-control" class="checked" name="kompetensiNilaiP[<?=$row->kompetensiId?>]" placeholder="P" aria-describedby="" value="<?=$row->kompetensiNilaiP?>"></td>
                                                                <td width="85"><input type="text" class="form-control" name="kompetensiNilaiQ[<?=$row->kompetensiId?>]" placeholder="Q" aria-describedby="" value="<?=$row->kompetensiNilaiQ?>"></td>
                                                                <td width="85"><input type="text" class="form-control" name="kompetensiNilaiR[<?=$row->kompetensiId?>]" placeholder="R" aria-describedby="" value="<?=$row->kompetensiNilaiR?>"></td>
                                                                  <td width="85"><input type="text" class="form-control" readonly name="kompetensiNilaiT[<?=$row->kompetensiId?>]" placeholder="R" aria-describedby="" value="<?=$row->kompetensiNilaiT?>"></td>
                                                                <td>
                                                                     <?php if($ref_revisi == true) {?>
                                                                                <?php if ($ref_revisi->revisiket == 1 and $susrSgroupNama == 'Dosen') { ?>
                                                                                 <a href="<?=$update_url.$key?>" title="Update" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                                                    <span>
                                                                                        <i class="fa fa-pencil-alt"></i>
                                                                                    </span>
                                                                                </a>
                                                                                <a href="<?=$delete_url.$key?>" title="Delete" id='ts_remove_row<?= $i; ?>' class="ts_remove_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                                                    <span>
                                                                                        <i class="fa fa-trash-alt"></i>
                                                                                    </span>
                                                                                </a>

                                                                            <?php  } } else{?> 
                                                                             <a href="<?=$update_url.$key?>" title="Update" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                                                <span>
                                                                                    <i class="fa fa-pencil-alt"></i>
                                                                                </span>
                                                                            </a>
                                                                            <a href="<?=$delete_url.$key?>" title="Delete" id='ts_remove_row<?= $i; ?>' class="ts_remove_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                                                <span>
                                                                                    <i class="fa fa-trash-alt"></i>
                                                                                </span>
                                                                            </a>

                                                                        <?php }?>
                                                                </td>

                                                        </tr>
                                                    <?php } }  ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!--end::Tab Pane-->
                                    <!--begin::Tab Pane-->
                                    <div class="tab-pane" id="kt_builder_header">

                                        <div class="table-responsive">
                                            <table class="table table-hover myTable">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <!-- <th>No</th> -->
                                                        <th>Periode/ Instansi/Perusahaan, Organisasi, Lokasi Tempat Kegiatan/ berkas </th>
                                                        <th>Uraian Kegiatan</th>
                                                        <th>Uraian Kompetensi</th>
                                                        <th>Nilai P</th>
                                                        <th>Nilai Q</th>
                                                        <th>Nilai R</th>
                                                        <th>Nilai T </th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if($datasnilai3!=false)
                                                    {
                                                        $i = 1;
                                                        foreach($datasnilai3 as $row)
                                                        {
                                                            if (!empty($row->kompetensiId))
                                                                $cek = "checked";
                                                            else
                                                                $cek="";
                                                            $key = $this->encryptions->encode($row->aktivitasId,$this->config->item('encryption_key'));
                                                            ?>
                                                            <tr>
                                                                <!-- <th scope="row"><?=$i++?></th> -->
                                                                <td><!-- <?=$row->aktivitasTahunAwal?>
                                                                <br>
                                                                <?=$row->aktivitasNama?>
                                                                <br>
                                                                <?=$row->aktivitasJumlah?>
                                                                <br> -->
                                                                <a class="red" href="<?php echo base_url();?>public/assets/berkas/<?=$row->aktivitasBerkas?>" ><i class="flaticon-file-1"></i></a>

                                                            </td>
                                                            <td><?=$row->aktivitasUraian?></td>
                                                            <td><?=$row->uraianKompetensiNama?> <?=$row->uraianKompetensiKet?></td>


                                                           <input type="hidden" name="cekKomp[]" value="<?=$row->kompetensiId?>" />
                                                            <td width="85"><input type="text" class="form-control" class="checked" name="kompetensiNilaiP[<?=$row->kompetensiId?>]" placeholder="P" aria-describedby="" value="<?=$row->kompetensiNilaiP?>"></td>
                                                            <td width="85"><input type="text" class="form-control" name="kompetensiNilaiQ[<?=$row->kompetensiId?>]" placeholder="Q" aria-describedby="" value="<?=$row->kompetensiNilaiQ?>"></td>
                                                            <td width="85"><input type="text" class="form-control" name="kompetensiNilaiR[<?=$row->kompetensiId?>]" placeholder="R" aria-describedby="" value="<?=$row->kompetensiNilaiR?>"></td>
                                                              <td width="85"><input type="text" class="form-control" readonly name="kompetensiNilaiT[<?=$row->kompetensiId?>]" placeholder="R" aria-describedby="" value="<?=$row->kompetensiNilaiT?>"></td>
                                                             <td>
                                                                            <?php if($ref_revisi == true) {?>
                                                                                <?php if ($ref_revisi->revisiket == 1 and $susrSgroupNama == 'Dosen') { ?>
                                                                                 <a href="<?=$update_url.$key?>" title="Update" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                                                    <span>
                                                                                        <i class="fa fa-pencil-alt"></i>
                                                                                    </span>
                                                                                </a>
                                                                                <a href="<?=$delete_url.$key?>" title="Delete" id='ts_remove_row<?= $i; ?>' class="ts_remove_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                                                    <span>
                                                                                        <i class="fa fa-trash-alt"></i>
                                                                                    </span>
                                                                                </a>

                                                                            <?php  } } else{?> 
                                                                             <a href="<?=$update_url.$key?>" title="Update" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                                                <span>
                                                                                    <i class="fa fa-pencil-alt"></i>
                                                                                </span>
                                                                            </a>
                                                                            <a href="<?=$delete_url.$key?>" title="Delete" id='ts_remove_row<?= $i; ?>' class="ts_remove_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                                                <span>
                                                                                    <i class="fa fa-trash-alt"></i>
                                                                                </span>
                                                                            </a>

                                                                        <?php }?>
                                                                    </td>


                                                        </tr>
                                                    <?php } }  ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!--end::Tab Pane-->
                                    <!--begin::Tab Pane-->
                                    <div class="tab-pane" id="kt_builder_subheader">

                                        <div class="table-responsive">
                                            <table class="table table-hover myTable">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <!-- <th>No</th> -->
                                                        <th>Periode/ Instansi/Perusahaan, Organisasi, Lokasi Tempat Kegiatan/ berkas </th>
                                                        <th>Uraian Kegiatan</th>
                                                        <th>Uraian Kompetensi</th>
                                                        <th>Nilai P</th>
                                                        <th>Nilai Q</th>
                                                        <th>Nilai R</th>
                                                        <th>Nilai T </th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if($datasnilai4!=false)
                                                    {
                                                        $i = 1;
                                                        foreach($datasnilai4 as $row)
                                                        {
                                                            if (!empty($row->kompetensiId))
                                                                $cek = "checked";
                                                            else
                                                                $cek="";
                                                            $key = $this->encryptions->encode($row->aktivitasId,$this->config->item('encryption_key'));
                                                            ?>
                                                            <tr>
                                                                <!-- <th scope="row"><?=$i++?></th> -->
                                                                <td><?=$row->aktivitasTahunAwal?>
                                                                <br>
                                                                <?=$row->aktivitasNama?>
                                                                <br>
                                                                <?=$row->aktivitasJumlah?>
                                                                <br>
                                                                <a class="red" href="<?php echo base_url();?>public/assets/berkas/<?=$row->aktivitasBerkas?>" ><i class="flaticon-file-1"></i></a>

                                                            </td>
                                                            <td><?=$row->aktivitasUraian?></td>
                                                            <td><?=$row->uraianKompetensiNama?> <?=$row->uraianKompetensiKet?></td>


                                                            <input type="hidden" name="cekKomp[]" value="<?=$row->kompetensiId?>" />
                                                            <td width="85"><input type="text" class="form-control" class="checked" name="kompetensiNilaiP[<?=$row->kompetensiId?>]" placeholder="P" aria-describedby="" value="<?=$row->kompetensiNilaiP?>"></td>
                                                            <td width="85"><input type="text" class="form-control" name="kompetensiNilaiQ[<?=$row->kompetensiId?>]" placeholder="Q" aria-describedby="" value="<?=$row->kompetensiNilaiQ?>"></td>
                                                            <td width="85"><input type="text" class="form-control" name="kompetensiNilaiR[<?=$row->kompetensiId?>]" placeholder="R" aria-describedby="" value="<?=$row->kompetensiNilaiR?>"></td>
                                                              <td width="85"><input type="text" class="form-control" readonly name="kompetensiNilaiT[<?=$row->kompetensiId?>]" placeholder="R" aria-describedby="" value="<?=$row->kompetensiNilaiT?>"></td>
                                                             <td>
                                                                            <?php if($ref_revisi == true) {?>
                                                                                <?php if ($ref_revisi->revisiket == 1 and $susrSgroupNama == 'Dosen') { ?>
                                                                                 <a href="<?=$update_url.$key?>" title="Update" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                                                    <span>
                                                                                        <i class="fa fa-pencil-alt"></i>
                                                                                    </span>
                                                                                </a>
                                                                                <a href="<?=$delete_url.$key?>" title="Delete" id='ts_remove_row<?= $i; ?>' class="ts_remove_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                                                    <span>
                                                                                        <i class="fa fa-trash-alt"></i>
                                                                                    </span>
                                                                                </a>

                                                                            <?php  } } else{?> 
                                                                             <a href="<?=$update_url.$key?>" title="Update" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                                                <span>
                                                                                    <i class="fa fa-pencil-alt"></i>
                                                                                </span>
                                                                            </a>
                                                                            <a href="<?=$delete_url.$key?>" title="Delete" id='ts_remove_row<?= $i; ?>' class="ts_remove_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                                                <span>
                                                                                    <i class="fa fa-trash-alt"></i>
                                                                                </span>
                                                                            </a>

                                                                        <?php }?>
                                                                    </td>


                                                        </tr>
                                                    <?php } }  ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!--end::Tab Pane-->
                                    <!--begin::Tab Pane-->
                                    <div class="tab-pane" id="kt_builder_content">

                                        <div class="table-responsive">
                                            <table class="table table-hover myTable">
                                                <thead class="thead-light">
                                                    <tr>
                                                        <!-- <th>No</th> -->
                                                        <th>Periode/ Instansi/Perusahaan, Organisasi, Lokasi Tempat Kegiatan/ berkas </th>
                                                        <th>Uraian Kegiatan</th>
                                                        <th>Uraian Kompetensi</th>
                                                        <th>Nilai P</th>
                                                        <th>Nilai Q</th>
                                                        <th>Nilai R</th>
                                                        <th>Nilai T</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if($datasnilai5!=false)
                                                    {
                                                        $i = 1;
                                                        foreach($datasnilai5 as $row)
                                                        {
                                                            if (!empty($row->kompetensiId))
                                                                $cek = "checked";
                                                            else
                                                                $cek="";
                                                            $key = $this->encryptions->encode($row->aktivitasId,$this->config->item('encryption_key'));
                                                            ?>
                                                            <tr>
                                                                <!-- <th scope="row"><?=$i++?></th> -->
                                                                <td><?=$row->aktivitasTahunAwal?> 
                                                                         <?php if($row->aktivitasTahunAkhir==true){?> - <?=$row->aktivitasTahunAkhir?> <?php } else{ } ?> 
                                                                <br>
                                                                <?=$row->aktivitasNama?>
                                                                <br>
                                                                <?=$row->aktivitasJumlah?>
                                                                <br>
                                                                <a class="red" href="<?php echo base_url();?>public/assets/berkas/<?=$row->aktivitasBerkas?>" ><i class="flaticon-file-1"></i></a>

                                                            </td>
                                                            <td><?=$row->aktivitasUraian?></td>
                                                            <td><?=$row->uraianKompetensiNama?> <?=$row->uraianKompetensiKet?></td>


                                                          <input type="hidden" name="cekKomp[]" value="<?=$row->kompetensiId?>" />
                                                            <td width="80"><input type="text" class="form-control" class="checked" name="kompetensiNilaiP[<?=$row->kompetensiId?>]" placeholder="P" aria-describedby="" value="<?=$row->kompetensiNilaiP?>"></td>
                                                            <td width="80"><input type="text" class="form-control" name="kompetensiNilaiQ[<?=$row->kompetensiId?>]" placeholder="Q" aria-describedby="" value="<?=$row->kompetensiNilaiQ?>"></td>
                                                            <td width="80"><input type="text" class="form-control" name="kompetensiNilaiR[<?=$row->kompetensiId?>]" placeholder="R" aria-describedby="" value="<?=$row->kompetensiNilaiR?>"></td>
                                                              <td width="85"><input type="text" class="form-control" readonly name="kompetensiNilaiT[<?=$row->kompetensiId?>]" placeholder="R" aria-describedby="" value="<?=$row->kompetensiNilaiT?>"></td>
                                                             <td>
                                                                            <?php if($ref_revisi == true) {?>
                                                                                <?php if ($ref_revisi->revisiket == 1 and $susrSgroupNama == 'Dosen') { ?>
                                                                                 <a href="<?=$update_url.$key?>" title="Update" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                                                    <span>
                                                                                        <i class="fa fa-pencil-alt"></i>
                                                                                    </span>
                                                                                </a>
                                                                                <a href="<?=$delete_url.$key?>" title="Delete" id='ts_remove_row<?= $i; ?>' class="ts_remove_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                                                    <span>
                                                                                        <i class="fa fa-trash-alt"></i>
                                                                                    </span>
                                                                                </a>

                                                                            <?php  } } else{?> 
                                                                             <a href="<?=$update_url.$key?>" title="Update" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                                                <span>
                                                                                    <i class="fa fa-pencil-alt"></i>
                                                                                </span>
                                                                            </a>
                                                                            <a href="<?=$delete_url.$key?>" title="Delete" id='ts_remove_row<?= $i; ?>' class="ts_remove_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                                                <span>
                                                                                    <i class="fa fa-trash-alt"></i>
                                                                                </span>
                                                                            </a>

                                                                        <?php }?>
                                                                    </td>


                                                        </tr>
                                                    <?php } }  ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!--end::Tab Pane-->

                                    <?php if($ref_revisi == true) {?>
                                        <?php if ($ref_revisi->revisiket == 1 and $susrSgroupNama == 'Dosen') { ?>
                                            <div class="kt-portlet__foot">
                                                <div class="kt-form__actions">
                                                    <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                                </div>
                                            </div>

                                        <?php  } } else{?> 
                                            <div class="kt-portlet__foot">
                                                <div class="kt-form__actions">
                                                    <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                                </div>
                                            </div>


                                        <?php }?>

                                        <!--end::Form-->
                                    </div>    
                                </div>
                            </div>

                            <!--end::Section-->
                        </div>
                    </div>

                    <!--end::Portlet-->
                </div>
            </div>
        </div>
        <!--End::Row-->
