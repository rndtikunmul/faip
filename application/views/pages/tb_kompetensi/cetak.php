<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="UTF-8">

    <title>Cetak Nilai</title>

    <style>
    table, th, td {
      font-size: 10pt;
      font-style: normal;
      border-collapse: collapse;
      font : Arial;

  }

  dd {
  color:white
}

</style>

</head>


<body style="font-family:Arial, Helvetica, sans-serif">

  <div align="center" style=" font-weight: bold;">FORMULIR PENILAIAN</div>
  <div align="center" style=" font-weight: bold;">REKOGNISI PEMBELAJARAN LAMPAU</div>
  <br>
  <div class="table-responsive">

    <table class="table table-hover" border="0"  width="100%" >
        <thead >

             <tr>
                <td  height="2%" width="15%">Nomor</td>
                <td width="3%"></td>
                <td colspan="3"></td>
            </tr>
            <tr>
                <td  height="2%"  width="15%">Nama Pemohon</td>
                <td width="3%">:</td>
                <td colspan="3"><?=$insinyur!=false?$insinyur->fdpNama:''?></td>
            </tr>
            <tr>
                <td >No KTA</td>
                <td>:</td>
                <td><?=$insinyur!=false?$insinyur->fdpNoKta:''?></td>
                <td height="2%">  <table border="1" align="right" >
                            <tr >
                                <td height="1%"><dd>.</dd></td>
                            </tr>
                        </table></td>
                <td style=" padding: 5px;"> Permohonan baru</td>
            </tr>
            <tr>
                <td>Badan Kejuruan</td>
                <td>:</td>
                <td><?=$insinyur!=false?$insinyur->fdpBadanKejuruan:''?></td>
                 <td height="3%">  <table border="1" align="right">
                            <tr >
                                <td ><dd>.</dd></td>
                            </tr>
                        </table></td>
                <td style=" padding: 5px;"> Penilaian ulang</td>
            </tr>
             <tr>
                <td  height="2%">Sub Bidang</td>
                <td>:</td>
                <td colspan="3"></td>
            </tr>
            <tr>
                <td  height="2%">Perusahaan</td>
                <td>:</td>
                <td colspan="3"><?=$insinyur!=false?$insinyur->fdpNamaLembaga:''?></td>
            </tr>
            <tr>
                <?php
                if($datas!=false){
                $ipk= 0;    
                foreach($datas as $row){ $ipk += $row->nilaiangka * $row->sks / 24;
                ?><?php }}?>
                <td  height="2%">IPK</td>
                <td>:</td>
                <td colspan="2"><?=$ipk?></td>
            </tr>
         </thead>
        </table>
        <br>

        <table class="table table-hover" border="1" align="center" width="100%" >
            <thead class="thead-light" >
                <tr  style=" font-weight: bold;" align="center" >
                    <td >Mata Kuliah</td>
                    <td >SKS</td>
                    <td colspan="5" align="center">Ringkasan Bakuan Kompetensi</td>   
                    <td >Nilai</td>
                    <td >Nilai Angka</td>
                    <td >Nilai RPL</td>
                </tr>
                <tr style=" font-weight: bold;" align="center">
                    <td ></td>
                    <td></td>
                    <td>W1</td>
                    <td>W2</td>
                    <td>W3</td>
                    <td>W4</td>
                    <td>P</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

            </thead>
            <tbody>
                <?php
                if($datas!=false)
                {
                    $totalsks= 0; 
                    $totalW1= 0; 
                    $totalW2= 0; 
                    $totalW3= 0;
                    $totalW4= 0;
                    $totalP= 0; 
                    $totalnilairpl= 0;
                    $i = 1;
                    foreach($datas as $row)
                    {
                        $key = $this->encryptions->encode($row->aktivitasFdpId,$this->config->item('encryption_key'));
                        $totalsks += isset($row->sks)?$row->sks:0;
                        $totalW1 += isset($row->sks)?$row->W1:0;
                        $totalW2 += isset($row->sks)?$row->W2:0;
                        $totalW3 += isset($row->sks)?$row->W3:0;
                        $totalW4 += isset($row->sks)?$row->W4:0;
                        $totalP += isset($row->sks)?$row->P:0;
                        $totalnilairpl += isset($row->sks)?$row->nilairpl:0;

                        ?>
                             
                            <?php if($row->matakuliah=='Studi Kasus & Praktek') { ?>
                             <tr>
                                    <td >Studi Kasus</td>
                                    <td>4</td>
                                    <td rowspan="2"><?=$row->W1?></td>
                                    <td rowspan="2"><?=$row->W2?></td>
                                    <td rowspan="2"><?=$row->W3?></td>
                                    <td rowspan="2"><?=$row->W4?></td>
                                    <td rowspan="2"><?=$row->P?></td>
                                    <td><?=$row->nilaihuruf?></td>
                                    <td><?=$row->nilaiangka?></td>
                                    <td><?=$row->nilairpl?></td> 
                             </tr>
                              <tr>
                                    <td >Praktik Keinsinyuran</td>
                                    <td>12</td> 
                                    <td><?=$row->nilaihuruf?></td>
                                    <td><?=$row->nilaiangka?></td>
                                    <td><?=$row->nilairpl?></td> 
                             </tr>
                             <?php }else{?>
                            <tr>

                                    <td><?=$row->matakuliah?></td>
                                    <td><?=$row->sks?></td>
                                    <td><?=$row->W1?></td>
                                    <td><?=$row->W2?></td>
                                    <td><?=$row->W3?></td>
                                    <td><?=$row->W4?></td>
                                    <td><?=$row->P?></td>
                                    <td><?=$row->nilaihuruf?></td>
                                    <td><?=$row->nilaiangka?></td>
                                    <td><?=$row->nilairpl?></td>
                            </tr>
                            <?php   }  ?>
                        <?php
                    }
                }
                ?>
            </tbody>
            <tr>
                <td >Total</td>
                <td><?=$totalsks?></td>
                <td><?=$totalW1?></td>
                <td><?=$totalW2?></td>
                <td><?=$totalW3?></td>
                <td><?=$totalW4?></td>
                <td><?=$totalP?></td>
                <td></td>
                <td></td>
                <td><?=$totalnilairpl?></td>
            </tr>
            <tr>
                <td >Batasan Min</td>
                <td></td>
                <td>60</td>
                <td>180</td>
                <td>120</td>
                <td>60</td>
                <td>180</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Status</td>
                <td></td>
                <td> <?php if ($totalW1 < 60 ) { ?>
                    kurang
                <?php }else { ?>
                    OK
                <?php } ?>
            </td>
            <td>
                <?php if ($totalW2 < 180 ) { ?>
                    kurang
                <?php }else { ?>
                    OK
                <?php } ?>
            </td>
            <td>
                <?php if ($totalW3 < 120 ) { ?>
                    kurang
                <?php }else { ?>
                    OK
                <?php } ?>
            </td>
            <td>
                <?php if ($totalW4 < 60 ) { ?>
                    kurang
                <?php }else { ?>
                    OK
                <?php } ?>
            </td>
            <td>
                <?php if ($totalP < 180 ) { ?>
                    kurang
                <?php }else { ?>
                    OK
                <?php } ?>
            </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <br>

 <table class="table table-hover" border="0" align="center" width="100%">
    <thead class="thead-light" >
        <tr>
            <td colspan="10" align="center" >KEPUTUSAN</td>
        </tr>
        <tr>
            <td colspan="10" align="center" >
                <?php if (($totalW1 >= 60) AND  ($totalW2 >=180 ) AND ($totalW3 >=120 ) AND ($totalW4 >= 60 ) AND ($totalP >= 180)  ) { ?>
                    LULUS
                <?php }else { ?>
                    TIDAK LULUS
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td colspan="10" align="center" height="40" ></td>

        </tr>

        <tr>
            <td colspan="10" align="center" ><?=$tanggal->tanggalKet?>, <?=date_indo ($tanggal->tanggal)?></td>

        </tr>
        <tr>
            <td colspan="10" align="center" height="60" ></td>

        </tr>
        <tr>

            <td colspan="10" align="center" ><?=$ttd!=false?$ttd->susrNama:''?>
        </td>

    </tr>
    <tr>
        <td colspan="10" align="center" >NIP.<?=$ttd!=false?$ttd->susrProfil:''?></td>
    </tr>

</thead >
</table>
                    </div>

                </body>
