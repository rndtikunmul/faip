<?php $this->load->view('layouts/subheader'); ?>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?> <span class="text-danger">*Penilai Hanya Mengisi Nilai P,Q,R</span>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                         <div class="kt-portlet__head-actions">
                            <a href="<?=$cetaknilai_url.$nip?>" target="_blank" class="btn btn-outline-primary">
                                <span>
                                    <i class="flaticon-graphic-1"></i>
                                    <span>TotalNilai</span>
                                </span>
                            </a>

                             <a href="<?=$cetakformulir_url.$nip?>" target="_blank" class="btn btn-outline-primary">
                                <span>
                                  <i class="flaticon2-image-file kt-font-succes"></i>
                                    <span>Cetak Formulir</span>
                                </span>
                            </a>
                             <a href="<?=$cetalampiran_url.$nip?>" target="_blank" class="btn btn-outline-primary">
                                <span>
                                  <i class="flaticon2-image-file kt-font-succes"></i>
                                    <span>Cetak Lampiran</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="card card-custom">
                              <form class="kt-form" action="<?=$save_url?>" method="post" id="form_custom" >
                                    <div class="card-body">
                                        <div class="tab-content pt-3">
                                            <!--begin::Tab Pane-->
                                            <div class="tab-pane active" id="kt_builder_themes">
                                                <div class="kt-section__content">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover myTable">
                                                            <thead class="thead-light">
                                                                <tr>
                                                                    <th>Deskripsi </th>
                                                                    <th>Uraian Kegiatan</th>
                                                                    <th>Uraian Kompetensi</th>
                                                                    <th>Nilai P</th>
                                                                    <th>Nilai Q</th>
                                                                    <th>Nilai R</th>
                                                                    <th>Nilai T</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                    <?php
                                                    if($datasnilai!=false)
                                                    {
                                                        $i = 1;
                                                        foreach($datasnilai as $row)
                                                        {
                                                            if (!empty($row->kompetensiId))
                                                                $cek = "checked";
                                                            else
                                                                $cek="";
                                                            $key = $this->encryptions->encode($row->kompetensiId,$this->config->item('encryption_key'));
                                                            ?>
                                                    <tr>
                                                        <td>

                                                         <?php if($row->aktivitasIdForm=="I.2"){?> 
                                                            Jenjang :<?=$row->jenjangNama?>
                                                            <br>
                                                            Univ : <?=$row->aktivitasNama?>
                                                            <br>
                                                            Fak : <?=$row->aktivitasJumlah?>
                                                            <br>
                                                            Jurusan : <?=$row->aktivitasTahunAwal?>
                                                            <br>
                                                            Lokasi : <?=$row->kotaNama?>-<?=$row->negNama?>
                                                            <br>
                                                            Gelar : <?=$row->aktivitasTahunAkhir?>
                                                            <br>
                                                            Judul TA : <?=$row->aktivitasJudulTa?>
                                                            <br>
                                                            IPK :<?=$row->aktivitasNilai?> 


                                                    <?php }elseif ($row->aktivitasIdForm=="II.1"){?> 
                                                          
                                                            Nama : <?=$row->aktivitasNama?>
                                                            <br>
                                                            Alamat : <?=$row->aktivitasJumlah?>
                                                            <br>
                                                            Telpon : <?=$row->aktivitasJudulTa?>
                                                            <br>
                                                            Hubungan :<?=$row->aktivitasUraian?> 
                                                    <?php }elseif ( $row->aktivitasIdForm=="II.2"){?> 
                                                          
                                                            <?=$row->aktivitasUraian?> 
                                                      <?php }elseif ($row->aktivitasIdForm=="IV.1" or $row->aktivitasIdForm=="IV.4" ){?> 
                                                          
                                                            Karya Tulis : <?=$row->aktivitasNama?>
                                                            <br>
                                                            Media Publikasi/Penyelenggara : <?=$row->aktivitasLembaga?>
                                                            <br>
                                                            Bulan-Tahun : <?=$row->aktivitasTahunAwal?>
                                                            <br>
                                                            Lokasi :<?=$row->aktivitasJumlah?> 
                                                        <?php }elseif ($row->aktivitasIdForm=="IV.2" or $row->aktivitasIdForm=="IV.3"){?> 
                                                          
                                                           Nama Seminar : <?=$row->aktivitasNama?>
                                                            <br>
                                                           Penyelenggara : <?=$row->aktivitasLembaga?>
                                                            <br>
                                                            Bulan-Tahun : <?=$row->aktivitasTahunAwal?>
                                                            <br>
                                                           Makalah :<?=$row->aktivitasJumlah?> 
                                                           <br>
                                                            Lokasi : <?=$row->kotaNama?>-<?=$row->negNama?>
                                                         <?php }elseif ($row->aktivitasIdForm=="V"){?> 
                                                          
                                                          Bahasa : <?=$row->bahasaNama?>
                                                            <br>
                                                           Kemampuan : <?=$row->aktivitasUraian?>
                                                            <br>
                                                            Jenis Tulisan : <?=$row->aktivitasLembaga?>
                                                            <br>
                                                         Nilai Toefl/sejenis:<?=$row->aktivitasJumlah?> 
                                                          
                                                     <?php } else { ?> 

                                                                 <?php if($row->aktivitasIdForm=="I.5" or $row->aktivitasIdForm=="I.6"){?>
                                                                Periode: <?=date('d-m-Y ',strtotime ($row->aktivitasPeriode))?> s/d <?=date('d-m-Y ',strtotime ($row->aktivitasPeriodeAkhir))?> 

                                                                 <?php } else{?> 
                                                                 Periode: <?=$row->aktivitasTahunAwal?> -  <?=$row->aktivitasTahunAkhir?>
                                                                <?php }?> 

                                                       
                                                        <br>
                                                        Organisasi/Nama : <?=$row->aktivitasNama?>
                                                        <br>
                                                        Jabatan/Jumlah jam/Jumlah SKS : <?=$row->aktivitasJumlah?>
                                                        <br>
                                                        Lokasi : <?=$row->kotaNama?>-<?=$row->negNama?>
                                                      
                                                      
                                                     <?php } ?> 
                                                        <br>
                                                        <a class="red" href="<?php echo base_url();?>public/assets/berkas/<?=$row->aktivitasBerkas?>" target="_blank"><i class="flaticon-file-1"></i></a>
                                                        </td>
                                                        <td><?=$row->aktivitasUraian?></td>
                                                        <td><?=$row->uraianKompetensiNama?> <?=$row->uraianKompetensiKet?></td>
                                                                     
                                                       <input type="hidden" name="cekKomp[]" value="<?=$row->kompetensiId?>" />
                                                        <td width="85"><input type="text" class="form-control" class="checked" name="kompetensiNilaiP[<?=$row->kompetensiId?>]" placeholder="P" aria-describedby="" value="<?=$row->kompetensiNilaiP?>"></td>
                                                        <td width="85" ><input type="text" class="form-control" name="kompetensiNilaiQ[<?=$row->kompetensiId?>]" placeholder="Q" aria-describedby="" value="<?=$row->kompetensiNilaiQ?>"></td>
                                                        <td width="85"><input type="text" class="form-control" name="kompetensiNilaiR[<?=$row->kompetensiId?>]" placeholder="R" aria-describedby="" value="<?=$row->kompetensiNilaiR?>"></td>
                                                           <td width="85"><input type="text" class="form-control" readonly  placeholder="T" aria-describedby="" value="<?=$row->kompetensiNilaiT?>"></td>
                                                         <td>
                                                        <?php if($ref_revisi == true) {?>
                                                            <?php if ($ref_revisi->revisiket == 1 and $susrSgroupNama == 'Dosen') { ?>
                                                             <a href="<?=$update_url.$key?>" title="Update" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                                <span>
                                                                    <i class="fa fa-pencil-alt"></i>
                                                                </span>
                                                            </a>
                                                            <a href="<?=$delete_url.$key?>" title="Delete" id='ts_remove_row<?= $i; ?>' class="ts_remove_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                                <span>
                                                                    <i class="fa fa-trash-alt"></i>
                                                                </span>
                                                            </a>

                                                        <?php  } } else{?> 
                                                         <a href="<?=$update_url.$key?>" title="Update" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                            <span>
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </span>
                                                        </a>
                                                        <a href="<?=$delete_url.$key?>" title="Delete" id='ts_remove_row<?= $i; ?>' class="ts_remove_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                            <span>
                                                                <i class="fa fa-trash-alt"></i>
                                                            </span>
                                                        </a>

                                                    <?php }?>
                                                </td>
                                             </tr>
                                                            <?php } } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end::Tab Pane-->
                                     

                                    <?php if($ref_revisi == true) {?>
                                        <?php if ($ref_revisi->revisiket == 1 and $susrSgroupNama == 'Dosen') { ?>
                                            <div class="kt-portlet__foot">
                                                <div class="kt-form__actions">
                                                    <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                                </div>
                                            </div>

                                        <?php  } } else{?> 
                                            <div class="kt-portlet__foot">
                                                <div class="kt-form__actions">
                                                    <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                                </div>
                                            </div>


                                        <?php }?>

                                        <!--end::Form-->
                                    </div>    
                                </div>
                            </form>
                            </div>

                            <!--end::Section-->
                        </div>
                    </div>

                    <!--end::Portlet-->
                </div>
            </div>
        </div>
        <!--End::Row-->
