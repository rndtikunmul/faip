<?php $this->load->view('layouts/subheader'); ?>
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
	<div class="row">
		<div class="col-md-12">
			<div id="response"></div>
			<!--begin::Portlet-->
			<div class="kt-portlet">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							<?=strtoupper($page_judul)?>
						</h3>

					</div>

					<?php if($susrSgroupNama == 'Mhs') {?>
						<div class="kt-portlet__head-actions" style="padding-top: 10px">
							<a href="<?=$cetakformulir_url.$nip?>" class="btn btn-outline-primary" target="_blank">
								<span>
									<i class="flaticon2-image-file kt-font-succes"></i>
									<span>Cetak Formulir</span>
								</span>
							</a>
                      <!--  <a href="<?=$cetaknilai_url.$nip?>" class="btn btn-outline-succes">
                            <span>
                              <i class="flaticon2-image-file kt-font-succes"></i>
                              <span>Cetak Nilai</span>
                          </span>
                      </a> -->
                      <a href="<?=$cetalampiran_url.$nip?>" class="btn btn-outline-success" target="_blank">
                      	<span>
                      		<i class="flaticon2-image-file kt-font-succes"></i>
                      		<span>Cetak Lampiran</span>
                      	</span>
                      </a>
                  </div>
              <?php }else{?>
              <?php } ?>   
          </div>

          <!--begin::Form-->
          <div class="table-responsive">

                   <!--   <table class="table table-hover" border="0"  width="100%" >
                        <thead >

                           <tr>
                                <td colspan="3" align="center" >FORMULIR PENILAIAN</td>
                            <tr>
                            <tr>
                                <td colspan="3" align="center">REKOGNISI PEMBELAJARAN LAMPAU</td>
                            <tr>
                            <tr>
                                <td colspan="3"></td>
                            <tr>
                            <tr>
                                <td width="15%">Nama Pemohon</td>
                                <td width="3%">:</td>
                                <td ><?=$insinyur!=false?$insinyur->fdpNama:''?></td>
                            <tr>
                            <tr>
                                <td>No KTA</td>
                                <td>:</td>
                                <td><?=$insinyur!=false?$insinyur->fdpNoKta:''?></td>
                            <tr>
                            <tr>
                                <td>Badan Kejuruan</td>
                                <td>:</td>
                                <td><?=$insinyur!=false?$insinyur->fdpBadanKejuruan:''?></td>
                            <tr>
                            <tr>
                                <td>Perusahaan</td>
                                <td>:</td>
                                <td><?=$insinyur!=false?$insinyur->fdpNamaLembaga:''?></td>
                            <tr>
                            <tr>
                                 <?php
                            if($datas!=false){
                               $ipk= 0;    
                               foreach($datas as $row){ $ipk += $row->nilaiangka * $row->sks / 24;
                                ?><?php }}?>
                                <td>IPK</td>
                                <td>:</td>
                                <td><?=$ipk?></td>
                            <tr>
                    </table>

                    <table class="table table-hover" border="1" >
                        <thead class="thead-light" >
                        <tr>
                            <td >Mata Kuliah</td>
                            <td >SKS</td>
                            <td colspan="5" align="center">Ringkasan Bakuan Kompetensi</td>   
                            <td >Nilai</td>
                            <td >Nilai Angka</td>
                            <td >Nilai RPL</td>
                        </tr>
                        <tr>
                            <td ></td>
                            <td></td>
                            <td>W1</td>
                            <td>W2</td>
                            <td>W3</td>
                            <td>W4</td>
                            <td>P</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                      
                        </thead>
                        <tbody>
                            <?php
                            if($datas!=false)
                            {
                                $totalsks= 0; 
                                $totalW1= 0; 
                                $totalW2= 0; 
                                $totalW3= 0;
                                $totalW4= 0;
                                $totalP= 0; 
                                $totalnilairpl= 0;
                                $i = 1;
                                foreach($datas as $row)
                                {
                                    $key = $this->encryptions->encode($row->aktivitasFdpId,$this->config->item('encryption_key'));
                                    $totalsks += isset($row->sks)?$row->sks:0;
                                    $totalW1 += isset($row->sks)?$row->W1:0;
                                    $totalW2 += isset($row->sks)?$row->W2:0;
                                    $totalW3 += isset($row->sks)?$row->W3:0;
                                    $totalW4 += isset($row->sks)?$row->W4:0;
                                    $totalP += isset($row->sks)?$row->P:0;
                                    $totalnilairpl += isset($row->sks)?$row->nilairpl:0;
                                  
                                    ?>
                                    <tr>
                                        <td><?=$row->matakuliah?></td>
                                        <td><?=$row->sks?></td>
                                        <td><?=$row->W1?></td>
                                        <td><?=$row->W2?></td>
                                        <td><?=$row->W3?></td>
                                        <td><?=$row->W4?></td>
                                        <td><?=$row->P?></td>
                                        <td><?=$row->nilaihuruf?></td>
                                        <td><?=$row->nilaiangka?></td>
                                        <td><?=$row->nilairpl?></td>

                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                        <tr>
                            <td >Total</td>
                            <td><?=$totalsks?></td>
                            <td><?=$totalW1?></td>
                            <td><?=$totalW2?></td>
                            <td><?=$totalW3?></td>
                            <td><?=$totalW4?></td>
                            <td><?=$totalP?></td>
                            <td></td>
                            <td></td>
                            <td><?=$totalnilairpl?></td>
                        </tr>
                        <tr>
                            <td >Batasan Min</td>
                            <td></td>
                            <td>60</td>
                            <td>180</td>
                            <td>120</td>
                            <td>60</td>
                            <td>180</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                         <tr>
                            <td>Status</td>
                            <td></td>
                            <td> <?php if ($totalW1 < 60 ) { ?>
                                    kurang
                                <?php }else { ?>
                                  OK
                               <?php } ?>
                            </td>
                            <td>
                                <?php if ($totalW2 < 180 ) { ?>
                                    kurang
                                <?php }else { ?>
                                   OK
                               <?php } ?>
                            </td>
                            <td>
                                <?php if ($totalW3 < 120 ) { ?>
                                    kurang
                                <?php }else { ?>
                                   OK
                               <?php } ?>
                            </td>
                            <td>
                                <?php if ($totalW4 < 60 ) { ?>
                                    kurang
                                <?php }else { ?>
                                   OK
                               <?php } ?>
                            </td>
                            <td>
                                <?php if ($totalP < 180 ) { ?>
                                    kurang
                                <?php }else { ?>
                                   OK
                               <?php } ?>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                     <table class="table table-hover" border="0" >
                        <thead class="thead-light" >
                       
                         <tr>
                                <td colspan="10" align="center" >KEPUTUSAN</td>
                        <tr>
                        <tr>
                                <td colspan="10" align="center" >
                                <?php if (($totalW1 >= 60) AND  ($totalW2 >=180 ) AND ($totalW3 >=120 ) AND ($totalW4 >= 60 ) AND ($totalP >= 180)  ) { ?>
                                   LULUS
                                <?php }else { ?>
                                   TIDAK LULUS
                               <?php } ?>
                                </td>
                        <tr>
                             <tr>
                                <td colspan="10" align="center" height="40" ></td>
                              
                        </tr>

                        <tr>
                                <td colspan="10" align="center" ><?=$tanggal->tanggalKet?>,<?=date_indo ($tanggal->tanggal)?></td>
                              
                        <tr>
                        <tr>
                                <td colspan="10" align="center" height="60" ></td>
                              
                        </tr>
                        <tr>
                      
                                <td colspan="10" align="center" ><?=$ttd!=false?$ttd->susrNama:''?>
                            </td>
                               
                        <tr>
                          <tr>
                                <td colspan="10" align="center" >NIP.<?=$ttd!=false?$ttd->susrProfil:''?></td>
                        <tr>


                        </table> -->
                    </div>

                    <!--end::Form-->
                </div>

                <!--end::Portlet-->
            </div>
        </div>
    </div>
    <!--End::Row-->
