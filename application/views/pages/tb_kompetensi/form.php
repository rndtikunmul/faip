
            <!-- BEGIN: Subheader -->
            <?php $this->load->view('layouts/subheader'); ?>
            <!-- END: Subheader -->

            <!--Begin::Row-->
            <!-- begin:: Content -->
            <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div id="response"></div>
                        <!--begin::Portlet-->
                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        <?=strtoupper($page_judul)?>
                                    </h3>
                                </div>
                            </div>

                            <!--begin::Form-->
                            <form class="kt-form" action="<?=$save_url?>" method="post" id="form_form">
                                <div class="kt-portlet__body">
                                    <input type="hidden" name="kompetensiIdOld" value="<?=$datas!=false?$datas->kompetensiId:''?>">
                                    
                              <!--   <div class="form-group">
                                    <label>kompetensiAktivitasId</label>
                                    <select class="form-control m-select2" name="kompetensiAktivitasId">
                                            <option value=""></option>
                                    <?php 
                                    foreach($tb_aktivitas as $row):
                                        echo '<option value="'.$row->aktivitasId.'" ' . ($datas != false ? $datas->aktivitasId == $row->kompetensiAktivitasId ? 'selected' : '' : '') . '>'.$row->aktivitasId.'</option>';
                                    endforeach;
                                    ?>
                                    </select>
                                    
                                </div> -->
                            
                                <div class="form-group">
                                    <label>Uraian Kompetensi</label>
                                    <select class="form-control m-select2" name="kompetensiUraianKompetensiId">
                                            <option value=""></option>
                                    <?php 
                                    foreach($ref_uraiankompetensi as $row):
                                        echo '<option value="'.$row->uraianKompetensiId.'" ' . ($datas != false ? $datas->kompetensiUraianKompetensiId == $row->uraianKompetensiId ? 'selected' : '' : '') . '>'.$row->uraianKompetensiNama. '-'. $row->uraianKompetensiKet. '</option>';
                                    endforeach;
                                    ?>
                                    </select>
                                    
                                </div>
                            
                               <!--  <div class="form-group">
                                    <label>kompetensiNilaiP</label>
                                    <input type="text" class="form-control" name="kompetensiNilaiP" placeholder="kompetensiNilaiP" aria-describedby="kompetensiNilaiP" value="<?=$datas!=false?$datas->kompetensiNilaiP:''?>">
                                </div>
                            
                                <div class="form-group">
                                    <label>kompetensiNilaiQ</label>
                                    <input type="text" class="form-control" name="kompetensiNilaiQ" placeholder="kompetensiNilaiQ" aria-describedby="kompetensiNilaiQ" value="<?=$datas!=false?$datas->kompetensiNilaiQ:''?>">
                                </div>
                            
                                <div class="form-group">
                                    <label>kompetensiNilaiR</label>
                                    <input type="text" class="form-control" name="kompetensiNilaiR" placeholder="kompetensiNilaiR" aria-describedby="kompetensiNilaiR" value="<?=$datas!=false?$datas->kompetensiNilaiR:''?>">
                                </div>
                            
                                <div class="form-group">
                                    <label>kompetensiNilaiT</label>
                                    <input type="text" class="form-control" name="kompetensiNilaiT" placeholder="kompetensiNilaiT" aria-describedby="kompetensiNilaiT" value="<?=$datas!=false?$datas->kompetensiNilaiT:''?>">
                                </div>
                            
                                <div class="form-group">
                                    <label>kompetensiUserPenilai</label>
                                    <input type="text" class="form-control" name="kompetensiUserPenilai" placeholder="kompetensiUserPenilai" aria-describedby="kompetensiUserPenilai" value="<?=$datas!=false?$datas->kompetensiUserPenilai:''?>">
                                </div>
                            
                                <div class="form-group">
                                    <label>kompetensiTimescreat</label>
                                    <input type="text" class="form-control" name="kompetensiTimescreat" placeholder="kompetensiTimescreat" aria-describedby="ko mpetensiTimescreat" value="<?=$datas!=false?$datas->kompetensiTimescreat:''?>">
                                </div>-->
                            
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                                        <button type="reset" class="btn btn-secondary">Cancel</button>
                                    </div>
                                </div>
                            </form>

                            <!--end::Form-->
                        </div>

                        <!--end::Portlet-->
                    </div>
                </div>
            </div>
            <!--End::Row-->
            