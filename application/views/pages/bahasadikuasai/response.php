<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                     <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-actions">
                            <?php if($ref_revisi == true) {?>
                                <?php if ($ref_revisi->revisiket == 1 and $susrSgroupNama == 'Mhs') { ?>
                                    <a href="<?= $create_url.$nip ?>" class="btn btn-outline-primary">
                                        <span><i class="flaticon2-plus"></i><span>Create</span> </span>
                                    </a>
                                <?php } ?> 

                                <?php if ($ref_revisi->revisiket == 1 and $susrSgroupNama == 'Dosen') { ?>
                                    <a href="<?= $nilaikompetensidosen_url.$nip ?>" class="btn btn-outline-primary">
                                        <span><i class="flaticon2-plus"></i><span>Nilai</span> </span>
                                    </a>
                                <?php } ?> 
                            <?php }else{?>
                                <a href="<?= $create_url.$nip ?>" class="btn btn-outline-primary">
                                    <span><i class="flaticon2-plus"></i><span>Create</span> </span>
                                </a>
                                
                            <?php }?>

                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                       <tr>
                                        <th>No</th>
                                        <th>Bahasa</th>
                                        <th>Kemampuan Verbal</th>
                                        <th>Jenis Tulisan</th>
                                        <th>Nilai Toefl/sejenis</th>
                                        <th>Berkas</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if($datas!=false)
                                    {
                                        $i = 1;
                                        foreach($datas as $row)
                                        {
                                            $key = $this->encryptions->encode($row->aktivitasId,$this->config->item('encryption_key'));
                                            ?>
                                            <tr>
                                                <th scope="row"><?=$i++?></th>
                                                <td><?=$row->bahasaNama?></td>
                                                <td><?=$row->aktivitasUraian?></td>
                                                <td><?=$row->aktivitasLembaga?></td>
                                                <td><?=$row->aktivitasJumlah?></td>
                                                <td>

                                                     <?php if($row->aktivitasBerkas == '') {?>
                                                        Tidak Ada Berkas
                                                     <?php }else{?>
                                                          <a class="red" href="<?php echo base_url();?>public/assets/berkas/<?=$row->aktivitasBerkas?>" target="_blank" ><i class="flaticon-file-1"></i></a>

                                                     <?php }?>
                                                </td>
                                                <td>
                                                    <?php if($ref_revisi == true) {?>
                                                        <?php if ($ref_revisi->revisiket == 1 and $susrSgroupNama == 'Mhs') { ?>
                                                           <a href="<?=$update_url.$key?>" title="Update" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                            <span>
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </span>
                                                        </a>
                                                        <a href="<?=$delete_url.$key?>" title="Delete" id='ts_remove_row<?= $i; ?>' class="ts_remove_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                            <span>
                                                                <i class="fa fa-trash-alt"></i>
                                                            </span>
                                                        </a>
                                                    <?php  } } else{?> 
                                                     <a href="<?=$update_url.$key?>" title="Update" class="btn btn-sm btn-outline-primary btn-elevate btn-circle btn-icon">
                                                        <span>
                                                            <i class="fa fa-pencil-alt"></i>
                                                        </span>
                                                    </a>
                                                    <a href="<?=$delete_url.$key?>" title="Delete" id='ts_remove_row<?= $i; ?>' class="ts_remove_row btn btn-sm btn-outline-danger btn-elevate btn-circle btn-icon">
                                                        <span>
                                                            <i class="fa fa-trash-alt"></i>
                                                        </span>
                                                    </a>

                                                <?php }?>


                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--end::Section-->
            </div>
        </div>

        <!--end::Portlet-->
    </div>
</div>
</div>
<!--End::Row-->