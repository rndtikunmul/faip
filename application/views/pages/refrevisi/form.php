
<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?=$save_url?>" method="post" id="form_form">
                    <div class="kt-portlet__body">
                        <input type="hidden" name="revisiIdOld" value="<?=$datas!=false?$datas->revisiId:''?>">

                        <div class="form-group">
                            <label>revisiNama</label>
                            <input type="text" class="form-control" name="revisiNama" placeholder="revisiNama" aria-describedby="revisiNama" value="<?=$datas!=false?$datas->revisiNama:''?>">
                        </div>

                        <div class="form-group">
                            <label>revisiket</label>
                            <!--  <input type="text" class="form-control" name="revisiket" placeholder="revisiket" aria-describedby="revisiket" value="<?=$datas!=false?$datas->revisiket:''?>"> -->
                            <select class="form-control m-select2" name="revisiket">
                                <option value=""></option>
                                <option value="1">Buka</option>
                                <option value="0">Tutup</option>
                                
                            </select>
                        </div>


                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->
