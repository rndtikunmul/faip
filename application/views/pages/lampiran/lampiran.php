
<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?>
                        </h3>

                    </div>
                    
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?=$save_url?>" method="post" id="form_data" >
                    <div class="kt-portlet__body">
                        <input type="hidden" name="fptIdOld" value="<?=$datas!=false?$datas->lampiranNim:''?>">
                        <input type="hidden" class="form-control" name="lampiranNim" placeholder="fopFdpId" aria-describedby="lampiranNim" value="<?=$datas!=false?$datas->lampiranNim:$fdpNoKta?>">

                        <div class="form-group">
                            <label>No. Aktifitas </label>
                            <input type="text" class="form-control" name="lampiranAktifitas" placeholder="lampiranAktifitas" aria-describedby="lampiranAktifitas" value="<?=$datas!=false?$datas->lampiranAktifitas:''?>">
                        </div>

                        <div class="form-group">
                            <label>Nama Proyek</label>
                            <input type="text" class="form-control" name="lampiranNamaProyek" placeholder="Nama Proyek" aria-describedby="lampiranNamaProyek" value="<?=$datas!=false?$datas->lampiranNamaProyek:''?>">
                        </div>
                        <div class="form-group">
                            <label>Jangka Waktu Proyek</label>
                            <input type="text" class="form-control" name="lampiranWaktu" placeholder="jangka Waktu" aria-describedby="lampiranWaktu" value="<?=$datas!=false?$datas->lampiranWaktu:''?>">
                        </div>
                        <div class="form-group">
                            <label>Nama Atasan/Pengawas/Supervisor </label>
                            <input type="text" class="form-control" name="lampiranAtasan" placeholder="Nama Atasan" aria-describedby="lampiranAtasan" value="<?=$datas!=false?$datas->lampiranAtasan:''?>">
                        </div>
                        
                        <div class="form-group">
                            <label>Uraian tugas yang dilaksanakan </label>
                            <textarea class="form-control" id="exampleTextarea" rows="3" name="lampiranUraian" placeholder="lampiranUraian" aria-describedby="lampiranUraian" ><?=$datas!=false?$datas->lampiranUraian:''?></textarea>
                        </div>

                        <div class="form-group">
                            <label>Putusan keinsinyuran yang diambil (kalau ada) </label>
                            <textarea class="form-control" id="exampleTextarea" rows="3" name="lampiranPutusan" placeholder="lampiranPutusan" aria-describedby="lampiranPutusan" ><?=$datas!=false?$datas->lampiranPutusan:''?></textarea>
                        </div>

                        <div class="form-group">
                            <label>Bagan organisasi yang menunjukkan posisi dan pertanggungjawaban (kalau ada) </label>
                            <input type="file" class="form-control form-control-lg form-control-solid" type="text" name="uploadfile" placeholder="Foto"  id="custom-file-input" />
                            <span class="text-danger">* Format JPG dan max 300Kb</span></label>
                        </div>

                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->
