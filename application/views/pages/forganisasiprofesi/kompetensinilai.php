
<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!--begin::Entry-->
<div class="d-flex flex-column-fluid">
    <!--begin::Container-->
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-xl-6">
                <!--begin::Card-->
                <div class="card card-custom example example-compact">
                    <div class="card-header">
                        <h3 class="card-title"> <?=strtoupper($page_judul)?></h3>

                    </div>
                    <!--begin::Form-->
                    <form class="form">
                        <div class="card-body">

                           <table class="table table-hover">
                            <tr>
                                <th>Nama Organisasi</th>
                                <td><?=$datas!=false?$datas->aktivitasNama:''?></td>
                            </tr>
                            <tr>
                                <th>Kota/Negara</th>
                                <td><?=$datas!=false?$datas->kotaNama:''?>/<?=$datas!=false?$datas->negNama:''?></td>
                            </tr>
                            <tr>
                                <th>Periode</th>
                                <td><?=$datas!=false?$datas->aktivitasTahunAwal:''?> - <?=$datas!=false?$datas->aktivitasTahunAkhir:''?></td>
                            </tr>
                            <tr>
                                <th>Jabatan Organisasi</th>
                                <td><<?=$datas!=false?$datas->aktivitasJumlah:''?></td>
                            </tr>
                            <tr>
                                <th>Uraian Organisasi</th>
                                <td><?=$datas!=false?$datas->aktivitasUraian:''?></td>
                            </tr>
                        </tr>
                        <tr>
                            <th>Berkas</th>
                            <td><a class="red" href="<?php echo base_url();?>assets/berkas_organisasi/<?=$datas!=false?$datas->aktivitasBerkas:''?>" ><i class="flaticon-file-1"></i></a></td>
                        </tr>


                    </table>
                    
                    <div class="separator separator-dashed my-8"></div>
                </div>    
            </form>
            <!--end::Form-->
        </div>
        <!--end::Card-->
    </div>
    <div class="col-lg-12 col-xl-6">
        <!--begin::Card-->
        <div class="card card-custom example example-compact">
            <div class="card-header">
                <h3 class="card-title">Nilai Kompetensi</h3>

            </div>
            <!--begin::Form-->
            <form class="kt-form" action="<?=$save_url?>" method="post" id="form_form" >
                <div class="kt-portlet__body">
                    <input type="hidden" name="fopIdOld" value="<?=$datas!=false?$datas->aktivitasId:''?>">
                  
                    <div class="form-group">
                        <table class="table table-hover">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Uraian Kompetensi</th>
                                    <th>Nilai P</th>
                                    <th>Nilai O</th>
                                    <th>Nilai R</th>
                                    <th>Nilai T</th>

                                </tr>
                            </thead>
                            <tbody>
                               <?php
                               foreach ($ref_uraiankompetensi as $row2) {
                                if (!empty($row2->kompetensiId))
                                    $cek = "checked";
                                else
                                    $cek="";

                                ?>
                                <tr>
                                    <th scope="row"><input type="checkbox" class="checked" <?=$cek?> name="cekKomp[]" value="<?=$row2->kompetensiId?>" /></th>
                                    <td><?=$row2->uraianKompetensiNama?> <?=$row2->uraianKompetensiKet?></td>
                                    <td><input type="text" class="form-control" class="checked" <?=$cek?> name="kompetensiNilaiP[]" placeholder="P" aria-describedby="" value="<?=$row2->kompetensiNilaiP?>"></td>
                                    <td><input type="text" class="form-control" name="kompetensiNilaiQ[]" placeholder="Q" aria-describedby="" value="<?=$row2->kompetensiNilaiQ?>"></td>
                                    <td><input type="text" class="form-control" name="kompetensiNilaiR[]" placeholder="R" aria-describedby="" value="<?=$row2->kompetensiNilaiR?>"></td>
                                     <td><input type="text" readonly class="form-control" name="kompetensiNilaiT" placeholder="T" aria-describedby="" value="<?=$row2->kompetensiNilaiT?>"></td>
                                 
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>

                </div>


            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </div>
        </form>

        <!--end::Form-->
    </div>
    <!--end::Card-->
</div>
</div>
</div>
<!--end::Container-->
</div>
<!--end::Entry-->
</div>
<!--end::Content-->

</div>
<!--end::Wrapper-->
</div>
<!--end::Page-->
</div>
        <!--end::Main-->