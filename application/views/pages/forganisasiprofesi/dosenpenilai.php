
<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?= strtoupper($page_judul) ?>
                        </h3>
                    </div>
                   
                </div>
                <div class="kt-portlet__body">

                    <!--begin::Section-->
                    <div class="kt-section">
                        <div class="kt-section__content">
                     <form class="kt-form" action="<?=$save_url?>" method="post" id="form_form" >
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Organisasi dan Kota/Negara</th>
                                            <th>Periode</th>
                                            <th>Jabatan</th>
                                            <th>Aktifitas</th>
                                            <th>Berkas</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if($datas2!=false)
                                        {
                                            $i = 1;
                                            foreach($datas2 as $row)
                                            {
                                                 if (!empty($row->kompetensiId))
                                    $cek = "checked";
                                else
                                    $cek="";
                                                $key = $this->encryptions->encode($row->aktivitasId,$this->config->item('encryption_key'));
                                                ?>
                                                <tr>
                                                    <th scope="row"><?=$i++?></th>
                                                    <td><?=$row->aktivitasNama?>.<?=$row->kotaNama?>- <?=$row->negNama?> </td>
                                                    <td><?=$row->aktivitasTahunAwal?>
                                                    - <?=$row->aktivitasTahunAkhir?></td>
                                                    <td><?=$row->aktivitasJumlah?></td>
                                                    <td><?=$row->aktivitasUraian?></td>
                                                    <td><a class="red" href="<?php echo base_url();?>assets/berkas_organisasi/<?=$row->aktivitasBerkas?>" ><i class="flaticon-file-1"></i></a>
                                                    </td>
                                                    <td>
                                              
          
                                                         <tr>
                                    <th scope="row"><input type="checkbox" class="checked" <?=$cek?> name="cekKomp[]" value="<?=$row->kompetensiId?>" /></th>
                                    <td><?=$row->uraianKompetensiNama?> <?=$row->uraianKompetensiKet?></td>
                                    <td><input type="text" class="form-control" class="checked" name="kompetensiNilaiP[]" placeholder="P" aria-describedby="" value="<?=$row->kompetensiNilaiP?>"></td>
                                    <td><input type="text" class="form-control" name="kompetensiNilaiQ[]" placeholder="Q" aria-describedby="" value="<?=$row->kompetensiNilaiQ?>"></td>
                                    <td><input type="text" class="form-control" name="kompetensiNilaiR[]" placeholder="R" aria-describedby="" value="<?=$row->kompetensiNilaiR?>"></td>
                                     <td><input type="text" readonly class="form-control" name="kompetensiNilaiT" placeholder="T" aria-describedby="" value="<?=$row->kompetensiNilaiT?>"></td>
                                 
                                </tr>
                                
                                                    </td>
                                                   
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                             <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </div>
        </form>
                        </div>
                    </div>
                    <!--end::Section-->
                </div>
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->
<!--Begin::Row-->
