
<!-- BEGIN: Subheader -->
<?php $this->load->view('layouts/subheader'); ?>
<!-- END: Subheader -->

<!--Begin::Row-->
<!-- begin:: Content -->
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    <div class="row">
        <div class="col-md-12">
            <div id="response"></div>
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            <?=strtoupper($page_judul)?>
                        </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" action="<?=$save_url?>" method="post" id="form_data" >
                    <div class="kt-portlet__body">
                        <input type="hidden" name="fpeIdOld" value="<?=$datas!=false?$datas->aktivitasId:''?>">
                        <input type="hidden" class="form-control" name="aktivitasFdpId" placeholder="fopFdpId" aria-describedby="aktivitasFdpId" value="<?=$datas!=false?$datas->aktivitasFdpId:$fdpNoKta?>">

                        <div class="form-group">
                            <label>Nama Instansi/Perusahaan </label>
                            <input type="text" class="form-control" name="aktivitasLembaga" placeholder="Nama Instansi/Perusahaan " aria-describedby="aktivitasLembaga" value="<?=$datas!=false?$datas->aktivitasLembaga:''?>">
                        </div>

                        <div class="form-group">
                            <label>Nama Proyek</label>
                            <input type="text" class="form-control" name="aktivitasNama" placeholder="Nama Proyek" aria-describedby="aktivitasNama" value="<?=$datas!=false?$datas->aktivitasNama:''?>">
                        </div>

                        <div class="form-group">
                            <label>Kota</label>
                            <select class="form-control m-select2" name="aktivitasKota">
                                <option value=""></option>
                                <?php 
                                foreach($ref_kota as $row):
                                    echo '<option value="'.$row->kotaKode.'" ' . ($datas != false ? $datas->aktivitasKota == $row->kotaKode ? 'selected' : '' : '') . '>'.$row->kotaNama.'</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Negara</label>
                            <select class="form-control m-select2" name="aktivitasNegara">
                                <option value=""></option>
                                <?php 
                                foreach($ref_negara as $row):
                                    echo '<option value="'.$row->negKode.'" ' . ($datas != false ? $datas->aktivitasNegara == $row->negKode ? 'selected' : '' : '') . '>'.$row->negNama.'</option>';
                                endforeach;
                                ?>
                            </select>

                        </div>

                        <div class="form-group">
                            <label>Tahun Periode Awal</label>
                            <input type="text" class="form-control" name="aktivitasTahunAwal" placeholder="Tahun Periode Awal" aria-describedby="aktivitasTahunAwal" value="<?=$datas!=false?$datas->aktivitasTahunAwal:''?>">
                        </div>
                        <div class="form-group">
                            <label>Tahun Periode Akhir</label>
                            <input type="text" class="form-control" name="aktivitasTahunAkhir" placeholder="Tahun Periode Akhir" aria-describedby="aktivitasTahunAkhir" value="<?=$datas!=false?$datas->aktivitasTahunAkhir:''?>">
                        </div>

                     
                        <div class="form-group">
                            <label>Jabatan</label>
                            <input type="text" class="form-control" name="aktivitasJumlah" placeholder="Jabatan" aria-describedby="aktivitasJumlah" value="<?=$datas!=false?$datas->aktivitasJumlah:''?>">
                        </div>
                        
                        <div class="form-group">
                            <label>Uraian</label>
                            <textarea class="form-control" id="exampleTextarea" rows="3" name="aktivitasUraian" placeholder="aktivitasUraian" aria-describedby="aktivitasUraian" ><?=$datas!=false?$datas->aktivitasUraian:''?></textarea>
                        </div>

                        <div class="form-group">
                            <label>Berkas</label>
                            <input type="file" class="form-control form-control-lg form-control-solid" type="text" name="uploadfile" placeholder="Foto"  id="custom-file-input" />
                            <span class="text-danger">* Format PDF dan max 700Kb</span></label>
               
                        </div>
                        <div class="form-group">
                            <label>Uraian Kompetensi</label>

                            <table class="table table-hover">
                                <thead class="thead-light">
                                    <tr>
                                        <th>#</th>
                                        <th>Uraian Kompetensi</th>

                                    </tr>
                                </thead>
                                <tbody>
                                   <?php
                                   foreach ($ref_uraiankompetensi as $row2) {
                                    if (!empty($row2->kompetensiUraianKompetensiId))
                                        $cek = "checked";
                                    else
                                        $cek="";

                                    ?>
                                    <tr>
                                        <th scope="row"><input type="checkbox"class="checked" <?=$cek?> name="cekKompetensi[]" value="<?=$row2->uraianKompetensiId?>" /></th>
                                        <td><?=$row2->uraianKompetensiNama?>  <?=$row2->uraianKompetensiKet?></td>

                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>


                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" id="btn_save" class="btn btn-primary">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--End::Row-->
