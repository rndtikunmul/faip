<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';



class Servis_faip extends REST_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Model_servis_faip','servis_faip');
	}
	
	public function index_get()
	{
		$fdp = $this->servis_faip->all();
		$datas    = [];
		foreach ($fdp as $value) {
			$datas[] = array(
				'fdpNim' => $value->fdpNim,
				'fdpNama' => $value->fdpNama
			);
			
		}
		if (empty($datas)) {
			$this->response([
				'status' => FALSE,
				'error' => 'Data tidak ditemukan'
			], REST_Controller::HTTP_NOT_FOUND);
		}else{
			$this->response([
				'status' => TRUE,
				'datas' => ['datas' => $datas]
			], REST_Controller::HTTP_OK);
	}
}

	
	public function nim_post()
	{
		$this->form_validation->set_rules('nim', 'nim', 'required|trim|xss_clean');
		if ($this->form_validation->run() == TRUE) {
		// $nim = $this->input->post('nim',TRUE);
		$nim = str_replace("'", "", htmlspecialchars($this->input->post('nim'), ENT_QUOTES));
		$fdp = $this->servis_faip->by_nim($nim);
		$datas    = [];
		foreach ($fdp as $value) {
			$datas[] = array(
				'fdpNim' => $value->fdpNim,
				'fdpNama' => $value->fdpNama
			);
			
		}
		if (empty($datas)) {
			$this->response([
				'status' => FALSE,
				'error' => 'Data tidak ditemukan'
			], REST_Controller::HTTP_NOT_FOUND);
		}else{
			$this->response([
				'status' => TRUE,
				'datas' => ['datas' => $datas]
			], REST_Controller::HTTP_OK);
		}
	}
}

	
}

/* End of file Servicesia.php */
/* Location: ./application/controllers/Servicesia.php */