<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
class ref_unitkompetensi extends MY_Controller {
	function __construct()
	{
		parent::__construct();

		$this->_template = 'layouts/template';
		$this->_path_page = 'pages/ref_unitkompetensi/';
		$this->_path_js = null;
		$this->_judul = 'Ref_unitkompetensi';
		$this->_controller_name = 'ref_unitkompetensi';
		$this->_model_name = 'model_ref_unitkompetensi';
		$this->_page_index = 'index';
		$this->load->model($this->_model_name,'',TRUE);
	}


	public function index()

	{

		$data = $this->get_master($this->_path_page.$this->_page_index);

		$data['scripts'] = [];

		$data['datas'] = $this->{$this->_model_name}->get_ref_table('ref_unitkompetensi');

		$data['create_url'] = base_url($this->_controller_name.'/create').'/';

		$data['update_url'] = base_url($this->_controller_name.'/update').'/';

		$data['delete_url'] = base_url($this->_controller_name.'/delete').'/';

		$this->load->view($this->_template, $data);

	}


	public function create()

	{	

		$data = $this->get_master($this->_path_page.'form');	

		$data['scripts'] = [];	

		$data['save_url'] = base_url($this->_controller_name.'/save').'/';	

		$data['status_page'] = 'Create';

		$data['datas'] = false;


		$this->load->view($this->_template, $data);

	}


	public function update()

	{		

		$data = $this->get_master($this->_path_page.'form');	

		$keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));

		$data['scripts'] = [];	

		$data['save_url'] = base_url($this->_controller_name.'/save').'/';	

		$data['status_page'] = 'Update';

		$key = ['unitKompetensiId'=>$keyS];

		$data['datas'] = $this->{$this->_model_name}->get_by_id('ref_unitkompetensi',$key);


		$this->load->view($this->_template, $data);

	}


	public function save()

	{		

		$unitKompetensiIdOld = $this->input->post('unitKompetensiIdOld');

		$this->form_validation->set_rules('unitKompetensiNama','unitKompetensiNama','trim|xss_clean');

		$this->form_validation->set_rules('unitKompetensiKet','unitKompetensiKet','trim|xss_clean');


		if($this->form_validation->run()) 

		{	

			if(IS_AJAX)

			{

				$unitKompetensiNama = $this->input->post('unitKompetensiNama');

				$unitKompetensiKet = $this->input->post('unitKompetensiKet');



				$param = array(
					'unitKompetensiNama'=>$unitKompetensiNama,
					'unitKompetensiKet'=>$unitKompetensiKet,

				);

				if(empty($unitKompetensiIdOld))
				{
					$proses = $this->{$this->_model_name}->insert('ref_unitkompetensi',$param);
				} else {
					$key = array('unitKompetensiId'=>$unitKompetensiIdOld);
					$proses = $this->{$this->_model_name}->update('ref_unitkompetensi',$param,$key);
				}

				if($proses)
					message($this->_judul.' Berhasil Disimpan','success');
				else
				{
					$error = $this->db->error();
					message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
				}
			}
		} else {
			message('Ooops!! Something Wrong!! '.validation_errors(),'error');
		}
	}

	public function delete()
	{
		$keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
		$key = ['unitKompetensiId'=>$keyS];
		$proses = $this->{$this->_model_name}->delete('ref_unitkompetensi',$key);
		if ($proses) 
			message($this->_judul.' Berhasil Dihapus','success');
		else
		{
			$error = $this->db->error();
			message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
		}
	}
}