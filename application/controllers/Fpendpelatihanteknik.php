<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class fpendpelatihanteknik extends MY_Controller {
    function __construct()
    {
        parent::__construct();
        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/fpendpelatihanteknik/';
        $this->_path_js = 'datapribadi';
        $this->_judul = 'I.5 Pendidikan/Pelatihan Teknik/Pertanian & Profesi Keinsinyuran Yang Diikuti';
        $this->_controller_name = 'fpendpelatihanteknik';
        $this->_model_name = 'model_fpendpelatihanteknik';
        $this->_page_index = 'index';
        $this->load->model($this->_model_name,'',TRUE);
    }

    public function index()
    {
        $session_data = $this->session->userdata('logged_in');
        $data = $this->get_master($this->_path_page.$this->_page_index);
        if (($session_data['susrSgroupNama'] == 'Mhs') OR ($session_data['susrSgroupNama'] == 'ADMIN') )
        {

            $data['fdpNoKta'] = $this->{$this->_model_name}->get_ref_table('f_datapribadi');

        }else{
            $data['fdpNoKta'] = $this->{$this->_model_name}->bimbingan($session_data['susrSgroupNama_ori']);
        }
        $data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];
        $data['response_url'] = base_url($this->_controller_name.'/response');
        $data['susrSgroupNama_ori'] = $session_data['susrSgroupNama_ori'];
        $this->load->view($this->_template, $data);
    }

    public function response()
    {
     $session_data = $this->session->userdata('logged_in');     
     $this->form_validation->set_rules('fdpNoKta', 'pegmNIP', 'trim|required|xss_clean');
     if ($this->form_validation->run()) {
        if (IS_AJAX) {
            $data = $this->get_master($this->_path_page . $this->_page_index);
            $data['ref_revisi'] = $this->{$this->_model_name}->get_by_id('ref_revisi', ['revisiNama'=>$session_data['susrSgroupNama']]);
            $pegmNIP = $this->input->post('fdpNoKta');
            $data['nip'] = $this->encryptions->encode($pegmNIP, $this->config->item('encryption_key'));
            $key = ['aktivitasFdpId'=>$pegmNIP];
            $data['datas'] = $this->{$this->_model_name}->all($key); 
            // echo $this->db->last_query(); exit; 
            $data['create_url'] = base_url($this->_controller_name . '/create') . '/';
            $data['update_url'] = base_url($this->_controller_name . '/update') . '/';
            $data['delete_url'] = base_url($this->_controller_name . '/delete') . '/';
            $pages = $this->_path_page . 'response';
            $this->load->view($pages, $data);
        }
    } else {
        message('Ooops!! Something Wrong!!', 'error');
    }
}

    public function create()
    {	
        $data = $this->get_master($this->_path_page.'form');	
        $data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];
        $data['save_url'] = base_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['fdpNoKta'] =$this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['ref_kota'] = $this->{$this->_model_name}->get_ref_table('ref_kota');
        $data['ref_negara'] = $this->{$this->_model_name}->get_ref_table('ref_negara');
        $data['ref_lvlatih'] = $this->{$this->_model_name}->get_ref_table('ref_lvlatih');

        $key = false;
        $urai2 = [''];
        $data['ref_uraiankompetensi'] = $this->{$this->_model_name}->uraiankompetensiid($key,'6',$urai2);
        $this->load->view($this->_template, $data);
    }

    public function update()
    {		
        $data = $this->get_master($this->_path_page.'form');	
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];
        $data['save_url'] = base_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Update';
        $key = ['aktivitasId'=>$keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['ref_kota'] = $this->{$this->_model_name}->get_ref_table('ref_kota');
        $data['ref_negara'] = $this->{$this->_model_name}->get_ref_table('ref_negara');
        $data['ref_lvlatih'] = $this->{$this->_model_name}->get_ref_table('ref_lvlatih');
        $urai2 = [''];
        $data['ref_uraiankompetensi'] = $this->{$this->_model_name}->uraiankompetensiid($keyS,'6',$urai2);
        $this->load->view($this->_template, $data);
    }


    public function save()
    {		
        $fptIdOld = $this->input->post('fptIdOld');
        $this->form_validation->set_rules('aktivitasKota','aktivitasKota','trim|xss_clean|required');
        $this->form_validation->set_rules('aktivitasNegara','aktivitasNegara','trim|xss_clean|required');
        $this->form_validation->set_rules('aktivitasFdpId','aktivitasFdpId','trim|xss_clean|required');
        $this->form_validation->set_rules('aktivitasNama','aktivitasNama','trim|xss_clean|required');
        $this->form_validation->set_rules('aktivitasPeriode','aktivitasPeriode','trim|xss_clean|required');
        $this->form_validation->set_rules('aktivitasJumlah','aktivitasJumlah','trim|xss_clean|required');
        $this->form_validation->set_rules('aktivitasUraian','aktivitasUraian','trim|xss_clean|required');
        $this->form_validation->set_rules('aktivitasTahunAwal','aktivitasTahunAwal','trim|xss_clean|required');

        $this->form_validation->set_rules('aktivitasLembaga','aktivitasLembaga','trim|xss_clean|required');
        $this->form_validation->set_rules('cekKompetensi[]','Modul','trim|required|xss_clean');

        if($this->form_validation->run()) 
        {	
         if(IS_AJAX)
         {
             $cekKompetensi = $this->input->post('cekKompetensi');    
             $aktivitasKota = $this->input->post('aktivitasKota');
             $aktivitasNegara = $this->input->post('aktivitasNegara');
             $aktivitasFdpId = $this->input->post('aktivitasFdpId');
             $aktivitasNama = $this->input->post('aktivitasNama');
             $aktivitasPeriodetanggalawal = $this->input->post('aktivitasPeriode');
             $tglawal = explode("/", $aktivitasPeriodetanggalawal);
             $aktivitasPeriode= $tglawal[0];
             $aktivitasPeriodeAkhir= $tglawal[1];
             $aktivitasJumlah = $this->input->post('aktivitasJumlah');
             $aktivitasUraian = $this->input->post('aktivitasUraian');
             $aktivitasTahunAwal = $this->input->post('aktivitasTahunAwal');
             $aktivitasTahunAkhir = $this->input->post('aktivitasTahunAkhir');
             $aktivitasLembaga = $this->input->post('aktivitasLembaga');

             $param = array(
                'aktivitasKota'=>$aktivitasKota,
                'aktivitasNegara'=>$aktivitasNegara,
                'aktivitasFdpId'=>$aktivitasFdpId,
                'aktivitasNama'=>$aktivitasNama,
                'aktivitasPeriode'=>$aktivitasPeriode,
                'aktivitasPeriodeAkhir'=>$aktivitasPeriodeAkhir,
                'aktivitasJumlah'=>$aktivitasJumlah,
                'aktivitasTahunAwal'=>$aktivitasTahunAwal,
                'aktivitasTahunAkhir'=>$aktivitasTahunAkhir,
                'aktivitasUraian'=>$aktivitasUraian,
                'aktivitasLembaga'=>$aktivitasLembaga,
                'aktivitasIdForm'=>'I.5',
                'aktivitasTahunSemester'=> date('Y'),

            );

             if(empty($fptIdOld) and (empty($_FILES['uploadfile']['name']) ))
             {
                message('Maaf.. silahkan Upload Berkas dalam bentuk PDF', 'error');
                exit(); 
            } else
            {
               $date_awal = date('Y-m-d i-s');
               if(!empty($_FILES['uploadfile']['name']))
               {
                $uploadfilename = 'Pendpelatihan_'.$aktivitasFdpId.'_'.$date_awal;
                $konfig = array(
                    'url' => FCPATH .'public/assets/berkas/',
                    'type' => 'pdf',
                    'size' => 0.7*1024,
                    'namafile' => $uploadfilename
                );

                $this->load->library('UploadArsip');
                $file_name = $this->uploadarsip->arsip($konfig, 'uploadfile');
                if (!empty($file_name))
                    $param['aktivitasBerkas'] = $file_name;

            }
        }

        if(count($cekKompetensi)<6)
        {
            if(empty($fptIdOld))
            {
                $proses = $this->{$this->_model_name}->insert('tb_aktivitas',$param);
                $cekaktivitasave = $this->{$this->_model_name}->cekaktivitas($aktivitasFdpId,'I.5',$file_name);
                foreach ($cekKompetensi as $modul) { 

                    $paramkompetensi = array(
                        'kompetensiAktivitasId'=>$cekaktivitasave->aktivitasId,
                        'kompetensiUraianKompetensiId'=>$modul,
                        'kompetensiFdpId' =>$cekaktivitasave->aktivitasFdpId,

                    );   

                    $this->{$this->_model_name}->insert('tb_kompetensi',$paramkompetensi);
                }
            } else {
                $key = array('aktivitasId'=>$fptIdOld);
                $proses = $this->{$this->_model_name}->update('tb_aktivitas',$param,$key);

                $this->{$this->_model_name}->delete('tb_kompetensi',array('kompetensiAktivitasId'=>$fptIdOld));
                foreach ($cekKompetensi as $modul) 
                { 
                    $paramkompetensi = array(
                        'kompetensiAktivitasId'=>$fptIdOld,
                        'kompetensiUraianKompetensiId'=>$modul,
                        'kompetensiFdpId' =>$aktivitasFdpId,

                    ); 
                    $this->{$this->_model_name}->insert('tb_kompetensi',$paramkompetensi);
                }
            }

        } else{

            message('Maaf.. Uraian tidak boleh lebih dari 5', 'error');
        } 

        if($proses)
            message($this->_judul.' Berhasil Disimpan','success');
        else
        {
            $error = $this->db->error();
            message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
        }
    }
    } else {
        message('Ooops!! Something Wrong!! '.validation_errors(),'error');
    }
    }


    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $key = ['aktivitasId'=>$keyS];
        $proses = $this->{$this->_model_name}->delete('tb_aktivitas',$key);
        if ($proses) 
            message($this->_judul.' Berhasil Dihapus','success');
        else
        {
            $error = $this->db->error();
            message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
        }
    }
}
