<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class tb_kompetensi extends MY_Controller {
	function __construct()
	{
		parent::__construct();

		$this->_template = 'layouts/template';
		$this->_path_page = 'pages/tb_kompetensi/';
		$this->_path_js = 'user';
		$this->_judul = 'Nilai Kompetensi';
		$this->_controller_name = 'tb_kompetensi';
		$this->_model_name = 'model_tb_kompetensi';
		$this->_page_index = 'index';

		$this->load->model($this->_model_name,'',TRUE);
	}

	public function index()
	{
		$session_data = $this->session->userdata('logged_in');
		$data = $this->get_master($this->_path_page.$this->_page_index);
		if (($session_data['susrSgroupNama'] == 'Mhs') OR ($session_data['susrSgroupNama'] == 'ADMIN') )
		{

			$data['fdpNoKta'] = $this->{$this->_model_name}->get_ref_table('f_datapribadi');

		}else{
			$data['fdpNoKta'] = $this->{$this->_model_name}->bimbingan($session_data['susrSgroupNama_ori']);
		}
		$data['scripts'] = ['user/' . $this->_controller_name];
		$data['response_url'] = base_url($this->_controller_name.'/response').'/';
		// echo $this->db->last_query(); exit();
		$data['susrSgroupNama_ori'] = $session_data['susrSgroupNama_ori'];
		$this->load->view($this->_template, $data);
	}

	public function response()
	{
		ini_set('max_execution_time', 0);
		$session_data = $this->session->userdata('logged_in');
		$this->form_validation->set_rules('fdpNoKta', 'pegmNIP', 'trim|required|xss_clean');
		if ($this->form_validation->run()) {
			if (IS_AJAX) {
				$data = $this->get_master($this->_path_page . $this->_page_index);
				$data['ref_revisi'] = $this->{$this->_model_name}->get_by_id('ref_revisi', ['revisiNama'=>$session_data['susrSgroupNama']]);
				$pegmNIP = $this->input->post('fdpNoKta');
				$matakuliah = $this->input->post('matakuliah');
				$data['nip'] = $this->encryptions->encode($pegmNIP, $this->config->item('encryption_key'));
				$key = ['aktivitasFdpId'=>$pegmNIP];
				$data['datasnilai'] = $this->{$this->_model_name}->nilai($key,$matakuliah);
				// echo $this->db->last_query(); exit();
				// echo $this->db->last_query();
				// exit;
				// $data['datas'] = $this->{$this->_model_name}->all_id($key);
				//   if ($matakuliah == 'kodeetik'){
				//     $data['datasnilai'] = $this->{$this->_model_name}->penilai($key,'II.1','I.3' ,'I.4');

				// }elseif ($matakuliah == 'profesionalisme'){
				//   $data['datasnilai'] = $this->{$this->_model_name}->penilaiprofesionalisme($key);

				// }elseif ($matakuliah == 'k3lh'){
				//   $data['datasnilai'] = $this->{$this->_model_name}->penilai($key,'II.2','' ,'');

				// }elseif ($matakuliah == 'seminar'){
				//   $data['datasnilai'] = $this->{$this->_model_name}->penilai($key,'IV.1','IV.2' ,'V');

				// }elseif ($matakuliah == 'studikasus'){
				//   $data['datasnilai'] = $this->{$this->_model_name}->penilaisk($key);

				// }
				// echo $this->db->last_query();
				$data['matakuliah'] =  $matakuliah ;
				//  $data['datasnilai'] = $this->{$this->_model_name}->penilai($key,'II.1','I.3' ,'I.4');
				//  $data['datasnilai2'] = $this->{$this->_model_name}->penilaiprofesionalisme($key);
				// $data['datasnilai3'] = $this->{$this->_model_name}->penilai($key,'II.2','' ,'');
				// $data['datasnilai4'] = $this->{$this->_model_name}->penilai($key,'IV.1','IV.2' ,'V');
				// $data['datasnilai5'] = $this->{$this->_model_name}->penilaisk($key);
				$data['save_url'] = base_url($this->_controller_name.'/savenilai').'/';  
				$data['create_url'] = base_url($this->_controller_name . '/create') . '/';
				$data['update_url'] = base_url($this->_controller_name . '/update') . '/';
				$data['delete_url'] = base_url($this->_controller_name . '/delete') . '/';
				$data['totalnilai_url'] = base_url($this->_controller_name . '/totalnilai').'/';
				$data['cetakformulir_url'] = base_url($this->_controller_name.'/cetakformulir').'/';
				$data['cetalampiran_url'] = base_url($this->_controller_name . '/cetaklampiran').'/';
				$session_data = $this->session->userdata('logged_in');
				if (($session_data['susrSgroupNama'] == 'Dosen') OR ($session_data['susrSgroupNama'] == 'ADMIN') )
				{
					$data['cetaknilai_url'] = base_url($this->_controller_name.'/cetaknilai').'/';     
					$pages = $this->_path_page . 'response';

				}else{
					$this->load->helper('tgl_indo'); 
					$data['datas'] = $this->{$this->_model_name}->total($pegmNIP);
					$where = ['fdpNim'=>$pegmNIP];
					$data['insinyur'] = $this->{$this->_model_name}->get_by_id('f_datapribadi',$where);
					$data['tb_aktivitas'] = $this->{$this->_model_name}->get_ref_table('tb_aktivitas');
					// echo $this->db->last_query();
     //    			exit();
					$data['ttd'] = $this->{$this->_model_name}->ttd($pegmNIP);
					$data['ref_uraiankompetensi'] = $this->{$this->_model_name}->get_ref_table('ref_uraiankompetensi');
					$data['cetaknilai_url'] = base_url($this->_controller_name.'/cetaknilai').'/';     
					$data['tanggal'] = $this->{$this->_model_name}->get_by_id('ref_tanggal', ['tanggalId'=>'1']);
					$pages = $this->_path_page . 'response3';
					// echo $this->db->last_query(); exit();
				}

				$this->load->view($pages, $data);
			}
		} else {
			message('Ooops!! Something Wrong!!', 'error');
		}
	}



	function cetaklampiran()
	{

		ini_set('max_execution_time', 0);
		$this->load->library('pdfgenerator');
		$session_data = $this->session->userdata('logged_in');
		if ( $session_data['susrSgroupNama_ori'] =='ADMIN') {
			$keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
		}else{
			$keyS =  $session_data['susrSgroupNama_ori'];   
		}

		$data['insinyur'] = $this->{$this->_model_name}->insinyur('f_datapribadi',['fdpNim'=>$keyS]); 
		$data['lampiran'] = $this->{$this->_model_name}->lampiran(['lampiranNim'=>$keyS]);    
        // echo "<pre>";
        //   print_r ($keyS);
        //   echo "</pre>";  exit;
        // $this->load->view($this->_path_page.'cetaklampiran', $data, FALSE);
		$this->pdfgenerator->generate($this->load->view($this->_path_page.'cetaklampiran',$data,true),'cetaklampiran','potrait');

	}

	public function savenilai()
	{       
		$session_data = $this->session->userdata('logged_in');
		$this->form_validation->set_rules('cekKomp[]','Modul','trim|required|xss_clean');
    // $this->form_validation->set_rules('kompetensiNilaiP[]','kompetensiNilaiP','trim|required|xss_clean');
    // $this->form_validation->set_rules('kompetensiNilaiQ[]','kompetensiNilaiQ','trim|required|xss_clean');
    // $this->form_validation->set_rules('kompetensiNilaiR[]','kompetensiNilaiR','trim|required|xss_clean');

		if($this->form_validation->run()) 
		{   
			if(IS_AJAX)
			{ 
				$kompetensiNilaiP = $this->input->post('kompetensiNilaiP');    
				$kompetensiNilaiQ = $this->input->post('kompetensiNilaiQ');
				$kompetensiNilaiR = $this->input->post('kompetensiNilaiR');
				$cekKomp = $this->input->post('cekKomp');  
				$kompetensiId= $this->input->post('kompetensiId');  

				foreach($cekKomp as $key=>$value)
				{
					$kompetensiNilaiP[$value] = isset($kompetensiNilaiP[$value])?intval($kompetensiNilaiP[$value]):0;
					$kompetensiNilaiQ[$value] = isset($kompetensiNilaiQ[$value])?intval($kompetensiNilaiQ[$value]):0;
					$kompetensiNilaiR[$value] = isset($kompetensiNilaiR[$value])?intval($kompetensiNilaiR[$value]):0;

					$paramkompetensi = array(
						'kompetensiNilaiP'=>$kompetensiNilaiP[$value],
						'kompetensiNilaiQ'=>$kompetensiNilaiQ[$value],
						'kompetensiNilaiR'=>$kompetensiNilaiR[$value],
						'kompetensiNilaiT'=>$kompetensiNilaiP[$value] * $kompetensiNilaiQ[$value] * $kompetensiNilaiR[$value],
						'kompetensiUserPenilai'=> $session_data['susrSgroupNama_ori'],
					); 

           //           echo "<pre>";
        			// print_r ($paramkompetensi);
        			// print_r ($value);
        			// echo "</pre>"; 

					$proses = $this->db->update('tb_kompetensi',$paramkompetensi,array('kompetensiId' => $value)); 

				}
				 // exit;



				if($proses)
					message($this->_judul.' Berhasil Disimpan','success');
				else
				{
					$error = $this->db->error();
					message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
				}
			}
		} else {
			message('Ooops!! Something Wrong!! '.validation_errors(),'error');
		}
	}



	public function create()
	{   
		$data = $this->get_master($this->_path_page.'form');    
		$data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];
		$data['save_url'] = base_url($this->_controller_name.'/save').'/';  
		$data['status_page'] = 'Create';
		$data['datas'] = false;
		$data['tb_aktivitas'] = $this->{$this->_model_name}->get_ref_table('tb_aktivitas');
		$data['ref_uraiankompetensi'] = $this->{$this->_model_name}->get_ref_table('ref_uraiankompetensi');

		$this->load->view($this->_template, $data);
	}

	public function update()
	{       
		$data = $this->get_master($this->_path_page.'form');    
		$keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
		$data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];   
		$data['save_url'] = base_url($this->_controller_name.'/save').'/';  
		$data['status_page'] = 'Update';
		$data['datas'] = $this->{$this->_model_name}->by_id(['kompetensiId'=>$keyS]);
		$data['tb_aktivitas'] = $this->{$this->_model_name}->get_ref_table('tb_aktivitas');
		$data['ref_uraiankompetensi'] = $this->{$this->_model_name}->get_ref_table('ref_uraiankompetensi');

		$this->load->view($this->_template, $data);
	}

	public function totalnilai()
	{       

		$session_data = $this->session->userdata('logged_in');
		$this->load->helper('tgl_indo'); 
		$data = $this->get_master($this->_path_page.'totalnilai'); 
		$data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];  
		$data['cetakformulir_url'] = base_url($this->_controller_name.'/cetakformulir').'/';
		$pegmNIP =  $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
		$data['datas'] = $this->{$this->_model_name}->total($pegmNIP);
		$where = ['fdpNim'=>$pegmNIP];
		$data['insinyur'] = $this->{$this->_model_name}->get_by_id('f_datapribadi',$where);
		$data['tanggal'] = $this->{$this->_model_name}->get_by_id('ref_tanggal', ['tanggalId'=>'1']);
		$data['tb_aktivitas'] = $this->{$this->_model_name}->get_ref_table('tb_aktivitas');
		$data['ref_uraiankompetensi'] = $this->{$this->_model_name}->get_ref_table('ref_uraiankompetensi');
		$data['ttd'] = $this->{$this->_model_name}->ttd($pegmNIP);
		$data['susrSgroupNama'] = $session_data['susrSgroupNama'] ;
		$this->load->view($this->_template, $data);
	}

	function cetaknilai()
	{

		$this->load->library('pdfgenerator');
		$this->load->helper('tgl_indo'); 
		$session_data = $this->session->userdata('logged_in');

		$pegmNIP =  $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
		$data['datas'] = $this->{$this->_model_name}->total($pegmNIP);
        // // echo $this->db->last_query();
        // // exit();
		$where = ['fdpNim'=>$pegmNIP];		
		$data['insinyur'] = $this->{$this->_model_name}->get_by_id('f_datapribadi',$where);
		$data['tanggal'] = $this->{$this->_model_name}->get_by_id('ref_tanggal', ['tanggalId'=>'1']);
		// $data['tb_aktivitas'] = $this->{$this->_model_name}->get_ref_table('tb_aktivitas');
		$data['ref_uraiankompetensi'] = $this->{$this->_model_name}->get_ref_table('ref_uraiankompetensi'); 
		$data['ttd'] = $this->{$this->_model_name}->ttd($pegmNIP);
        // $this->load->view($this->_path_page.'cetak', $data, FALSE);
		$this->pdfgenerator->generate($this->load->view($this->_path_page.'cetak',$data,true),'cetakformulir','potrait');

	}


	public function save()
	{       
		$kompetensiIdOld = $this->input->post('kompetensiIdOld');
		$this->form_validation->set_rules('kompetensiUraianKompetensiId','kompetensiUraianKompetensiId','trim|xss_clean');

		if($this->form_validation->run()) 
		{   
			if(IS_AJAX)
			{
				$kompetensiUraianKompetensiId = $this->input->post('kompetensiUraianKompetensiId');


				$param = array(
					'kompetensiUraianKompetensiId'=>$kompetensiUraianKompetensiId,

				);

				$key = array('kompetensiId'=>$kompetensiIdOld);
				$proses = $this->{$this->_model_name}->update('tb_kompetensi',$param,$key);

				if($proses)
					message($this->_judul.' Berhasil Disimpan','success');
				else
				{
					$error = $this->db->error();
					message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
				}
			}
		} else {
			message('Ooops!! Something Wrong!! '.validation_errors(),'error');
		}
	}

	function cetakformulir()
	{

		ini_set('max_execution_time', 0);
		$this->load->library('pdfgenerator');
		$session_data = $this->session->userdata('logged_in');
		$keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
		$key = ['aktivitasFdpId'=>$keyS];
		$data['penformal'] = $this->{$this->_model_name}->cetakformulir($key,'I.2','' ,'');
		$cetak1 = $this->{$this->_model_name}->cetak($key,'I.3'); 
		$datasnew=FALSE;
		$forfanisasi = false;
		if($cetak1!==false) {
			foreach ($cetak1 as $row) 
			{
				$forfanisasi = $this->{$this->_model_name}->cetak2($row->aktivitasId);

				if($forfanisasi!=false)
				{
					foreach($forfanisasi as $row2)
					{

						$datasnew[$row->aktivitasId] = $forfanisasi;  

					}
				}            
			}

		}

		$data['orprof'] = $cetak1;
		$data['orprof1'] = $datasnew;

		$cetak2 = $this->{$this->_model_name}->cetak($key,'I.4'); 
		$datasnew2=FALSE;
		$penghargaan = false;
		if($cetak2!==false) {
			foreach ($cetak2 as $row) 
			{
				$penghargaan = $this->{$this->_model_name}->cetak2($row->aktivitasId);

				if($penghargaan!=false)
				{
					foreach($penghargaan as $row2)
					{

						$datasnew2[$row->aktivitasId] = $penghargaan;  

					}
				}            
			}

		}

		$data['fpenghargaan1'] = $cetak2;
		$data['fpenghargaan2'] = $datasnew2;

		$cetak3 = $this->{$this->_model_name}->cetak($key,'I.5'); 
		$datasnew3=FALSE;
		$teknik = false;
		if($cetak3!==false) {
			foreach ($cetak3 as $row) 
			{
				$teknik = $this->{$this->_model_name}->cetak2($row->aktivitasId);

				if($teknik!=false)
				{
					foreach($teknik as $row2)
					{

						$datasnew3[$row->aktivitasId] = $teknik;  

					}
				}            
			}

		}

		$data['fpendteknik1'] = $cetak3;
		$data['fpendteknik2'] = $datasnew3;
		$cetak4 = $this->{$this->_model_name}->cetak($key,'I.6'); 
		$datasnew4=FALSE;
		$manaj = false;
		if($cetak4!==false) {
			foreach ($cetak4 as $row) 
			{
				$manaj = $this->{$this->_model_name}->cetak2($row->aktivitasId);

				if($manaj!=false)
				{
					foreach($manaj as $row2)
					{

						$datasnew4[$row->aktivitasId] = $manaj;  

					}
				}            
			}

		}

		$data['fpendmanaj1'] = $cetak4;
		$data['fpendmanaj2'] = $datasnew4;
		$cetak5 = $this->{$this->_model_name}->cetak($key,'II.2'); 
		$datasnew5=FALSE;
		$kodeetikII2 = false;
		if($cetak5!==false) {
			foreach ($cetak5 as $row) 
			{
				$kodeetikII2 = $this->{$this->_model_name}->cetak2($row->aktivitasId);

				if($kodeetikII2!=false)
				{
					foreach($kodeetikII2 as $row2)
					{

						$datasnew5[$row->aktivitasId] = $kodeetikII2;  

					}
				}            
			}

		}

		$data['kodeetikII21'] = $cetak5;
		$data['kodeetikII22'] = $datasnew5;
		$cetak6 = $this->{$this->_model_name}->cetak($key,'III.1'); 

		$datasnew6=FALSE;
		$kualifikasi1 = false;
		if($cetak6!==false) {
			foreach ($cetak6 as $row) 
			{
				$kualifikasi1 = $this->{$this->_model_name}->cetak2($row->aktivitasId);

				if($kualifikasi1!=false)
				{
					foreach($kualifikasi1 as $row2)
					{

						$datasnew6[$row->aktivitasId] = $kualifikasi1;  

					}
				}            
			}

		}

		$data['kualifikasiprof11'] = $cetak6;
		$data['kualifikasiprof12'] = $datasnew6;

		$cetak7 = $this->{$this->_model_name}->cetak($key,'III.2'); 
		$datasnew7=FALSE;
		$kualifikasi2 = false;
		if($cetak7!==false) {
			foreach ($cetak7 as $row) 
			{
				$kualifikasi2 = $this->{$this->_model_name}->cetak2($row->aktivitasId);

				if($kualifikasi2!=false)
				{
					foreach($kualifikasi2 as $row2)
					{

						$datasnew7[$row->aktivitasId] = $kualifikasi2;  

					}
				}            
			}

		}

		$data['kualifikasiprof21'] = $cetak7;
		$data['kualifikasiprof22'] = $datasnew7;
		$cetak8 = $this->{$this->_model_name}->cetak($key,'III.3'); 
		$datasnew8=FALSE;
		$kualifikasi3 = false;
		if($cetak8!==false) {
			foreach ($cetak8 as $row) 
			{
				$kualifikasi3 = $this->{$this->_model_name}->cetak2($row->aktivitasId);

				if($kualifikasi3!=false)
				{
					foreach($kualifikasi3 as $row2)
					{

						$datasnew8[$row->aktivitasId] = $kualifikasi3;  

					}
				}            
			}

		}

		$data['kualifikasiprof31'] = $cetak8;
		$data['kualifikasiprof32'] = $datasnew8;
		$cetak9 = $this->{$this->_model_name}->cetak($key,'III.4'); 
		$datasnew9=FALSE;
		$kualifikasi4 = false;
		if($cetak9!==false) {
			foreach ($cetak9 as $row) 
			{
				$kualifikasi4 = $this->{$this->_model_name}->cetak2($row->aktivitasId);

				if($kualifikasi4!=false)
				{
					foreach($kualifikasi4 as $row2)
					{

						$datasnew9[$row->aktivitasId] = $kualifikasi4;  

					}
				}            
			}

		}

		$data['kualifikasiprof41'] = $cetak9;
		$data['kualifikasiprof42'] = $datasnew9;
		$cetak10 = $this->{$this->_model_name}->cetak($key,'III.5'); 
		$datasnew10 =FALSE;
		$kualifikasi5 = false;
		if($cetak10!==false) {
			foreach ($cetak10 as $row) 
			{
				$kualifikasi5 = $this->{$this->_model_name}->cetak2($row->aktivitasId);

				if($kualifikasi5!=false)
				{
					foreach($kualifikasi5 as $row2)
					{

						$datasnew10[$row->aktivitasId] = $kualifikasi5;  

					}
				}            
			}

		}

		$data['kualifikasiprof51'] = $cetak10;
		$data['kualifikasiprof52'] = $datasnew10;
		$cetak11 = $this->{$this->_model_name}->cetak($key,'IV.1'); 
		$datasnew11 =FALSE;
		$publikasi1 = false;
		if($cetak11!==false) {
			foreach ($cetak11 as $row) 
			{
				$publikasi1 = $this->{$this->_model_name}->cetak2($row->aktivitasId);

				if($publikasi1!=false)
				{
					foreach($publikasi1 as $row2)
					{

						$datasnew11[$row->aktivitasId] = $publikasi1;  

					}
				}            
			}

		}

		$data['publikasi11'] = $cetak11;
		$data['publikasi12'] = $datasnew11;
		$cetak12 = $this->{$this->_model_name}->cetak($key,'IV.2'); 
		$datasnew12 =FALSE;
		$publikasi2 = false;
		if($cetak12!==false) {
			foreach ($cetak12 as $row) 
			{
				$publikasi2 = $this->{$this->_model_name}->cetak2($row->aktivitasId);

				if($publikasi2!=false)
				{
					foreach($publikasi2 as $row2)
					{

						$datasnew12[$row->aktivitasId] = $publikasi2;  

					}
				}            
			}

		}

		$data['publikasi21'] = $cetak12;
		$data['publikasi22'] = $datasnew12;
		$cetak13 = $this->{$this->_model_name}->cetak($key,'IV.3'); 
		$datasnew13 =FALSE;
		$publikasi3 = false;
		if($cetak13!==false) {
			foreach ($cetak13 as $row) 
			{
				$publikasi3 = $this->{$this->_model_name}->cetak2($row->aktivitasId);

				if($publikasi3!=false)
				{
					foreach($publikasi3 as $row2)
					{

						$datasnew13[$row->aktivitasId] = $publikasi3;  

					}
				}            
			}

		}

		$data['publikasi31'] = $cetak13;
		$data['publikasi32'] = $datasnew13;
		$cetak14 = $this->{$this->_model_name}->cetak($key,'IV.4'); 
		$datasnew14 =FALSE;
		$publikasi4 = false;
		if($cetak14!==false) {
			foreach ($cetak14 as $row) 
			{
				$publikasi4 = $this->{$this->_model_name}->cetak2($row->aktivitasId);

				if($publikasi4!=false)
				{
					foreach($publikasi4 as $row2)
					{

						$datasnew14[$row->aktivitasId] = $publikasi4;  

					}
				}            
			}

		}

		$data['publikasi41'] = $cetak14;
		$data['publikasi42'] = $datasnew14;
		$cetak15 = $this->{$this->_model_name}->cetak($key,'V'); 
		$datasnew15 =FALSE;
		$bahasa5 = false;
		if($cetak15!==false) {
			foreach ($cetak15 as $row) 
			{
				$bahasa5 = $this->{$this->_model_name}->cetak2($row->aktivitasId);

				if($bahasa5!=false)
				{
					foreach($bahasa5 as $row2)
					{

						$datasnew15[$row->aktivitasId] = $bahasa5;  

					}
				}            
			}

		}

		$data['bahasa1'] = $cetak15;
		$data['bahasa2'] = $datasnew15;
		$data['kodeetikII1'] = $this->{$this->_model_name}->cetak($key,'II.1'); 
		$data['insinyur'] = $this->{$this->_model_name}->insinyur('f_datapribadi',['fdpNim'=>$keyS]); 
		// $data['lampiran'] = $this->{$this->_model_name}->lampiran(['lampiranNim'=>$keyS]); 
		$data['tgl'] = $this->{$this->_model_name}->get_by_id('ref_tanggal', ['tanggalId'=>'1']);

        // $this->load->view($this->_path_page.'cetakformulir', $data, false);
		$this->pdfgenerator->generate($this->load->view($this->_path_page.'cetakformulir',$data,true),'cetakformulir','landscape','A4');

	}
	public function loadimage()
	{
		$file = $this->uri->segment(3);
		ob_clean();
		$path = FCPATH . '../assets/berkas/'. $file;
		$size = getimagesize($path);
		header('Content-Type:' . $size['mime']);
		switch ($size['mime']) {
			case 'image/png':
			$img = imagecreatefrompng($path);

			imagepng($img);
			break;

			default:
			$img = imagecreatefromjpeg($path);
			imagejpeg($img);
			break;
		}
		imagedestroy($img);
	}

	public function delete()
	{
		$keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
		$key = ['kompetensiId'=>$keyS];
		$proses = $this->{$this->_model_name}->delete('tb_kompetensi',$key);
		if ($proses) 
			message($this->_judul.' Berhasil Dihapus','success');
		else
		{
			$error = $this->db->error();
			message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
		}
	}
}
