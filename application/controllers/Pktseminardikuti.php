<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class pktseminardikuti extends MY_Controller {
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/pktseminardikuti/';
        $this->_path_js = 'pubkomtan';
        $this->_judul = 'IV.3 Seminar/Lokakarya Keinsinyuran Yang Diikuti';
        $this->_controller_name = 'pktseminardikuti';
        $this->_model_name = 'model_pubkomtan';
        $this->_page_index = 'index';   
        $this->load->model($this->_model_name,'',TRUE);
    }

    public function index()
    {
        $session_data = $this->session->userdata('logged_in');
        $data = $this->get_master($this->_path_page.$this->_page_index);
        if (($session_data['susrSgroupNama'] == 'Mhs') OR ($session_data['susrSgroupNama'] == 'ADMIN') )
        {

            $data['fdpNoKta'] = $this->{$this->_model_name}->get_ref_table('f_datapribadi');

        }else{
            $data['fdpNoKta'] = $this->{$this->_model_name}->bimbingan($session_data['susrSgroupNama_ori']);
        }
        $data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];
        $data['response_url'] = base_url($this->_controller_name.'/response');
        $data['susrSgroupNama_ori'] = $session_data['susrSgroupNama_ori'];
        $data['susrSgroupNama'] = $session_data['susrSgroupNama'];
        $this->load->view($this->_template, $data);
    }


    public function response()
    {
        $session_data = $this->session->userdata('logged_in');
        $this->form_validation->set_rules('fdpNoKta', 'pegmNIP', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $data = $this->get_master($this->_path_page . $this->_page_index);
                $data['ref_revisi'] = $this->{$this->_model_name}->get_by_id('ref_revisi', ['revisiNama'=>$session_data['susrSgroupNama']]);

                $data['nilaikompetensidosen_url'] = base_url($this->_controller_name . '/nilaikompetensidosen').'/';
                $pegmNIP = $this->input->post('fdpNoKta');
                $data['nip'] = $this->encryptions->encode($pegmNIP, $this->config->item('encryption_key'));
                $key = ['aktivitasFdpId'=>$pegmNIP];
                $form = ['aktivitasIdForm'=>'IV.3'];
                $data['datas'] = $this->{$this->_model_name}->all($key,$form);
                $data['create_url'] = base_url($this->_controller_name . '/create') . '/';
                $data['update_url'] = base_url($this->_controller_name . '/update') . '/';
                $data['delete_url'] = base_url($this->_controller_name . '/delete') . '/';
                $pages = $this->_path_page . 'response';
                $this->load->view($pages, $data);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }

    public function create()
    {	
        $session_data = $this->session->userdata('logged_in');
        $data = $this->get_master($this->_path_page.'form');	
        $data['fdpNoKta'] =$this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];
        $data['save_url'] = base_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['ref_kota'] = $this->{$this->_model_name}->get_ref_table('ref_kota');
        $data['ref_negara'] = $this->{$this->_model_name}->get_ref_table('ref_negara');
        $key = false;
        $urai2 = ['6','7','8','9','10','11','12','13'];
        $data['ref_uraiankompetensi'] = $this->{$this->_model_name}->uraiankompetensiid($key,'5',$urai2);
        $this->load->view($this->_template, $data);
    }

    public function update()
    {		
        $data = $this->get_master($this->_path_page.'form');	
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];
        $data['save_url'] = base_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Update';
        $key = ['aktivitasId'=>$keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['ref_kota'] = $this->{$this->_model_name}->get_ref_table('ref_kota');
        $data['ref_negara'] = $this->{$this->_model_name}->get_ref_table('ref_negara');
        $urai2 = ['6','7','8','9','10','11','12','13'];
        $data['ref_uraiankompetensi'] = $this->{$this->_model_name}->uraiankompetensiid($keyS,'5',$urai2);
        $this->load->view($this->_template, $data);
    }

    public function nilaikompetensidosen()
    {       
        $data = $this->get_master($this->_path_page.'dosenpenilai');    
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];
        $data['save_url'] = base_url($this->_controller_name.'/savenilai').'/';  
        $data['status_page'] = 'Update';
        $nim = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['datas2'] = $this->{$this->_model_name}->by_all(array('aktivitasFdpId' => $nim),'IV.3');
        $data['ref_kota'] = $this->{$this->_model_name}->get_ref_table('ref_kota');
        $data['ref_negara'] = $this->{$this->_model_name}->get_ref_table('ref_negara');
        $data['ref_negara'] = $this->{$this->_model_name}->get_ref_table('ref_negara');
        $data['ref_uraiankompetensi'] = $this->{$this->_model_name}->uraiannilaikompetensiid($keyS);
        $this->load->view($this->_template, $data);
    }
    public function savenilai()
    {       
        $session_data = $this->session->userdata('logged_in');
        $fopIdOld = $this->input->post('fopIdOld');
        $this->form_validation->set_rules('cekKomp[]','Modul','trim|required|xss_clean');
        $this->form_validation->set_rules('kompetensiNilaiP[]','Modul','trim|required|xss_clean');
        $this->form_validation->set_rules('kompetensiNilaiQ[]','Modul','trim|required|xss_clean');
        $this->form_validation->set_rules('kompetensiNilaiR[]','Modul','trim|required|xss_clean');

        if($this->form_validation->run()) 
        {   
            if(IS_AJAX)
            { 
                $kompetensiNilaiP = $this->input->post('kompetensiNilaiP');    
                $kompetensiNilaiQ = $this->input->post('kompetensiNilaiQ');
                $kompetensiNilaiR = $this->input->post('kompetensiNilaiR');
                $kompetensiNilaiT = $this->input->post('kompetensiNilaiT');
                $cekKomp = $this->input->post('cekKomp');  
                $kompetensiId= $this->input->post('kompetensiId');  

                foreach($cekKomp as $key=>$value)
                {

                    $paramkompetensi = array(
                        'kompetensiNilaiP'=>$kompetensiNilaiP[$key],
                        'kompetensiNilaiQ'=>$kompetensiNilaiQ[$key],
                        'kompetensiNilaiR'=>$kompetensiNilaiR[$key],
                        'kompetensiNilaiT'=>$kompetensiNilaiP[$key] * $kompetensiNilaiQ[$key] * $kompetensiNilaiR[$key],
                        'kompetensiUserPenilai'=>$session_data['susrSgroupNama_ori'],
                    ); 
                    $proses = $this->db->update('tb_kompetensi',$paramkompetensi,array('kompetensiId' => $value)); 

                };
                if($proses)
                    message($this->_judul.' Berhasil Disimpan','success');
                else
                {
                    $error = $this->db->error();
                    message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! '.validation_errors(),'error');
        }
    }

    public function save()
    {		
        $fpeIdOld = $this->input->post('fpeIdOld');
        $this->form_validation->set_rules('aktivitasFdpId','aktivitasFdpId','trim|xss_clean|required');
        $this->form_validation->set_rules('aktivitasNama','aktivitasNama','trim|xss_clean|required');
        $this->form_validation->set_rules('aktivitasTahunAwal','aktivitasTahunAwal','trim|xss_clean|required');
        $this->form_validation->set_rules('aktivitasUraian','aktivitasUraian','trim|xss_clean|required');
        $this->form_validation->set_rules('aktivitasLembaga','aktivitasLembaga','trim|xss_clean|required');
        $this->form_validation->set_rules('aktivitasKota','aktivitasKota','trim|xss_clean|required');
        $this->form_validation->set_rules('cekKompetensi[]','Modul','trim|required|xss_clean');


        if($this->form_validation->run()) 
        {	

            if(IS_AJAX)
            {
                $cekKompetensi = $this->input->post('cekKompetensi');
                $aktivitasFdpId = $this->input->post('aktivitasFdpId');
                $aktivitasNama = $this->input->post('aktivitasNama');
                $aktivitasTahunAwal = $this->input->post('aktivitasTahunAwal');
                $aktivitasUraian = $this->input->post('aktivitasUraian');
                $aktivitasLembaga = $this->input->post('aktivitasLembaga');
                $aktivitasTahunAwal = $this->input->post('aktivitasTahunAwal');
                $aktivitasKota = $this->input->post('aktivitasKota');
                $aktivitasNegara = $this->input->post('aktivitasNegara');


                $param = array(
                    'aktivitasFdpId'=>$aktivitasFdpId,
                    'aktivitasNama'=>$aktivitasNama,
                    'aktivitasTahunAwal'=>$aktivitasTahunAwal,
                    'aktivitasUraian'=>$aktivitasUraian,
                    'aktivitasLembaga'=>$aktivitasLembaga,
                    'aktivitasIdForm'=>'IV.3',
                    'aktivitasKota'=>$aktivitasKota,
                    'aktivitasNegara'=>$aktivitasNegara,
                    'aktivitasTahunAwal'=>$aktivitasTahunAwal,
                    'aktivitasTahunSemester'=> date('Y')

                );

                if(empty($fpeIdOld) and (empty($_FILES['uploadfile']['name']) ))
                {
                    message('Maaf.. silahkan Upload Berkas dalam bentuk PDF', 'error');
                    exit(); 
                } else {
                    $date_awal = date('Y-m-d i-s');
                    if(!empty($_FILES['uploadfile']['name']))
                    {
                        $uploadfilename = 'pubkomtan_1V3'.$aktivitasFdpId.'_'.$date_awal;
                        $konfig = array(
                            'url' => FCPATH .'public/assets/berkas/',
                            'type' => 'pdf',
                            'size' => 0.7*1024,
                            'namafile' => $uploadfilename
                        );

                        $this->load->library('UploadArsip');
                        $file_name = $this->uploadarsip->arsip($konfig, 'uploadfile');
                        if (!empty($file_name))
                            $param['aktivitasBerkas'] = $file_name;
                    }
                }

                if(count($cekKompetensi)<16)
                {

                    if(empty($fpeIdOld))
                    {
                        $proses = $this->{$this->_model_name}->insert('tb_aktivitas',$param);
                        $cekaktivitasave = $this->{$this->_model_name}->cekaktivitas($aktivitasFdpId,'IV.3',$file_name);
                        foreach ($cekKompetensi as $modul) { 

                            $paramkompetensi = array(
                                'kompetensiAktivitasId'=>$cekaktivitasave->aktivitasId,
                                'kompetensiUraianKompetensiId'=>$modul,
                                'kompetensiFdpId' =>$cekaktivitasave->aktivitasFdpId,

                            );   

                            $this->{$this->_model_name}->insert('tb_kompetensi',$paramkompetensi);
                        }
                        } else {
                            $key = array('aktivitasId'=>$fpeIdOld);
                            $proses = $this->{$this->_model_name}->update('tb_aktivitas',$param,$key);$this->{$this->_model_name}->delete('tb_kompetensi',array('kompetensiAktivitasId'=>$fpeIdOld));

                            foreach ($cekKompetensi as $modul) { 
                                $paramkompetensi = array(
                                    'kompetensiAktivitasId'=>$fpeIdOld,
                                    'kompetensiUraianKompetensiId'=>$modul,
                                    'kompetensiFdpId' =>$aktivitasFdpId,

                                ); 
                                $this->{$this->_model_name}->insert('tb_kompetensi',$paramkompetensi);
                            }
                        }

                    } else{

                        message('Maaf.. Uraian tidak boleh lebih dari 15', 'error');
                    } 

                    if($proses)
                        message($this->_judul.' Berhasil Disimpan','success');
                    else
                    {
                        $error = $this->db->error();
                        message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
                    }
                }
            } else {
                message('Ooops!! Something Wrong!! '.validation_errors(),'error');
            }
        }

        public function delete()
        {
            $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
            $key = ['aktivitasId'=>$keyS];
            $proses = $this->{$this->_model_name}->delete('tb_aktivitas',$key);
            if ($proses) 
                message($this->_judul.' Berhasil Dihapus','success');
            else
            {
                $error = $this->db->error();
                message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
            }
        }
    }
