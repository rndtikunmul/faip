<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class refbimbingan extends MY_Controller {
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/refbimbingan/';
        $this->_path_js = 'user';
        $this->_judul = 'Bimbingan';
        $this->_controller_name = 'refbimbingan';
        $this->_model_name = 'model_refbimbingan';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name,'',TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page.$this->_page_index);
        $data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];
        $data['response_url'] = base_url($this->_controller_name.'/response');
        $data['create_url'] = base_url($this->_controller_name . '/create') . '/';
        $data['fdpNoKta'] = $this->{$this->_model_name}->get_ref_table('f_datapribadi');
        $this->load->view($this->_template, $data);
    }

    public function response()
    {
        $this->form_validation->set_rules('fdpNoKta', 'pegmNIP', 'trim|required|xss_clean');
        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $data = $this->get_master($this->_path_page . $this->_page_index);
                $pegmNIP = $this->input->post('fdpNoKta');
                $data['nip'] = $this->encryptions->encode($pegmNIP, $this->config->item('encryption_key'));
                $key = ['bimbinganFdpId'=>$pegmNIP];
                $data['datas'] = $this->{$this->_model_name}->all($key);
                $data['update_url'] = base_url($this->_controller_name . '/update') . '/';
                $data['delete_url'] = base_url($this->_controller_name . '/delete') . '/';
                $data['create_url'] = base_url($this->_controller_name . '/create') . '/';

                // $data['download_url'] = base_url($this->_controller_name . '/download') . '/';

                $pages = $this->_path_page . 'response';
                $this->load->view($pages, $data);
            }
        } else {
            message('Ooops!! Something Wrong!!', 'error');
        }
    }

    public function create()
    {	
        $data = $this->get_master($this->_path_page.'form');	
        $data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];
        $data['fdpNoKta'] = $this->{$this->_model_name}->get_ref_table('f_datapribadi');
        $data['pembimbing'] = $this->{$this->_model_name}->get_ref_table('s_user','',['susrSgroupNama'=>'Dosen']);
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['save_url'] = base_url($this->_controller_name.'/save').'/';	

        $this->load->view($this->_template, $data);
    }

    public function update()
    {		
        $data = $this->get_master($this->_path_page.'form');	
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];
        $data['save_url'] = base_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Update';
        $key = ['bimbinganId'=>$keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('ref_bimbingan',$key);
        $keyfdatapribadi = ['fdpNim'=>$data['datas']->bimbinganFdpId];
        $data['pembimbing'] = $this->{$this->_model_name}->get_ref_table('s_user','',['susrSgroupNama'=>'Dosen']);
        $data['fdpNoKta'] = $this->{$this->_model_name}->get_ref_table('f_datapribadi','',  $keyfdatapribadi);
        $this->load->view($this->_template, $data);
    }

    public function save()
    {		
        $bimbinganIdOld = $this->input->post('bimbinganIdOld');
        $this->form_validation->set_rules('bimbinganFdpId','bimbinganFdpId','trim|xss_clean');
        $this->form_validation->set_rules('bimbinganNip','bimbinganNip','trim|xss_clean');

        if($this->form_validation->run()) 
        {	
            if(IS_AJAX)
            {
                $bimbinganFdpId = $this->input->post('bimbinganFdpId');
                $bimbinganNip = $this->input->post('bimbinganNip');
                $bimbinganStatus = $this->input->post('bimbinganStatus');
                $bimbinganKet = $this->input->post('bimbinganKet');


                $param = array(
                    'bimbinganFdpId'=>$bimbinganFdpId,
                    'bimbinganNip'=>$bimbinganNip,
                    'bimbinganStatus'=>$bimbinganStatus,
                    'bimbinganKet'=>$bimbinganKet,

                );
                // print_r($bimbinganIdOld); exit;

                if(empty($bimbinganIdOld))
                {
                    $proses = $this->{$this->_model_name}->insert('ref_bimbingan',$param);
    			//echo $this->db->last_query();
				// exit;
                } else {
                    $key = array('bimbinganId'=>$bimbinganIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_bimbingan',$param,$key);
                }

                if($proses)
                    message($this->_judul.' Berhasil Disimpan','success');
                else
                {
                    $error = $this->db->error();
                    message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! '.validation_errors(),'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $key = ['bimbinganId'=>$keyS];
        $proses = $this->{$this->_model_name}->delete('ref_bimbingan',$key);
        if ($proses) 
            message($this->_judul.' Berhasil Dihapus','success');
        else
        {
            $error = $this->db->error();
            message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
        }
    }
}
