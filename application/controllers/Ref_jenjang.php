<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class ref_jenjang extends MY_Controller {
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/ref_jenjang/';
        $this->_path_js = null;
        $this->_judul = 'Ref_jenjang';
        $this->_controller_name = 'ref_jenjang';
        $this->_model_name = 'model_ref_jenjang';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name,'',TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page.$this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('ref_jenjang');
        $data['create_url'] = base_url($this->_controller_name.'/create').'/';
        $data['update_url'] = base_url($this->_controller_name.'/update').'/';
        $data['delete_url'] = base_url($this->_controller_name.'/delete').'/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {	
        $data = $this->get_master($this->_path_page.'form');	
        $data['scripts'] = [];	
        $data['save_url'] = base_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        
        $this->load->view($this->_template, $data);
    }

    public function update()
    {		
        $data = $this->get_master($this->_path_page.'form');	
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $data['scripts'] = [];	
        $data['save_url'] = base_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Update';
        $key = ['jenjangId'=>$keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('ref_jenjang',$key);
        
        $this->load->view($this->_template, $data);
    }

    public function save()
    {		
        $jenjangIdOld = $this->input->post('jenjangIdOld');$this->form_validation->set_rules('jenjangNama','jenjangNama','trim|xss_clean');

        if($this->form_validation->run()) 
        {	
            if(IS_AJAX)
            {
                $jenjangNama = $this->input->post('jenjangNama');
                

                $param = array(
                    'jenjangNama'=>$jenjangNama,

                );

                if(empty($jenjangIdOld))
                {
                    $proses = $this->{$this->_model_name}->insert('ref_jenjang',$param);
                } else {
                    $key = array('jenjangId'=>$jenjangIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_jenjang',$param,$key);
                }

                if($proses)
                    message($this->_judul.' Berhasil Disimpan','success');
                else
                {
                    $error = $this->db->error();
                    message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! '.validation_errors(),'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $key = ['jenjangId'=>$keyS];
        $proses = $this->{$this->_model_name}->delete('ref_jenjang',$key);
        if ($proses) 
            message($this->_judul.' Berhasil Dihapus','success');
        else
        {
            $error = $this->db->error();
            message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
        }
    }
}
