<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class refrevisi extends MY_Controller {
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/refrevisi/';
        $this->_path_js = 'user';
        $this->_judul = 'Refrevisi';
        $this->_controller_name = 'refrevisi';
        $this->_model_name = 'model_refrevisi';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name,'',TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page.$this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('ref_revisi');
        $data['create_url'] = base_url($this->_controller_name.'/create').'/';
        $data['update_url'] = base_url($this->_controller_name.'/update').'/';
        $data['delete_url'] = base_url($this->_controller_name.'/delete').'/';
        $data['revisi_url'] = base_url($this->_controller_name.'/revisi').'/';

        $this->load->view($this->_template, $data);
    }

    public function create()
    {	
        $data = $this->get_master($this->_path_page.'form');	
        $data['scripts'] = [];	
        $data['save_url'] = base_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {		
        $data = $this->get_master($this->_path_page.'form');	
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $data['scripts'] = [];	
        $data['save_url'] = base_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Update';
        $key = ['revisiId'=>$keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('ref_revisi',$key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {		
        $revisiIdOld = $this->input->post('revisiIdOld');$this->form_validation->set_rules('revisiNama','revisiNama','trim|xss_clean');
        $this->form_validation->set_rules('revisiket','revisiket','trim|xss_clean');

        if($this->form_validation->run()) 
        {	
            if(IS_AJAX)
            {
                $revisiNama = $this->input->post('revisiNama');
                $revisiket = $this->input->post('revisiket');


                $param = array(
                    'revisiNama'=>$revisiNama,
                    'revisiket'=>$revisiket,

                );

                if(empty($revisiIdOld))
                {
                    $proses = $this->{$this->_model_name}->insert('ref_revisi',$param);
                } else {
                    $key = array('revisiId'=>$revisiIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_revisi',$param,$key);
                }

                if($proses)
                    message($this->_judul.' Berhasil Disimpan','success');
                else
                {
                    $error = $this->db->error();
                    message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! '.validation_errors(),'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $key = ['revisiId'=>$keyS];
        $proses = $this->{$this->_model_name}->delete('ref_revisi',$key);
        if ($proses) 
            message($this->_judul.' Berhasil Dihapus','success');
        else
        {
            $error = $this->db->error();
            message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
        }
    }

    public function revisi()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $key = ['revisiId'=>$keyS];
        $ganti = $this->{$this->_model_name}->get_by_id('ref_revisi',$key);

        if ($ganti->revisiket == 0){
            $gantistatus = 1;
        }else{
            $gantistatus = 0;
        }
            $param = array(
                'revisiket'=>$gantistatus,
             );
      
        $proses = $this->{$this->_model_name}->update('ref_revisi',$param,$key);
        if ($proses) 
            message($this->_judul.' Berhasil Di Simpan','success');
        else
        {
            $error = $this->db->error();
            message($this->_judul.' Gagal Simpan, '.$error['code'].': '.$error['message'],'error');
        }
    }
}
