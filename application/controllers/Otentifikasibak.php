<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class otentifikasi extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();	
		$this->load->library('Aksesapi');	
		$this->load->model('model_login','',TRUE);
	}

	public function index()
	{		
		$this->form_validation->set_rules('username','username','trim|required|xss_clean');
		$this->form_validation->set_rules('password','password','trim|required|xss_clean|callback_check_database');

		if($this->form_validation->run() == FALSE)
		{
			$result['status'] = 'danger';
            $result['message'] = 'Incorrect Username or Password. Please try again.';
        }
		else
		{
			$result['status'] = 'success';
            $result['message'] = 'You have successfully logged in.';
			$result['redirect_url'] = base_url().'home';
		}

		$this->output->set_content_type('application/json');
	    $this->output->set_output(json_encode($result));
	    echo $this->output->get_output();
	    exit();
	}

	// function check_database($password)
	// {
	// 	$username = $this->input->post('username');
	// 	$row = $this->model_login->get_by_id('s_user',['susrNama'=>$username]);

	// 	if($row)
	// 	{		
	// 		if(password_verify($password, $row->susrPassword))
	// 		{
	// 			$sess_array = array(
	// 				'susrNama' => $row->susrNama,
	// 				'susrSgroupNama' => $row->susrSgroupNama,
	// 				'susrSgroupNama_ori' => $row->susrSgroupNama,
	// 				'susrProfil' => $row->susrProfil
	// 			);
	// 			$this->model_login->update('s_user',array('susrLastLogin'=>date('Y-m-d H:i:s')),array('susrNama' => $row->susrNama));
	// 			$this->session->set_userdata('logged_in',$sess_array);
	// 			if(function_exists('debuglog'))
	// 				debuglog('Otentifikasi: '.date('Y-m-d H:i:s'),$sess_array);
	// 			return TRUE;
	// 		} else
	// 			return FALSE;
	// 	}
	// 	else
	// 	{	
	// 		return FALSE;
	// 	}
	// }	

	function check_database($password)
	{
		$api = new Aksesapi();
		$this->load->library('Apii');
		$username = $this->input->post('username');
		$row = $this->model_login->get_by_id('s_user',['susrNama'=>$username]);
		$datamhs= $api->GetAccess($username, $password);
		$datadosen= $api->GetDosen($username, $password);
		$proses = getUserAPI($username, $password);	
		$datatoken = $this->apii->PostLogin('unmul','UnivMul4w@rman');
		if($row)
		{		
			if(password_verify($password, $row->susrPassword))
			{
				$sess_array = array(
					'susrNama' => $row->susrNama,
					'susrSgroupNama' => $row->susrSgroupNama,
					'susrSgroupNama_ori' => $row->susrSgroupNama,
					'susrProfil' => $row->susrProfil,
					'token' => $datatoken->token
				);
				$this->model_login->update('s_user',array('susrLastLogin'=>date('Y-m-d H:i:s')),array('susrNama' => $row->susrNama));
				$this->session->set_userdata('logged_in',$sess_array);
				if(function_exists('debuglog'))
					debuglog('Otentifikasi: '.date('Y-m-d H:i:s'),$sess_array);
				return TRUE;
			} else
				return FALSE;
		}
		elseif(isset($datamhs->sessionId))
		{	
			$susrSgroupNama = 'Mhs';
			$nim =  $datamhs->nim;
			$cekmhs=$this->model_login->get_by_id('f_datapribadi',['fdpNoKta'=>$username]);

				if($cekmhs == FALSE) {
					
						$sess_array = array(
						'susrNama' => $datamhs->name,
						'susrSgroupNama' =>  $susrSgroupNama,
						'susrSgroupNama_ori' => $datamhs->nim,
						'sessionId' => $datamhs->sessionId,
						'susrProfil' => $datamhs->name,
						'ipk' => $datamhs->ipk,
						'token' => $datatoken->token
					);
					$this->session->set_userdata('logged_in',$sess_array);
			   	  return 0;
				}
				else
				{ 
					$sess_array = array(
						'susrNama' => $datamhs->name,
						'susrSgroupNama' =>  $susrSgroupNama,
						'susrSgroupNama_ori' => $datamhs->nim,
						'susrProfil' => $datamhs->name,
						'sessionId' => $datamhs->sessionId,
						'ipk' => $datamhs->ipk
					);
					$this->session->set_userdata('logged_in',$sess_array);
					return TRUE;
				}

		}
		elseif(isset($datadosen->sessionId))
		{	
			$susrSgroupNama = 'Dosen';
			$sess_array = array(
				'susrNama' => $datadosen->name,
				'susrSgroupNama' =>  $susrSgroupNama,
				'susrSgroupNama_ori' => $datadosen->nip,
				'susrProfil' => $datadosen->nip
			);
			$this->session->set_userdata('logged_in',$sess_array);
			return TRUE;
		}
		elseif($proses['status'] === TRUE)
		{	
			$row = $this->model_login->get_by_id('s_user', ['susrNama' => $username]);
			$datas = $proses['datas'];
			
			$sess_array = array(
				'susrNama' => $datas['user'],
				'susrSgroupNama' => !empty($row)?$row->susrSgroupNama:'Dosen',
				'susrSgroupNama_ori' => $datas['user'],
				'susrProfil' => $datas['profilName']
			);
			
			$this->model_login->update('s_user', array('susrLastLogin' => date('Y-m-d H:i:s')), array('susrNama' => $username));
			$this->session->set_userdata('logged_in', $sess_array);
			if (function_exists('debuglog'))
				debuglog('Otentifikasi: ' . date('Y-m-d H:i:s'), $sess_array);
			return TRUE;
		}
		else
		{	
			return FALSE;
		}
	}	
}
