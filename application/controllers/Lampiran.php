<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class lampiran  extends MY_Controller {
  function __construct()
  {
    parent::__construct();
    $this->_template = 'layouts/template';
    $this->_path_page = 'pages/lampiran/';
    $this->_path_js = 'datapribadi';
    $this->_judul = 'Lampiran';
    $this->_controller_name = 'lampiran';
    $this->_model_name = 'model_lampiran';
    $this->_model_name2 = 'model_tb_kompetensi';
    $this->_page_index = 'index';
    $this->_page_index2 = 'lampiran';
    $this->load->model($this->_model_name,'',TRUE);
    $this->load->model($this->_model_name2,'',TRUE);
  }

    public function index()
    {
      $session_data = $this->session->userdata('logged_in');

      if ($session_data['susrSgroupNama'] == 'Mhs')
      {

        $data = $this->get_master($this->_path_page.$this->_page_index2);

      }else{
        $data = $this->get_master($this->_path_page.$this->_page_index);

      }

     
      $key = ['lampiranNim'=>$session_data['susrSgroupNama_ori']]; 
      $data['datas'] = $this->{$this->_model_name}->by_id($key);
      $data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];
      $data['response_url'] = base_url($this->_controller_name.'/response');
      $data['susrSgroupNama_ori'] = $session_data['susrSgroupNama_ori'];
      $data['fdpNoKta'] = $session_data['susrSgroupNama_ori'];
      $data['nip'] = $this->encryptions->encode($session_data['susrSgroupNama_ori'], $this->config->item('encryption_key'));
      $data['save_url'] = base_url($this->_controller_name.'/save').'/'; 
      $this->load->view($this->_template, $data);
    }

function cetaklampiran()
    {

        ini_set('max_execution_time', 0);
        $this->load->library('pdfgenerator');
        $session_data = $this->session->userdata('logged_in');
        $keyS =  $session_data['susrSgroupNama_ori'];

        $data['insinyur'] = $this->{$this->_model_name}->insinyur('f_datapribadi',['fdpNim'=>$keyS]); 
        $data['lampiran'] = $this->{$this->_model_name}->lampiran(['lampiranNim'=>$keyS]);      
// $this->load->view($this->_path_page.'cetaklampiran', $data, FALSE);
        $this->pdfgenerator->generate($this->load->view($this->_path_page.'cetaklampiran',$data,true),'cetaklampiran','potrait');

    }

    function cetaknilai()
{

 $session_data = $this->session->userdata('logged_in');    
    $this->load->library('pdfgenerator');
    $this->load->helper('tgl_indo'); 
     $data['fdpNoKta'] = $session_data['susrSgroupNama_ori']; 
    $this->pdfgenerator->generate($this->load->view($this->_path_page.'cetak',$data,true),'cetakformulir','potrait');

}


    public function response()
    {
     $session_data = $this->session->userdata('logged_in');     
     $this->form_validation->set_rules('tahun', 'pegmNIP', 'trim|required|xss_clean');
     if ($this->form_validation->run()) {
      if (IS_AJAX) {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['ref_revisi'] = $this->{$this->_model_name}->get_by_id('ref_revisi', ['revisiNama'=>$session_data['susrSgroupNama']]);
        $pegmNIP = $this->input->post('fdpNim');
        $tahun = $this->input->post('tahun');
        $data['nip'] = $this->encryptions->encode($tahun, $this->config->item('encryption_key'));
        $key = $tahun;
        $where = $pegmNIP;
        $data['datas'] = $this->{$this->_model_name}->all($key,$where); 
        $data['create_url'] = base_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = base_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = base_url($this->_controller_name . '/delete') . '/';
        $pages = $this->_path_page . 'response';
        $this->load->view($pages, $data);
      }
    } else {
      message('Ooops!! Something Wrong!!', 'error');
    }
  }




  public function create()
  {	
    $data = $this->get_master($this->_path_page.'form');	
    $data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];
    $data['save_url'] = base_url($this->_controller_name.'/save').'/';	
    $data['status_page'] = 'Create';
    $data['datas'] = false;
    $data['fdpNoKta'] =$this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
    $key = false;
    $this->load->view($this->_template, $data);
  }

    public function update()
    {		
      $data = $this->get_master($this->_path_page.'lampiran');	
      $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
      $data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];
      $data['save_url'] = base_url($this->_controller_name.'/save').'/';	
      $data['status_page'] = 'Update';
      $key = ['lampiranNim'=>$keyS];
      $data['datas'] = $this->{$this->_model_name}->by_id($key);
      $this->load->view($this->_template, $data);
    }


    public function save()
    {		
      $fptIdOld = $this->input->post('fptIdOld');
      $this->form_validation->set_rules('lampiranNamaProyek','lampiranNamaProyek','trim|xss_clean|required');
      $this->form_validation->set_rules('lampiranAtasan','lampiranAtasan','trim|xss_clean|required');
      $this->form_validation->set_rules('lampiranUraian','lampiranUraian','trim|xss_clean|required');
      if($this->form_validation->run()) 
      {	
       if(IS_AJAX)
       {
         $lampiranNim = $this->input->post('lampiranNim');
         $lampiranNamaProyek = $this->input->post('lampiranNamaProyek');
         $lampiranAtasan = $this->input->post('lampiranAtasan');
         $lampiranUraian = $this->input->post('lampiranUraian');
         $lampiranPutusan = $this->input->post('lampiranPutusan');
         $lampiranWaktu = $this->input->post('lampiranWaktu');
         $lampiranAktifitas = $this->input->post('lampiranAktifitas');

         $param = array(
          'lampiranNim'=>$lampiranNim,
          'lampiranNamaProyek'=>$lampiranNamaProyek,
          'lampiranWaktu'=>$lampiranWaktu,
          'lampiranPutusan'=>$lampiranPutusan,
          'lampiranAtasan'=>$lampiranAtasan,
          'lampiranUraian'=>$lampiranUraian,
          'lampiranAktifitas'=>$lampiranAktifitas,

        );

          $cek = $this->{$this->_model_name}->by_id(['lampiranNim'=>$lampiranNim]);
         $date_awal = date('Y-m-d i-s');
         if(!empty($_FILES['uploadfile']['name']))
         {
           $uploadfilename = 'lampiran'.$lampiranNim.'_'.$date_awal;
           $konfig = array(
            'url' => FCPATH .'public/assets/berkas/',
            'type' => 'jpg',
            'size' => 0.3*1024,
            'namafile' => $uploadfilename
          );

           $this->load->library('UploadArsip');
           $file_name = $this->uploadarsip->arsip($konfig, 'uploadfile');
           if (!empty($file_name))
            $param['lampiranBagan'] = $file_name;
        } 


     if((empty($fptIdOld)) and $cek == false ) 
        {
          $proses = $this->{$this->_model_name}->insert('tb_lampiran',$param);

        } else {
          $key = array('lampiranNim'=>$lampiranNim);
          $proses = $this->{$this->_model_name}->update('tb_lampiran',$param,$key);
        }

        if($proses)
          message($this->_judul.' Berhasil Disimpan','success');
        else
        {
          $error = $this->db->error();
          message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
        }
      }
    } else {
      message('Ooops!! Something Wrong!! '.validation_errors(),'error');
    }
    }

    public function delete()
    {
      $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
      $key = ['aktivitasId'=>$keyS];
      $proses = $this->{$this->_model_name}->delete('tb_aktivitas',$key);
      if ($proses) 
        message($this->_judul.' Berhasil Dihapus','success');
      else
      {
        $error = $this->db->error();
        message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
      }
    }




}
