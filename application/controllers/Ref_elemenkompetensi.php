<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class ref_elemenkompetensi extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/ref_elemenkompetensi/';
        $this->_path_js = null;
        $this->_judul = 'Elemen Kompetensi';
        $this->_controller_name = 'ref_elemenkompetensi';
        $this->_model_name = 'model_ref_elemenkompetensi';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = base_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = base_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = base_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = base_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['ref_unitkompetensi'] = $this->{$this->_model_name}->get_ref_table('ref_unitkompetensi');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = base_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['elemenKompetensiId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['ref_unitkompetensi'] = $this->{$this->_model_name}->get_ref_table('ref_unitkompetensi');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $elemenKompetensiIdOld = $this->input->post('elemenKompetensiIdOld');
        $this->form_validation->set_rules('elemenunitKompetensiId', 'elemenunitKompetensiId', 'trim|xss_clean');
        $this->form_validation->set_rules('elemenKompetensiNama', 'elemenKompetensiNama', 'trim|xss_clean');
        $this->form_validation->set_rules('elemenKompetensiKet', 'elemenKompetensiKet', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                // echo '<pre>';
                // print_r(($elemenKompetensiIdOld));
                // echo '</pre>'; exit;
                $elemenunitKompetensiId = $this->input->post('elemenunitKompetensiId');
                $elemenKompetensiNama = $this->input->post('elemenKompetensiNama');
                $elemenKompetensiKet = $this->input->post('elemenKompetensiKet');


                $param = array(
                    'elemenunitKompetensiId' => $elemenunitKompetensiId,
                    'elemenKompetensiNama' => $elemenKompetensiNama,
                    'elemenKompetensiKet' => $elemenKompetensiKet,

                );

                if (empty($elemenKompetensiIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_elemenkompetensi', $param);
                } else {
                    $key = array('elemenKompetensiId' => $elemenKompetensiIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_elemenkompetensi', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['elemenKompetensiId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('ref_elemenkompetensi', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}
