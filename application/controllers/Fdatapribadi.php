<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class Fdatapribadi extends MY_Controller {
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/fdatapribadi/';
        $this->_path_js = 'datapribadi';
        $this->_judul = 'I.1 Data Pribadi Umum';
        $this->_controller_name = 'fdatapribadi';
        $this->_model_name = 'model_fdatapribadi';
        $this->_page_index = 'index';
        $this->_page_index2 = 'datapribadimhs';

        $this->load->model($this->_model_name,'',TRUE);
    }

    public function index()
    {
        $session_data = $this->session->userdata('logged_in'); 
        if ($session_data['susrSgroupNama'] == 'Mhs')
        {

          $data = $this->get_master($this->_path_page.$this->_page_index2);

      }else{
          $data = $this->get_master($this->_path_page.$this->_page_index);

      }
      
      $key = ['fdpNim'=>$session_data['susrSgroupNama_ori']]; 
      $data['datas'] = $this->{$this->_model_name}->by_id($key);
      $data['response_url'] = base_url($this->_controller_name.'/response');
      $data['save_url'] = base_url($this->_controller_name.'/save').'/'; 
      $data['ref_kota'] = $this->{$this->_model_name}->get_ref_table('ref_kota');
      $data['fdpNoKta'] = $session_data['susrSgroupNama_ori'];
      $data['fdatapribadi'] = $this->{$this->_model_name}->get_ref_table('f_datapribadi');
      $data['fdpNama'] = $session_data['susrNama'];
      $data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];
      $data['create_url'] = base_url($this->_controller_name . '/create') . '/';
      $this->load->view($this->_template, $data);
  }

  public function response()
  {
    $this->form_validation->set_rules('tahun', 'fdpNim', 'trim|required|xss_clean');
    if ($this->form_validation->run()) {
        if (IS_AJAX) {
            $data = $this->get_master($this->_path_page . $this->_page_index);
            $fdpNim = $this->input->post('fdpNim');
            $tahun = $this->input->post('tahun');
            $data['fdpNim'] = $this->encryptions->encode($fdpNim, $this->config->item('encryption_key'));
            $key = $tahun;
            $where = $fdpNim;
            $data['datas'] = $this->{$this->_model_name}->all($key,$where); 
            $data['update_url'] = base_url($this->_controller_name . '/update') . '/';
            $data['delete_url'] = base_url($this->_controller_name . '/delete') . '/'; 
            $data['sinkron_url'] = base_url($this->_controller_name . '/sinkron') . '/';          
            $pages = $this->_path_page . 'response';
            $this->load->view($pages, $data);
        }
    } else {
        message('Ooops!! Something Wrong!!', 'error');
    }
}

public function sinkron()
{
    $session_data = $this->session->userdata('logged_in'); 
    $this->load->library('Apii');
    $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
    $key = ['fdpNim'=>$keyS];
    $fdpnotIdUser = $this->{$this->_model_name}->idsingkron($key);
    $fpf = $this->{$this->_model_name}->edu(['aktivitasFdpId'=>$keyS]);
    $fpx = $this->{$this->_model_name}->exp(['aktivitasFdpId'=>$keyS]);
    $bhs_skill = $this->{$this->_model_name}->organisasi(['aktivitasFdpId'=>$keyS],'V');
    $forganisasi = $this->{$this->_model_name}->organisasi(['aktivitasFdpId'=>$keyS],'I.3');
    $fpenghargaan = $this->{$this->_model_name}->organisasi(['aktivitasFdpId'=>$keyS],'I.4');
    $fpelatihantek = $this->{$this->_model_name}->organisasi(['aktivitasFdpId'=>$keyS],'I.5');
    $fpelatihanlain = $this->{$this->_model_name}->organisasi(['aktivitasFdpId'=>$keyS],'I.6');
    $frefkodeetik = $this->{$this->_model_name}->organisasi(['aktivitasFdpId'=>$keyS],'II.1');
    $fpendapat = $this->{$this->_model_name}->organisasi(['aktivitasFdpId'=>$keyS],'II.2');
    $fpengalamanmengajar = $this->{$this->_model_name}->organisasi(['aktivitasFdpId'=>$keyS],'III.2');
    $fpktkaryatulis = $this->{$this->_model_name}->organisasi(['aktivitasFdpId'=>$keyS],'IV.1');
    $fmakalah = $this->{$this->_model_name}->organisasi(['aktivitasFdpId'=>$keyS],'IV.2');
    $fpenelitian = $this->{$this->_model_name}->organisasi(['aktivitasFdpId'=>$keyS],'IV.3');
    $fkaryatemuan = $this->{$this->_model_name}->organisasi(['aktivitasFdpId'=>$keyS],'IV.4');
    $lampiran =  $this->{$this->_model_name}->lampiran($keyS) ;
    $perstart = $this->{$this->_model_name}->periode($keyS) ;
    $GetUsersimponi = $this->apii->GetUserId($session_data['token'],'users', $fdpnotIdUser->fdpEmail);
    $cekfaipId = $this->apii->Getfaip_id($session_data['token'],'faip',$fdpnotIdUser->fdpEmail);
    // $Getfaip =  $this->apii->GetFaip($session_data['token'],'faip_111',(reset($cekfaipId))->faip_id);
         // print_r($GetUsersimponi);
         // print_r($cekfaipId);
         // exit();   
    if ($GetUsersimponi == false)
    {
      message('Maaf Data Anda Tidak Ada di Simponi, Pastikan Email di EIRUMPPI sama dengan di SIMPONI', 'error');
      exit();
  }else{

    if (empty($fdpnotIdUser->fdpUserId) and ($GetUsersimponi == TRUE))
    {
       $GetUserId = $this->apii->GetUserId($session_data['token'],'users', $fdpnotIdUser->fdpEmail); 
       $userID = (reset($GetUserId))->user_id; 
       $userNoKta = (reset($GetUserId))->no_kta; 
       $updsfdpUserId  = $this->{$this->_model_name}->update('f_datapribadi',
        array('fdpUserId' => $userID, 'fdpNoKta' => $userNoKta)
        ,$key);
   }


   $fdpidfaip = $this->{$this->_model_name}->idsingkron($key);


   if ($cekfaipId == false)
   {

       $dataUserfaip =array(
           'periodstart' => $perstart->aktivitasTahunlulus,
           'periodend'=> date('Y'),
           'faip_type'=>'00',
           'certificate_type'=> '',
           'user_id' => $fdpidfaip->fdpUserId
       );
       $infaip =  $this->apii->Insert($session_data['token'],'faip',$dataUserfaip);
       $GetfaipId = $this->apii->Getfaip_id($session_data['token'],'faip',$fdpidfaip->fdpEmail);
       $faipID = (reset($GetfaipId))->faip_id; 
       $updfdfaipId  = $this->{$this->_model_name}->update('f_datapribadi',array( 'fdpFaipId' =>$faipID),$key);

   }else{
       $GetfaipId = $this->apii->Getfaip_id($session_data['token'],'faip',$fdpidfaip->fdpEmail);
       $faipID = (reset($GetfaipId))->faip_id; 
       $updfdfaipId  = $this->{$this->_model_name}->update('f_datapribadi',array( 'fdpFaipId' =>$faipID),$key);
   }
          // print_r($GetfaipId);
          // exit();

   $fdp = $this->{$this->_model_name}->idsingkron($key);

        //Alamat Rumah
        // $dataAddressHome = array( 'addresstype' => 1,
        //     'address' => $fdp->fdpAlamatRumah,
        //     'city' => $fdp->city,
        //     'province' => $fdp->provinsirumah,
        //     'phone'=>$fdp->fdpTelpon,
        //     'zipcode' =>$fdp->fdpKodePos,
        //     'is_mailing'=>1,
        //     'user_id' =>  $fdp->fdpUserId );
        //   $inAddressHome =  $this->apii->Insert($session_data['token'],'user_address',$dataAddressHome);

        //Alamat Kantor
        // $dataAddressWork = array( 'addresstype' => 2,
        //     'address' => $fdp->fdpAlamatLembaga,
        //     'city' => $fdp->citywork,
        //     'province' => $fdp->provinsiwork,
        //     'phone'=>$fdp->fdpTelponLembaga, 
        //     'zipcode' =>$fdp->fdpKodePosLembaga,
        //     'is_mailing'=>1,
        //     'user_id' => $fdp->fdpUserId );
        //       $inAddressWork =  $this->apii->Insert($session_data['token'],'user_address',$dataAddressWork);
        // User Education
      // if($fpf!==false) 
      //     { foreach ($fpf as $row) 
      //         {               
      //          $datauser_edu =
      //          array( 'type' => 1,
      //             'school' => $row->aktivitasNama,
      //             'startdate' => '',
      //             'enddate' => $row->aktivitasTahunlulus,
      //             'degree' => $row->jenjangNama,
      //             'fieldofstudy'=>$row->aktivitasJumlah,
      //             'mayor'=>$row->aktivitasTahunAwal,
      //             'title_prefix' =>  $row->aktivitasTahunAwal,
      //             'title'=> $row->aktivitasTahunAkhir,
      //             'score' => $row->aktivitasNilai,
      //             'activities' => $row->aktivitasJudulTa,
      //             'description' => $row->aktivitasUraian,
      //             'attachment'=> base64_encode(@file_get_contents(base_url().'public/assets/berkas/'.$row->aktivitasBerkas)),
      //             'user_id' => $fdp->fdpUserId
      //         );
      //             $inUser_edu =  $this->apii->Insert($session_data['token'],'user_edu',$datauser_edu); 
      //      }   
      //  } 

         //  //Pengalaman Kerja 
         // if($fpx!==false) 
         //  { foreach ($fpx as $row) 
         //      { 
         //          $dataUser_exp = array(
         //            'company' => $row->aktivitasNama,
         //            'title' => $row->aktivitasJumlah,
         //            'location' => $row->kotaNama,
         //            'provinsi' => $row->propNama,
         //            'negara' => $row->negNama,
         //            'startmonth' => '',
         //            'startyear' => $row->aktivitasTahunAwal,
         //            'endmonth' => '',
         //            'endyear' => $row->aktivitasTahunAkhir,
         //            'is_present' => 1,
         //            'actv' => $row->aktivitasNama,
         //            'description' => $row->aktivitasUraian,
         //            'attachment'=> base64_encode(@file_get_contents(base_url().'public/assets/berkas/'.$row->aktivitasBerkas)),
         //            'user_id' => $fdp->fdpUserId );
         //            $inUser_exp =  $this->apii->Insert($session_data['token'],'user_exp',$dataUser_exp);
         //      }   
         //  } 


    //FAIP_111 Alamat Rumah
   $datafaip11home = array( 
       'faip_id' => $fdp->fdpFaipId ,
       'addr_type' => 1,
       'addr_desc' => $fdp->fdpAlamatRumah,
       'addr_loc' => $fdp->city,
       'addr_zip' =>$fdp->fdpKodePos,
       'user_id' =>  $fdp->fdpUserId );
   $inAddressHome =  $this->apii->Insert($session_data['token'],'faip_111',$datafaip11home);



     //FAIP_111 Alamat Kantor
   $datafaip11work = array( 
       'faip_id' => $fdp->fdpFaipId,
       'addr_type' => 2,
       'addr_desc' => $fdp->fdpAlamatLembaga,
       'addr_loc' => $fdp->citywork,
       'addr_zip' =>$fdp->fdpKodePosLembaga,
       'user_id' =>  $fdp->fdpUserId );
   $inAddressWork =  $this->apii->Insert($session_data['token'],'faip_111',$datafaip11work);


        //FAIP_112 EXP
   $datafaip112 = array( 
      'faip_id' => $fdp->fdpFaipId,
      'exp_name' => $fdp->fdpNamaLembaga,
      'exp_desc' => $fdp->fdpJabatan,
      'exp_loc' => $fdp->citywork,
      'exp_zip' =>$fdp->fdpKodePosLembaga,
      'user_id' =>  $fdp->fdpUserId );
   $infaip112 =  $this->apii->Insert($session_data['token'],'faip_112',$datafaip112);

        //FAIP_113 Telepon Rumah
   $datafaip113home = array( 
    'faip_id' => $fdp->fdpFaipId,
    'phone_type' => 'home_phone',
    'phone_value' => $fdp->fdpTelpon,
    'user_id' =>  $fdp->fdpUserId );
   $infaip113home =  $this->apii->Insert($session_data['token'],'faip_113',$datafaip113home);

        //FAIP_113 Telepon Kantor
   $datafaip113work = array( 
      'faip_id' => $fdp->fdpFaipId,
      'phone_type' => 'office_phone',
      'phone_value' => $fdp->fdpTelponLembaga,
      'user_id' =>  $fdp->fdpUserId );
   $infaip113work =  $this->apii->Insert($session_data['token'],'faip_113',$datafaip113work);

        //FAIP_113 Telepon Seluler

   $datafaip113cell = array( 
      'faip_id' => $fdp->fdpFaipId,
      'phone_type' => 'mobile_phone',
      'phone_value' => $fdp->fdpNoSeluler,
      'user_id' =>  $fdp->fdpUserId );
   $infaip113cell =  $this->apii->Insert($session_data['token'],'faip_113',$datafaip113cell);

           //FAIP_113 Faksimil Rumah
   $datafaip113homefax = array( 
      'faip_id' => $fdp->fdpFaipId,
      'phone_type' => 'homefax_phone',
      'phone_value' => $fdp->fdpFaksimilrumah,
      'user_id' =>  $fdp->fdpUserId );
   $infaip113homefax =  $this->apii->Insert($session_data['token'],'faip_113',$datafaip113homefax);


        //FAIP_113 Faksimil Kantor
   $datafaip113workfax = array( 
    'user_id' =>  $fdp->fdpUserId,
    'faip_id' => $fdp->fdpFaipId,
    'phone_type' => 'workfax_phone',
    'phone_value' => $fdp->fdpFaksimilLembaga );
   $infaip113workfax =  $this->apii->Insert($session_data['token'],'faip_113',$datafaip113workfax);


         // FAIP 1.2 Berhasil

   if($fpf!==false) 
   { 
      foreach ($fpf as $row) 
      {               
         $datafaip_12 =
         array( 
          'user_id' =>  $fdp->fdpUserId, 
          'faip_id' => $fdp->fdpFaipId,
          'school_type' => $row->jenjangNama,
          'school' => $row->aktivitasNama,
          'fakultas'=>$row->aktivitasJumlah,
          'jurusan'=>$row->aktivitasTahunAwal,
          'kota' =>  $row->kotaNama,
          'provinsi'=> $row->propNama,
          'negara' => $row->negNama,
          'tahun_lulus' => $row->aktivitasTahunlulus,
          'title' => $row->aktivitasTahunAkhir,
          'judul'=> $row->aktivitasJudulTa,
          'uraian'=> $row->aktivitasUraian,
          'score'=> $row->aktivitasNilai ,
          'judicium'=> $row->aktivitasPeriode,
          'attachment' =>  base64_encode(@file_get_contents(base_url().'public/assets/berkas/'.$row->aktivitasBerkas))
      ); 
         $infaip_12 =  $this->apii->Insert($session_data['token'],'faip_12',$datafaip_12);
     } 
 } 

              //FAIP 1.3
 if($forganisasi!==false) 
  { foreach ($forganisasi as $row) 
      { 
          $komfaip_13=  $this->{$this->_model_name}->kompetensifaip(['aktivitasFdpId'=>$keyS],'I.3',$row->aktivitasId);
          $datafaip_13 = array(
            'user_id' => $fdp->fdpUserId ,
            'faip_id' => $fdp->fdpFaipId , 
            'nama_org' => $row->aktivitasNama,
            'tingkat' => '',
            'jabatan' => $row->aktivitasJumlah,
            'lingkup' => '',
            'tempat' => $row->kotaNama,
            'provinsi'=> $row->propNama,
            'negara' => $row->negNama,
            'startmonth' => '',
            'startyear' => $row->aktivitasTahunAwal,
            'endmonth' => '',
            'endyear' => $row->aktivitasTahunAkhir,
            'is_present' => 1,
            'aktifitas' => $row->aktivitasUraian,
            'attachment'=>  base64_encode(@file_get_contents(base_url().'public/assets/berkas/'.$row->aktivitasBerkas)),
            'kompetensi' =>(reset($komfaip_13))->komNama);
          $indatafaip_13 =  $this->apii->Insert($session_data['token'],'faip_13',$datafaip_13);
      }   
  } 
     //FAIP 1.4
  if($fpenghargaan!==false)  
   { foreach ($fpenghargaan as $row) 
    { 
        $komfaip_14 =  $this->{$this->_model_name}->kompetensifaip(['aktivitasFdpId'=>$keyS],'I.4',$row->aktivitasId);
        $datafaip_14 = array(
            'user_id' => $fdp->fdpUserId,
            'faip_id' => $fdp->fdpFaipId, 
            'startdate' => '',
            'startyear' => $row->aktivitasTahunAwal,
            'nama' => $row->aktivitasNama,
            'lembaga' => $row->aktivitasJumlah,
            'location' => '',
            'provinsi'=> '',
            'negara' => $row->negNama,
            'tingkat' => $row->propNama,
            'tingkatlembaga' => $row->kotaNama,
            'total' => '',
            'uraian' => $row->aktivitasUraian,
            'attachment'=>  base64_encode(@file_get_contents(base_url().'public/assets/berkas/'.$row->aktivitasBerkas)),
            'kompetensi' =>(reset($komfaip_14))->komNama);
        $indatafaip_14 =  $this->apii->Insert($session_data['token'],'faip_14',$datafaip_14);
    }   
} 

     //FAIP 1.5
if($fpelatihantek!==false) 
 { foreach ($fpelatihantek as $row) 
     { 
      $komfaip_15=  $this->{$this->_model_name}->kompetensifaip(['aktivitasFdpId'=>$keyS],'I.5',$row->aktivitasId);
      $datafaip_15 = array(
       'user_id' => $fdp->fdpUserId ,
       'faip_id' => $fdp->fdpFaipId ,
       'nama' => $row->aktivitasNama,
       'lembaga' => $row->aktivitasLembaga,
       'location' => $row->kotaNama,
       'provinsi'=> $row->propNama,
       'negara' => $row->negNama,
       'startdate' => '',
       'startyear' => $row->aktivitasPeriode,
       'enddate' => '',
       'endyear' => '',
       'is_present' => '1',
       'jam' => '',
       'tingkat' => $row->lvlatihNama,
       'lic' => $row->aktivitasTahunAkhir,
       'url' => '',
       'uraian' => $row->aktivitasUraian,
       'attachment'=>  base64_encode(@file_get_contents(base_url().'public/assets/berkas/'.$row->aktivitasBerkas)),
       'status' => '1',
       'jam_desc' => $row->aktivitasJumlah,
       'tingkat_desc' => '',
       'kompetensi' =>(reset($komfaip_15))->komNama);
      $indatafaip_15 =  $this->apii->Insert($session_data['token'],'faip_15',$datafaip_15);
  }   
} 

        //FAIP 1.6
if($fpelatihanlain!==false) 
    { foreach ($fpelatihanlain as $row) 
        { 
            $komfaip_16=  $this->{$this->_model_name}->kompetensifaip(['aktivitasFdpId'=>$keyS],'I.6',$row->aktivitasId);
            $datafaip_16 = array(
              'user_id' => $fdp->fdpUserId ,
              'faip_id' => $fdp->fdpFaipId ,
              'nama' => $row->aktivitasNama,
              'lembaga' => $row->aktivitasLembaga,
              'location' => $row->kotaNama,
              'provinsi'=> $row->propNama,
              'negara' => $row->negNama,
              'startdate' => '',
              'startyear' => $row->aktivitasPeriode,
              'enddate' => '',
              'endyear' => '',
              'is_present' => '1',
              'jam' => '',
              'tingkat' => $row->lvlatihNama,
              'lic' => $row->aktivitasTahunAkhir,
              'url' => '',
              'uraian' => $row->aktivitasUraian,
              'attachment'=>  base64_encode(@file_get_contents(base_url().'public/assets/berkas/'.$row->aktivitasBerkas)),
              'status' => '1',
              'jam_desc' => $row->aktivitasJumlah,
              'tingkat_desc' => $row->lvlatihNama,
              'kompetensi' =>(reset($komfaip_16))->komNama);

            $indatafaip_16 =  $this->apii->Insert($session_data['token'],'faip_16',$datafaip_16);
        }   
    } 

        //FAIP 2.1 Ref Kode Etik
    if($frefkodeetik!==false) 
     { foreach ($frefkodeetik as $row) 
         { 
             $komfaip_21=  $this->{$this->_model_name}->kompetensifaip(['aktivitasFdpId'=>$keyS],'II.1',$row->aktivitasId);
             $datafaip_21 = array(
                 'user_id' => $fdp->fdpUserId ,
                 'faip_id' => $fdp->fdpFaipId ,
                 'nama' => $row->aktivitasNama,
                 'lembaga' => '',
                 'notelp'=> $row->aktivitasTahunAwal,
                 'email'=> '',
                 'kota' => '',
                 'provinsi'=> '',
                 'negara' => '',
                 'alamat' => $row->aktivitasJumlah,
                 'hubungan' => $row->aktivitasUraian,
                 'status' => '1',
                 'kompetensi' =>(reset($komfaip_21))->komNama);

             $indatafaip_21 =  $this->apii->Insert($session_data['token'],'faip_21',$datafaip_21);
         }   
     } 

       //FAIP 2.2 Pengalaman & Pendapat Sendiri
     if($fpendapat!==false) 
         { foreach ($fpendapat as $row) 
             { 
                 $komfaip_22=  $this->{$this->_model_name}->kompetensifaip(['aktivitasFdpId'=>$keyS],'II.2',$row->aktivitasId);
                 $datafaip_22 = array(
                   'user_id' => $fdp->fdpUserId ,
                   'faip_id' => $fdp->fdpFaipId ,
                   'uraian' => $row->aktivitasUraian,
                   'status' => '1',
                   'kompetensi' =>(reset($komfaip_22))->komNama);

                 $indatafaip_22 =  $this->apii->Insert($session_data['token'],'faip_22',$datafaip_22);
             }   
         } 


        //FAIP 3
         if($fpx!==false) 
             { foreach ($fpx as $row) 
                 { 
                     $komfaip_3=  $this->{$this->_model_name}->kompetensifaip3(['aktivitasFdpId'=>$keyS],$row->aktivitasId);
                     $datafaip_3 = array(
                       'user_id' => $fdp->fdpUserId ,
                       'faip_id' => $fdp->fdpFaipId ,
                       'startdate' =>'' , 
                       'startyear' => $row->aktivitasTahunAwal, 
                       'enddate' => '', 
                       'endyear'=> $row->aktivitasTahunAkhir, 
                       'periode' => '', 
                       'is_present' =>'' , 
                       'instansi'=> $row->aktivitasLembaga,
                       'pemberitugas'=> '',
                       'location' =>  $row->kotaNama,
                       'provinsi'=>  $row->propNama,
                       'negara' => $row->negNama,
                       'namaproyek' => $row->aktivitasNama,
                       'title' => '',
                       'nilaipry'=> '',
                       'nilaijasa'=> '',
                       'nilaisdm' => '',
                       'nilaisulit' => '',
                       'nilaiproyek' =>'',
                       'posisi' => '',
                       'uraian' => $row->aktivitasUraian,
                       'attachment' =>  base64_encode(@file_get_contents(base_url().'public/assets/berkas/'.$row->aktivitasBerkas)),
                       'kompetensi' =>(reset($komfaip_3))->komNama );

                     $indatafaip_3 =  $this->apii->Insert($session_data['token'],'faip_3',$datafaip_3);
                 }   
             } 

       //FAIP 4 III. 2 Pengalaman Mengajar Pelajaran Keinsinyuran dan/atau Manajemen
             if($fpengalamanmengajar!==false) 
                 { foreach ($fpengalamanmengajar as $row) 
                     { 
                         $komfaip_4=  $this->{$this->_model_name}->kompetensifaip(['aktivitasFdpId'=>$keyS],'III.2',$row->aktivitasId);
                         $datafaip_4 = array(
                           'user_id' => $fdp->fdpUserId ,
                           'faip_id' => $fdp->fdpFaipId ,
                           'startdate' => '',
                           'startyear' => $row->aktivitasTahunAwal,
                           'enddate' => '',
                           'endyear' => $row->aktivitasTahunAkhir,
                           'periode' => '',
                           'is_present' => '1',
                           'instansi' => $row->aktivitasLembaga ,
                           'pemberitugas' => '' ,
                           'location' => $row->kotaNama,
                           'provinsi' => $row->propNama,
                           'negara' => $row->negNama ,
                           'namaproyek' => $row->aktivitasNama ,
                           'jumlahsks' => '',
                           'posisi' => '',
                           'uraian' => $row->aktivitasUraian,
                           'attachment' =>  base64_encode(@file_get_contents(base_url().'public/assets/berkas/'.$row->aktivitasBerkas)),
                           'status' => $row->aktivitasUraian,
                           'posisi_desc' => '1',
                           'jumlahsks_desc' => $row->aktivitasJumlah ,
                           'periode_desc' => '',
                           'kompetensi' =>(reset($komfaip_4))->komNama);

                         $indatafaip_4 =  $this->apii->Insert($session_data['token'],'faip_4',$datafaip_4);
                     }   
                 } 

       //FAIP 5.1 IV.1 KARYA TULIS DI BIDANG KEINSINYURAN YANG DIPUBLIKASIKAN 
                 if($fpktkaryatulis!==false) 
                     { foreach ($fpktkaryatulis as $row) 
                         { 
                             $komfaip_51=  $this->{$this->_model_name}->kompetensifaip(['aktivitasFdpId'=>$keyS],'IV.1',$row->aktivitasId);
                             $datafaip_51 = array(
                               'user_id' => $fdp->fdpUserId ,
                               'faip_id' => $fdp->fdpFaipId ,
                               'startdate' => '',
                               'startyear' => $row->aktivitasTahunAwal,
                               'nama' => $row->aktivitasNama,
                               'media' => $row->aktivitasLembaga,
                               'location' => $row->aktivitasJumlah,
                               'provinsi' => $row->propNama ,
                               'negara' => $row->negNama,
                               'tingkat' => '',
                               'tingkatmedia' => '',
                               'namaproyek' => '',
                               'jumlah' => '',
                               'uraian' => $row->aktivitasUraian,
                               'attachment' =>  base64_encode(@file_get_contents(base_url().'public/assets/berkas/'.$row->aktivitasBerkas)),
                               'status' => '1',
                               'tingkat_desc' => '',
                               'tingkatmedia_desc' => '',
                               'kompetensi' =>(reset($komfaip_51))->komNama);

                             $indatafaip_51 =  $this->apii->Insert($session_data['token'],'faip_51',$datafaip_51);
                         }   
                     } 

       //FAIP 5.2 IV.2 Makalah/Tulisan Yang Disajikan Dalam Seminar 
                     if($fmakalah!==false) 
                         { foreach ($fmakalah as $row) 
                             { 
                                 $komfaip_52=  $this->{$this->_model_name}->kompetensifaip(['aktivitasFdpId'=>$keyS],'IV.2',$row->aktivitasId);
                                 $datafaip_52 = array(
                                   'user_id' => $fdp->fdpUserId ,
                                   'faip_id' => $fdp->fdpFaipId ,
                                   'startdate' => '',
                                   'startyear' => $row->aktivitasTahunAwal,
                                   'nama' => $row->aktivitasNama,
                                   'penyelenggara' => $row->aktivitasLembaga,
                                   'location' => $row->kotaNama,
                                   'provinsi' => '',
                                   'negara' => $row->negNama,
                                   'judul' => $row->aktivitasJumlah,
                                   'tingkat' => '',
                                   'tingkatseminar' => '',
                                   'jumlah' => '',
                                   'uraian' => $row->aktivitasUraian,
                                   'attachment' => base64_encode(@file_get_contents(base_url().'public/assets/berkas/'.$row->aktivitasBerkas)),
                                   'status' => '1',
                                   'tingkat_desc' => '',
                                   'tingkatseminar_desc' => '',
                                   'kompetensi' =>(reset($komfaip_52))->komNama);

                                 $indatafaip_52 =  $this->apii->Insert($session_data['token'],'faip_52',$datafaip_52);
                             }   
                         } 

       //FAIP 5.3 IV.3 Pengalaman Dalam Penelitian 
                         if($fpenelitian!==false) 
                             { foreach ($fpenelitian as $row) 
                                 { 
                                     $komfaip_53=  $this->{$this->_model_name}->kompetensifaip(['aktivitasFdpId'=>$keyS],'IV.3',$row->aktivitasId);
                                     $datafaip_53 = array(
                                       'user_id' => $fdp->fdpUserId ,
                                       'faip_id' => $fdp->fdpFaipId ,
                                       'startdate' => '',
                                       'startyear' => $row->aktivitasTahunAwal,
                                       'nama' => $row->aktivitasNama,
                                       'penyelenggara' => $row->aktivitasLembaga,
                                       'location' => $row->kotaNama,
                                       'provinsi' => $row->propNama,
                                       'negara' => $row->negNama,
                                       'tingkat' => '',
                                       'tingkatseminar' => '',
                                       'jumlah' => '',
                                       'uraian' => $row->aktivitasUraian,
                                       'attachment' => base64_encode(@file_get_contents(base_url().'public/assets/berkas/'.$row->aktivitasBerkas)),
                                       'status' => '1',
                                       'tingkat_desc' => '',
                                       'tingkatseminar_desc' => '',
                                       'kompetensi' =>(reset($komfaip_53))->komNama);

                                     $indatafaip_53 =  $this->apii->Insert($session_data['token'],'faip_53',$datafaip_53);
                                 }   
                             } 

        //FAIP 5.4 IV.4 KARYA TEMUAN/INOVASI/PATEN DAN IMPLEMENTASI TEKNOLOGI BARU  
                             if($fkaryatemuan!==false) 
                                { foreach ($fkaryatemuan as $row) 
                                    { 
                                        $komfaip_54=  $this->{$this->_model_name}->kompetensifaip(['aktivitasFdpId'=>$keyS],'IV.4',$row->aktivitasId);
                                        $datafaip_54 = array(
                                          'user_id' => $fdp->fdpUserId ,
                                          'faip_id' => $fdp->fdpFaipId ,
                                          'startdate' => '',
                                          'startyear' => $row->aktivitasTahunAwal,
                                          'nama' => $row->aktivitasNama,
                                          'media_publikasi' => $row->aktivitasLembaga,
                                          'tingkat' => '',
                                          'tingkatseminar' => '',
                                          'jumlah' => '',
                                          'uraian' => $row->aktivitasUraian,
                                          'attachment' =>  base64_encode(@file_get_contents(base_url().'public/assets/berkas/'.$row->aktivitasBerkas)),
                                          'status' => '1',
                                          'tingkat_desc' => '',
                                          'tingkatseminar_desc' => '',
                                          'kompetensi' =>(reset($komfaip_54))->komNama);

                                        $indatafaip_54 =  $this->apii->Insert($session_data['token'],'faip_54',$datafaip_54);
                                    }   
                                } 

         //FAIP 6
                                if($bhs_skill!==false) 
                                 { foreach ($bhs_skill as $row) 
                                     { 
                                         $komfaip_6=  $this->{$this->_model_name}->kompetensifaip(['aktivitasFdpId'=>$keyS],'V',$row->aktivitasId);
                                         if ($row->aktivitasUraian=='Aktif')
                                         {
                                          $verbal = '3' ;
                                      }else {
                                          $verbal = '2';
                                      }

                                      $datafaip_6= array(
                                        'user_id' => $fdp->fdpUserId,
                                        'faip_id' => $fdp->fdpFaipId ,
                                        'nama' => $row->bahasaNama,
                                        'jenisbahasa' => '',
                                        'verbal'=> $verbal,
                                        'jenistulisan' => $row->aktivitasLembaga,
                                        'jumlah' => '',
                                        'nilai' =>$row->aktivitasJumlah,
                                        'uraian' => '',
                                        'attachment'=>  base64_encode(@file_get_contents(base_url().'public/assets/berkas/'.$row->aktivitasBerkas)),
                                        'kompetensi' =>(reset($komfaip_6))->komNama );

                                      $indatafaip_6 =  $this->apii->Insert($session_data['token'],'faip_6',$datafaip_6);
                                  }   
                              } 

       //FAIP Lampiran
                              if($lampiran!==false) 
                                 { foreach ($lampiran as $row) 
                                     { 
                                        $datalampiran= array(
                                            'user_id' => $fdp->fdpUserId,
                                            'faip_id' => $fdp->fdpFaipId ,
                                            'aktifitas' => $row->lampiranAktifitas,
                                            'nama' => $row->lampiranNamaProyek,
                                            'namaproyek'=> $row->lampiranNamaProyek,
                                            'jangka' => $row->lampiranWaktu,
                                            'atasan' => $row->lampiranAtasan,
                                            'uraianproyek' =>$row->lampiranUraian,
                                            'uraiantugas' => $row->lampiranPutusan,
                                            'bagan'=>'',
                                            'attachment'=> base64_encode(@file_get_contents(base_url().'public/assets/berkas/'.$row->lampiranBagan))
                                        );

                                        $indatafaip_lamp =  $this->apii->Insert($session_data['token'],'faip_lam',$datalampiran);
                                    }   
                                } 
       
                                  // exit();
                                  $updatesink  = $this->{$this->_model_name}->update('f_datapribadi',array( 'fdpsinkron' =>'1'),$key);

                              }
                              if ($updatesink) {
                                  message($this->_judul.' Berhasil Sinkron Ke Simponi','success');
                              }
                              else
                              {
                                $error = $this->db->error();
                                message($this->_judul.' Gagal Singkron, '.$error['code'].': '.$error['message'],'error');
                            }
                        }


                        public function sinkronfatapribadi()
                        {
                            $session_data = $this->session->userdata('logged_in'); 
                            $this->load->library('Apii');
                            $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
                            $key = ['fdpNim'=>$keyS];
                            $fdpnotIdUser = $this->{$this->_model_name}->idsingkron($key);
                            $fpf = $this->{$this->_model_name}->edu(['aktivitasFdpId'=>$keyS]);
                            $fpx = $this->{$this->_model_name}->exp(['aktivitasFdpId'=>$keyS]);
                            $lampiran =  $this->{$this->_model_name}->lampiran($keyS) ;
                            $perstart = $this->{$this->_model_name}->periode($keyS) ;
                            $GetUsersimponi = $this->apii->GetUserId($session_data['token'],'users', $fdpnotIdUser->fdpEmail);
                            $cekfaipId = $this->apii->Getfaip_id($session_data['token'],'faip',$fdpnotIdUser->fdpEmail);

         // print_r($GetfaipId);
         // print_r($cekfaipId);
         //     exit();
                            if ($GetUsersimponi == false)
                            {

                              message('Maaf Data Anda Tidak Ada di Simponi', 'error');
                              exit();
                          }elseif ($cekfaipId == TRUE) {
                            message('Maaf Data Faip Sudah Ada', 'error');
                            exit();
                        }else{
                            if (empty($fdpnotIdUser->fdpUserId))
                            {


            //Data Pribadi
           // $dataUser =array(
           //    'user_id' =>$fdpnotIdUser->fdpUserId,
           //     'firstname' => $fdpnotIdUser->fdpNama,
           //     'lastname'=> '',
           //     'gender'=>$fdpnotIdUser->fdpJenKel,
           //     'idtype'=> '',
           //     'idcard'=>'',
           //     'birthplace'=>$fdpnotIdUser->kotaNama,
           //     'dob'=>$fdpnotIdUser->fdpTanggalLahir,
           //     'mobilephone'=>$fdpnotIdUser->fdpNoSeluler,
           //     'website'=> '',
           //     'description' => '',
           //      'photo' => base64_encode(@file_get_contents(base_url().'public/assets/berkas/'.$fdpnotIdUser->fdpFoto)),
           //     'id_file'=>'');
             // $upd=  $this->apii->Updateuser($session_data['token'],$fdpnotIdUser->fdpUserId,$dataUser);
                               $GetUserId = $this->apii->GetUserId($session_data['token'],'users', $fdpnotIdUser->fdpEmail); 
                               $userID = (reset($GetUserId))->user_id; 
                               $userNoKta = (reset($GetUserId))->no_kta; 
                               $updsfdpUserId  = $this->{$this->_model_name}->update('f_datapribadi',
                                array('fdpUserId' => $userID, 'fdpNoKta' => $userNoKta)
                                ,$key);
                           }
             // $Get= $this->apii->Get($session_data['token'],'users', $fdpnotIdUser->fdpUserId); 

             //  print_r( $Get);
              // exit();

                           $updatesink  = $this->{$this->_model_name}->update('f_datapribadi',array( 'fdpsinkron' =>'1'),$key);

                       }
                       if ($updatesink) {
                          message($this->_judul.' Berhasil Sinkron Ke Simponi','success');
                      }
                      else
                      {
                        $error = $this->db->error();
                        message($this->_judul.' Gagal Singkron, '.$error['code'].': '.$error['message'],'error');
                    }
                }


                public function create()
                {	
                    $data = $this->get_master($this->_path_page.'form');	
                    $data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];
                    $data['save_url'] = base_url($this->_controller_name.'/save').'/';	
                    $data['status_page'] = 'Create';
                    $data['datas'] = false;
                    $data['ref_kota'] = $this->{$this->_model_name}->get_ref_table('ref_kota');

                    $this->load->view($this->_template, $data);
                }

                public function update()
                {		
                    $data = $this->get_master($this->_path_page.'form');	
                    $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
                    $data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];
                    $data['save_url'] = base_url($this->_controller_name.'/save').'/';	
                    $data['status_page'] = 'Update';
                    $key = ['fdpId'=>$keyS];
                    $data['datas'] = $this->{$this->_model_name}->by_id($key);
                    $data['ref_kota'] = $this->{$this->_model_name}->get_ref_table('ref_kota');

                    $this->load->view($this->_template, $data);
                }

                public function save()
                {		
                    $fdpIdOld = $this->input->post('fdpIdOld');
                    $this->form_validation->set_rules('fdpNama','fdpNama','trim|xss_clean|required');
                    $this->form_validation->set_rules('fdpEmail','fdpEmail','trim|xss_clean|required');
                    $this->form_validation->set_rules('fdpTempatLahir','fdpTempatLahir','trim|xss_clean|required');
                    $this->form_validation->set_rules('fdpTanggalLahir','fdpTanggalLahir','trim|xss_clean|required');
                    $this->form_validation->set_rules('fdpNoSeluler','fdpNoSeluler','trim|xss_clean');
                    $this->form_validation->set_rules('fdpTahunAwal','fdpTahunAwal','trim|xss_clean|required');
                    $this->form_validation->set_rules('fdpTahunAkhir','fdpTahunAkhir','trim|xss_clean|required');
                    $this->form_validation->set_rules('fdpBadanKejuruan','fdpBadanKejuruan','trim|xss_clean|required');
                    $this->form_validation->set_rules('fdpTahunLulus','fdpTahunLulus','trim|xss_clean|required');
                    $this->form_validation->set_rules('fdpAlamatRumah','fdpAlamatRumah','trim|xss_clean|required');
                    $this->form_validation->set_rules('fdpKota','fdpKota','trim|xss_clean');
                    $this->form_validation->set_rules('fdpNamaLembaga','fdpNamaLembaga','trim|xss_clean');
                    $this->form_validation->set_rules('fdpJabatan','fdpJabatan','trim|xss_clean');
                    $this->form_validation->set_rules('fdpAlamatLembaga','fdpAlamatLembaga','trim|xss_clean');
                    $this->form_validation->set_rules('fdpKotaLembaga','fdpKotaLembaga','trim|xss_clean');
                    $this->form_validation->set_rules('fdpTelponLembaga','fdpTelponLembaga','trim|xss_clean');

                    if($this->form_validation->run()) 
                    {	
                       $this->load->library('Upload');
             // $arrayTipeFile = array('application/pdf'=>'.pdf');
                       $arrayTipeFile = array('image/jpeg'=>'.jpg');
                       if(IS_AJAX)
                       {
                        $fdpNoKta = $this->input->post('fdpNoKta');
                        $fdpNama = $this->input->post('fdpNama');
                        $fdpEmail = $this->input->post('fdpEmail');
                        $fdpTempatLahir = $this->input->post('fdpTempatLahir');
                        $fdpTanggalLahir = $this->input->post('fdpTanggalLahir');
                        $fdpNim = $this->input->post('fdpNim');
                        $fdpTahunAwal = $this->input->post('fdpTahunAwal');
                        $fdpTahunAkhir = $this->input->post('fdpTahunAkhir');
                        $fdpBadanKejuruan = $this->input->post('fdpBadanKejuruan');
                        $fdpTahunLulus = $this->input->post('fdpTahunLulus');
                        $fdpInsinyurProfesionalPII = $this->input->post('fdpInsinyurProfesionalPII');
                        $fdpAlamatRumah = $this->input->post('fdpAlamatRumah');
                        $fdpKota = $this->input->post('fdpKota');
                        $fdpKodePos = $this->input->post('fdpKodePos');
                        $fdpTelpon = $this->input->post('fdpTelpon');
                        $fdpNamaLembaga = $this->input->post('fdpNamaLembaga');
                        $fdpJabatan = $this->input->post('fdpJabatan');
                        $fdpJabatanLain = $this->input->post('fdpJabatanLain');
                        $fdpAlamatLembaga = $this->input->post('fdpAlamatLembaga');
                        $fdpKotaLembaga = $this->input->post('fdpKotaLembaga');
                        $fdpKodePosLembaga = $this->input->post('fdpKodePosLembaga');
                        $fdpTelponLembaga = $this->input->post('fdpTelponLembaga');
                        $fdpFaksimilrumah = $this->input->post('fdpFaksimilrumah');
                        $fdpFaksimilLembaga = $this->input->post('fdpFaksimilLembaga');
                        $fdpNoSeluler = $this->input->post('fdpNoSeluler');
                        $fdpTelexRumah = $this->input->post('fdpTelexRumah');
                        $fdpTelexLembaga = $this->input->post('fdpTelexLembaga');
                        $fdpJenKel = $this->input->post('fdpJenKel');


                        $param = array(
                            'fdpNoKta'=>$fdpNoKta,
                            'fdpNama'=>$fdpNama,
                            'fdpEmail'=>$fdpEmail,
                            'fdpTempatLahir'=>$fdpTempatLahir,
                            'fdpTanggalLahir'=>$fdpTanggalLahir,
                            'fdpNim'=>$fdpNim,
                            'fdpTahunAwal'=>$fdpTahunAwal,
                            'fdpTahunAkhir'=>$fdpTahunAkhir,
                            'fdpBadanKejuruan'=>$fdpBadanKejuruan,
                            'fdpTahunLulus'=>$fdpTahunLulus,
                            'fdpInsinyurProfesionalPII'=>$fdpInsinyurProfesionalPII,
                            'fdpAlamatRumah'=>$fdpAlamatRumah,
                            'fdpKota'=>$fdpKota,
                            'fdpKodePos'=>$fdpKodePos,
                            'fdpTelpon'=>$fdpTelpon,
                            'fdpNamaLembaga'=>$fdpNamaLembaga,
                            'fdpJabatan'=>$fdpJabatan,
                            'fdpJabatanLain'=>$fdpJabatanLain,
                            'fdpAlamatLembaga'=>$fdpAlamatLembaga,
                            'fdpKotaLembaga'=>$fdpKotaLembaga,
                            'fdpKodePosLembaga'=>$fdpKodePosLembaga,
                            'fdpTelponLembaga'=>$fdpTelponLembaga,
                            'fdpFaksimilrumah'=>$fdpFaksimilrumah,
                            'fdpFaksimilLembaga'=>$fdpFaksimilLembaga,
                            'fdpNoSeluler'=>$fdpNoSeluler,
                            'fdpTelexRumah'=>$fdpTelexRumah,
                            'fdpTelexLembaga'=>$fdpTelexLembaga, 
                            'fdpJenKel'=>$fdpJenKel,    

                        );
        // print_r($param);
        // exit();

                        $cek = $this->{$this->_model_name}->by_id(['fdpNim'=>$fdpNim]);
                        if(empty($fdpIdOld) and (empty($_FILES['uploadfile']['name']) ))
                        {
                            message('Maaf.. Gagal Simpan Data . Silahkan Upload Berkas Dalam Bentuk JPG. Max 700Kb ');
                            exit(); 
                        } else
                        {
                         $date_awal = date('Y-m-d H');
                         if(!empty($_FILES['uploadfile']['name']))
                         {
                            $uploadfilename = 'foto'.$fdpNoKta.'_'.$date_awal;
                            $konfig = array(
                               'url' => FCPATH .'public/assets/berkas/',
                               'type' => 'jpg|JPEG',
                               'size' => 0.7*1024,
                               'namafile' => $uploadfilename
                           );

                            $this->load->library('UploadArsip');
                            $file_name = $this->uploadarsip->arsip($konfig, 'uploadfile');
                            if (!empty($file_name))
                                $param['fdpFoto'] = $file_name;

                        }
                    }


                    if((empty($fdpIdOld)) and $cek == false)
                    {
                        $proses = $this->{$this->_model_name}->insert('f_datapribadi',$param);
                    } else {
                        $key = array('fdpNim'=>$fdpNim);
                        $proses = $this->{$this->_model_name}->update('f_datapribadi',$param,$key);
                    }


                    if($proses)
                        message($this->_judul.' Berhasil Disimpan','success');
                    else
                    {
                        $error = $this->db->error();
                        message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
                    }
                }
            } else {
                message('Ooops!! Something Wrong!! '.validation_errors(),'error');
            }
        }


        public function loadimage()
        {
            $file = $this->uri->segment(3);
            ob_clean();
            $path = FCPATH . '../assets/berkas/'. $file;
            $size = getimagesize($path);
            header('Content-Type:' . $size['mime']);
            switch ($size['mime']) {
                case 'image/png':
                $img = imagecreatefrompng($path);

                imagepng($img);
                break;

                default:
                $img = imagecreatefromjpeg($path);
                imagejpeg($img);
                break;
            }
            imagedestroy($img);
        }


        public function delete()
        {
            $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
            $key = ['fdpId'=>$keyS];
            $proses = $this->{$this->_model_name}->delete('f_datapribadi',$key);
            if ($proses) 
                message($this->_judul.' Berhasil Dihapus','success');
            else
            {
                $error = $this->db->error();
                message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
            }
        }
    }
