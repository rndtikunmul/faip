<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class fpendformal extends MY_Controller {
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/fpendformal/';
        $this->_path_js = 'datapribadi';
        $this->_judul = 'I.2 Pendidikan Formal';
        $this->_controller_name = 'fpendformal';
        $this->_model_name = 'model_fpendformal';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name,'',TRUE);
    }

    public function index()
    {
        $session_data = $this->session->userdata('logged_in');
        $data = $this->get_master($this->_path_page.$this->_page_index);
        if (($session_data['susrSgroupNama'] == 'Mhs') OR ($session_data['susrSgroupNama'] == 'ADMIN') )
        {

            $data['fdpNoKta'] = $this->{$this->_model_name}->get_ref_table('f_datapribadi');

        }else{
            $data['fdpNoKta'] = $this->{$this->_model_name}->bimbingan($session_data['susrSgroupNama_ori']);
        }
        $data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];
        $data['response_url'] = base_url($this->_controller_name.'/response');
        $data['susrSgroupNama_ori'] = $session_data['susrSgroupNama_ori'];

        $this->load->view($this->_template, $data);
    }


    public function response()
    {
       $session_data = $this->session->userdata('logged_in');
       $this->form_validation->set_rules('fdpNoKta', 'pegmNIP', 'trim|required|xss_clean');
       if ($this->form_validation->run()) {
        if (IS_AJAX) {
            $data = $this->get_master($this->_path_page . $this->_page_index);
            $data['ref_revisi'] = $this->{$this->_model_name}->get_by_id('ref_revisi', ['revisiNama'=>$session_data['susrSgroupNama']]);
            $pegmNIP = $this->input->post('fdpNoKta');
            $data['nip'] = $this->encryptions->encode($pegmNIP, $this->config->item('encryption_key'));
            $key = ['aktivitasFdpId'=>$pegmNIP];
            $data['datas'] = $this->{$this->_model_name}->all($key);
            // echo $this->db->last_query(); exit(); 
            $data['create_url'] = base_url($this->_controller_name . '/create') . '/';
            $data['update_url'] = base_url($this->_controller_name . '/update') . '/';
            $data['delete_url'] = base_url($this->_controller_name . '/delete') . '/';
            $pages = $this->_path_page . 'response';
            $this->load->view($pages, $data);
        }
    } else {
        message('Ooops!! Something Wrong!!', 'error');
    }
}


public function create()
{	
    $session_data = $this->session->userdata('logged_in');
    $data = $this->get_master($this->_path_page.'form');	
    $data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];
    $data['save_url'] = base_url($this->_controller_name.'/save').'/';	
    $data['status_page'] = 'Create';
    $data['datas'] = false;
    $data['fdpNoKta'] =$this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
    $data['ref_jenjang'] = $this->{$this->_model_name}->get_ref_table('ref_jenjang');
    $data['ref_kota'] = $this->{$this->_model_name}->get_ref_table('ref_kota');
    $data['ref_negara'] = $this->{$this->_model_name}->get_ref_table('ref_negara');
    $key = false;
    $urai2 = [''];
    $data['ref_uraiankompetensi'] = $this->{$this->_model_name}->uraiankompetensiid($key,'6',$urai2);
    $this->load->view($this->_template, $data);
}

public function update()
{		
    $data = $this->get_master($this->_path_page.'form');	
    $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
    $data['scripts'] = [$this->_path_js.'/'.$this->_controller_name];
    $data['save_url'] = base_url($this->_controller_name.'/save').'/';	
    $data['status_page'] = 'Update';
    $key = ['aktivitasId'=>$keyS];
    $data['datas'] = $this->{$this->_model_name}->by_id($key);
    $data['ref_jenjang'] = $this->{$this->_model_name}->get_ref_table('ref_jenjang');
    $data['ref_kota'] = $this->{$this->_model_name}->get_ref_table('ref_kota');
    $data['ref_negara'] = $this->{$this->_model_name}->get_ref_table('ref_negara');
    $urai2 = [''];
    $data['ref_uraiankompetensi'] = $this->{$this->_model_name}->uraiankompetensiid($keyS,'6',$urai2);
    $this->load->view($this->_template, $data);
}

public function save()
{		
    $fpfIdOld = $this->input->post('fpfIdOld');
    $this->form_validation->set_rules('aktivitasKota','aktivitasKota','trim|xss_clean|required');
    $this->form_validation->set_rules('aktivitasNegara','aktivitasNegara','trim|xss_clean|required');
    $this->form_validation->set_rules('aktivitasNama','aktivitasNama','trim|xss_clean|required');
    $this->form_validation->set_rules('aktivitasTahunAwal','aktivitasTahunAwal','trim|xss_clean|required');
    $this->form_validation->set_rules('aktivitasJumlah','aktivitasJumlah','trim|xss_clean|required');
    $this->form_validation->set_rules('aktivitasUraian','aktivitasUraian','trim|xss_clean|required');
    $this->form_validation->set_rules('aktivitasTahunlulus','aktivitasTahunlulus','trim|xss_clean|required');
    $this->form_validation->set_rules('cekKompetensi[]','Modul','trim|required|xss_clean');



    if($this->form_validation->run()) 
    {	
        if(IS_AJAX)
        {


            $cekKompetensi = $this->input->post('cekKompetensi');
            $aktivitasLembaga = $this->input->post('aktivitasLembaga');
            $aktivitasKota = $this->input->post('aktivitasKota');
            $aktivitasNegara = $this->input->post('aktivitasNegara');
            $aktivitasFdpId = $this->input->post('fpfFdpId');
            $aktivitasNama = $this->input->post('aktivitasNama');
            $aktivitasJumlah = $this->input->post('aktivitasJumlah');
            $aktivitasTahunAwal = $this->input->post('aktivitasTahunAwal');
            $aktivitasTahunAkhir = $this->input->post('aktivitasTahunAkhir');
            $aktifitasJudulTa = $this->input->post('aktifitasJudulTa');
            $aktivitasUraian = $this->input->post('aktivitasUraian');
            $aktivitasNilai = $this->input->post('aktivitasNilai');
            $aktivitasPeriode = $this->input->post('aktivitasPeriode');
            $aktivitasJudulTa = $this->input->post('aktivitasJudulTa');
            $aktivitasTahunlulus = $this->input->post('aktivitasTahunlulus');


            $param = array(
                'aktivitasLembaga'=>$aktivitasLembaga,
                'aktivitasKota'=>$aktivitasKota,
                'aktivitasNegara'=>$aktivitasNegara,
                'aktivitasFdpId'=>$aktivitasFdpId,
                'aktivitasNama'=>$aktivitasNama,
                'aktivitasJumlah'=>$aktivitasJumlah,
                'aktivitasTahunAwal'=>$aktivitasTahunAwal,
                'aktivitasTahunAkhir'=>$aktivitasTahunAkhir,
                'aktivitasJudulTa'=>$aktivitasJudulTa,
                'aktivitasUraian'=>$aktivitasUraian,
                'aktivitasNilai'=>$aktivitasNilai,
                'aktivitasPeriode'=>$aktivitasPeriode,
                'aktivitasIdForm'=>'I.2',
                'aktivitasTahunlulus'=>$aktivitasTahunlulus,
                'aktivitasTahunSemester'=> date('Y')

            );

            if(empty($fpfIdOld) and (empty($_FILES['uploadfile']['name']) ))
            {
                message('Maaf.. silahkan Upload Berkas dalam bentuk PDF', 'error');
                exit(); 
            } else
            {
                $date_awal = date('Y-m-d i-s');
                if(!empty($_FILES['uploadfile']['name']))
                {
                    $uploadfilename = 'Pendidikan_'.$aktivitasFdpId.'_'.$date_awal;
                    $konfig = array(
                        'url' => FCPATH .'public/assets/berkas/',
                        'type' => 'pdf',
                        'size' => 0.7*1024,
                        'namafile' => $uploadfilename
                    );

                    $this->load->library('UploadArsip');
                    $file_name = $this->uploadarsip->arsip($konfig, 'uploadfile');
                    if (!empty($file_name))
                        $param['aktivitasBerkas'] = $file_name;

                }
            }

            if(count($cekKompetensi)<6)
            {
                if(empty($fpfIdOld))
                {

                    $proses = $this->{$this->_model_name}->insert('tb_aktivitas',$param);
                    $cekaktivitasave = $this->{$this->_model_name}->cekaktivitas($aktivitasFdpId,$aktivitasJudulTa,$file_name);
                    foreach ($cekKompetensi as $modul) { 

                        $paramkompetensi = array(
                            'kompetensiAktivitasId'=>$cekaktivitasave->aktivitasId,
                            'kompetensiUraianKompetensiId'=>$modul,
                            'kompetensiFdpId' =>$cekaktivitasave->aktivitasFdpId,

                        );   

                        $this->{$this->_model_name}->insert('tb_kompetensi',$paramkompetensi);
                    }
                } else {
                    $key = array('aktivitasId'=>$fpfIdOld);
                    $proses = $this->{$this->_model_name}->update('tb_aktivitas',$param,$key);
                    $this->{$this->_model_name}->delete('tb_kompetensi',array('kompetensiAktivitasId'=>$fpfIdOld));

                    foreach ($cekKompetensi as $modul) { 
                        $paramkompetensi = array(
                            'kompetensiAktivitasId'=>$fpfIdOld,
                            'kompetensiUraianKompetensiId'=>$modul,
                            'kompetensiFdpId' =>$aktivitasFdpId,

                        ); 
                        $this->{$this->_model_name}->insert('tb_kompetensi',$paramkompetensi);
                    }
                }
            } else {message('Maaf.. Uraian tidak boleh lebih dari 5', 'error'); } 


            if($proses)
                message($this->_judul.' Berhasil Disimpan','success');
            else
            {
                $error = $this->db->error();
                message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
            }
        }
    } else { message('Ooops!! Something Wrong!! '.validation_errors(),'error'); }
}

public function delete()
{
    $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
    $key = ['aktivitasId'=>$keyS];
    $proses = $this->{$this->_model_name}->delete('tb_aktivitas',$key);
    if ($proses) 
        message($this->_judul.' Berhasil Dihapus','success');
    else
    {
        $error = $this->db->error();
        message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
    }
}
}
