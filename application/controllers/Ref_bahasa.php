<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class ref_bahasa extends MY_Controller {
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/ref_bahasa/';
        $this->_path_js = null;
        $this->_judul = 'Ref_bahasa';
        $this->_controller_name = 'ref_bahasa';
        $this->_model_name = 'model_ref_bahasa';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name,'',TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page.$this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('ref_bahasa');
        $data['create_url'] = base_url($this->_controller_name.'/create').'/';
        $data['update_url'] = base_url($this->_controller_name.'/update').'/';
        $data['delete_url'] = base_url($this->_controller_name.'/delete').'/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {	
        $data = $this->get_master($this->_path_page.'form');	
        $data['scripts'] = [];	
        $data['save_url'] = base_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        
        $this->load->view($this->_template, $data);
    }

    public function update()
    {		
        $data = $this->get_master($this->_path_page.'form');	
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $data['scripts'] = [];	
        $data['save_url'] = base_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Update';
        $key = ['bahasaId'=>$keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('ref_bahasa',$key);
        
        $this->load->view($this->_template, $data);
    }

    public function save()
    {		
        $bahasaIdOld = $this->input->post('bahasaIdOld');$this->form_validation->set_rules('bahasaNama','bahasaNama','trim|xss_clean');

        if($this->form_validation->run()) 
        {	
            if(IS_AJAX)
            {
                $bahasaNama = $this->input->post('bahasaNama');
                

                $param = array(
                    'bahasaNama'=>$bahasaNama,

                );

                if(empty($bahasaIdOld))
                {
                    $proses = $this->{$this->_model_name}->insert('ref_bahasa',$param);
                } else {
                    $key = array('bahasaId'=>$bahasaIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_bahasa',$param,$key);
                }

                if($proses)
                    message($this->_judul.' Berhasil Disimpan','success');
                else
                {
                    $error = $this->db->error();
                    message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! '.validation_errors(),'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $key = ['bahasaId'=>$keyS];
        $proses = $this->{$this->_model_name}->delete('ref_bahasa',$key);
        if ($proses) 
            message($this->_judul.' Berhasil Dihapus','success');
        else
        {
            $error = $this->db->error();
            message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
        }
    }
}
