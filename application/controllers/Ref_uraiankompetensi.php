<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class ref_uraiankompetensi extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/ref_uraiankompetensi/';
        $this->_path_js = null;
        $this->_judul = 'Uraian Kompetensi';
        $this->_controller_name = 'ref_uraiankompetensi';
        $this->_model_name = 'model_ref_uraiankompetensi';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name, '', TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page . $this->_page_index);
        $data['scripts'] = [];
        $data['datas'] = $this->{$this->_model_name}->all();
        $data['create_url'] = base_url($this->_controller_name . '/create') . '/';
        $data['update_url'] = base_url($this->_controller_name . '/update') . '/';
        $data['delete_url'] = base_url($this->_controller_name . '/delete') . '/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $data['scripts'] = [];
        $data['save_url'] = base_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Create';
        $data['datas'] = false;
        $data['ref_elemenkompetensi'] = $this->{$this->_model_name}->get_ref_table('ref_elemenkompetensi');

        $this->load->view($this->_template, $data);
    }

    public function update()
    {
        $data = $this->get_master($this->_path_page . 'form');
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $data['scripts'] = [];
        $data['save_url'] = base_url($this->_controller_name . '/save') . '/';
        $data['status_page'] = 'Update';
        $key = ['uraianKompetensiId' => $keyS];
        $data['datas'] = $this->{$this->_model_name}->by_id($key);
        $data['ref_elemenkompetensi'] = $this->{$this->_model_name}->get_ref_table('ref_elemenkompetensi');

        $this->load->view($this->_template, $data);
    }

    public function save()
    {
        $uraianKompetensiIdOld = $this->input->post('uraianKompetensiIdOld');
        $this->form_validation->set_rules('uraianelemenKompetensiId', 'uraianelemenKompetensiId', 'trim|xss_clean');
        $this->form_validation->set_rules('uraianKompetensiNama', 'uraianKompetensiNama', 'trim|xss_clean');
        $this->form_validation->set_rules('uraianKompetensiKet', 'uraianKompetensiKet', 'trim|xss_clean');

        if ($this->form_validation->run()) {
            if (IS_AJAX) {
                $uraianelemenKompetensiId = $this->input->post('uraianelemenKompetensiId');
                $uraianKompetensiNama = $this->input->post('uraianKompetensiNama');
                $uraianKompetensiKet = $this->input->post('uraianKompetensiKet');


                $param = array(
                    'uraianelemenKompetensiId' => $uraianelemenKompetensiId,
                    'uraianKompetensiNama' => $uraianKompetensiNama,
                    'uraianKompetensiKet' => $uraianKompetensiKet,

                );

                if (empty($uraianKompetensiIdOld)) {
                    $proses = $this->{$this->_model_name}->insert('ref_uraiankompetensi', $param);
                } else {
                    $key = array('uraianKompetensiId' => $uraianKompetensiIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_uraiankompetensi', $param, $key);
                }

                if ($proses)
                    message($this->_judul . ' Berhasil Disimpan', 'success');
                else {
                    $error = $this->db->error();
                    message($this->_judul . ' Gagal Disimpan, ' . $error['code'] . ': ' . $error['message'], 'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! ' . validation_errors(), 'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3), $this->config->item('encryption_key'));
        $key = ['uraianKompetensiId' => $keyS];
        $proses = $this->{$this->_model_name}->delete('ref_uraiankompetensi', $key);
        if ($proses)
            message($this->_judul . ' Berhasil Dihapus', 'success');
        else {
            $error = $this->db->error();
            message($this->_judul . ' Gagal Dihapus, ' . $error['code'] . ': ' . $error['message'], 'error');
        }
    }
}