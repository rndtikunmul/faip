<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('IS_AJAX', isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');

class ref_tanggal extends MY_Controller {
    function __construct()
    {
        parent::__construct();

        $this->_template = 'layouts/template';
        $this->_path_page = 'pages/ref_tanggal/';
        $this->_path_js = 'user';
        $this->_judul = 'Tanggal Form Penilaian';
        $this->_controller_name = 'ref_tanggal';
        $this->_model_name = 'model_ref_tanggal';
        $this->_page_index = 'index';

        $this->load->model($this->_model_name,'',TRUE);
    }

    public function index()
    {
        $data = $this->get_master($this->_path_page.$this->_page_index);
        $this->load->helper('tgl_indo');
        $data['scripts'] =  [$this->_path_js.'/'.$this->_controller_name];
        $data['datas'] = $this->{$this->_model_name}->get_ref_table('ref_tanggal');
        $data['create_url'] = site_url($this->_controller_name.'/create').'/';
        $data['update_url'] = site_url($this->_controller_name.'/update').'/';
        $data['delete_url'] = site_url($this->_controller_name.'/delete').'/';
        $this->load->view($this->_template, $data);
    }

    public function create()
    {	
        $data = $this->get_master($this->_path_page.'form');	
        $data['scripts'] =  [$this->_path_js.'/'.$this->_controller_name];	
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Create';
        $data['datas'] = false;

        $this->load->view($this->_template, $data);
    }

    public function update()
    {		
        $data = $this->get_master($this->_path_page.'form');	
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $data['scripts'] =  [$this->_path_js.'/'.$this->_controller_name];
        $data['save_url'] = site_url($this->_controller_name.'/save').'/';	
        $data['status_page'] = 'Update';
        $key = ['tanggalId'=>$keyS];
        $data['datas'] = $this->{$this->_model_name}->get_by_id('ref_tanggal',$key);

        $this->load->view($this->_template, $data);
    }

    public function save()
    {		
        $tanggalIdOld = $this->input->post('tanggalIdOld');$this->form_validation->set_rules('tanggal','tanggal','trim|xss_clean');
        $this->form_validation->set_rules('tanggalKet','tanggalKet','trim|xss_clean');

        if($this->form_validation->run()) 
        {	
            if(IS_AJAX)
            {
                $tanggal = $this->input->post('tanggal');
                $tanggalKet = $this->input->post('tanggalKet');


                $param = array(
                    'tanggal'=>$tanggal,
                    'tanggalKet'=>$tanggalKet,

                );

                if(empty($tanggalIdOld))
                {
                    $proses = $this->{$this->_model_name}->insert('ref_tanggal',$param);
                } else {
                    $key = array('tanggalId'=>$tanggalIdOld);
                    $proses = $this->{$this->_model_name}->update('ref_tanggal',$param,$key);
                }

                if($proses)
                    message($this->_judul.' Berhasil Disimpan','success');
                else
                {
                    $error = $this->db->error();
                    message($this->_judul.' Gagal Disimpan, '.$error['code'].': '.$error['message'],'error');
                }
            }
        } else {
            message('Ooops!! Something Wrong!! '.validation_errors(),'error');
        }
    }

    public function delete()
    {
        $keyS = $this->encryptions->decode($this->uri->segment(3),$this->config->item('encryption_key'));
        $key = ['tanggalId'=>$keyS];
        $proses = $this->{$this->_model_name}->delete('ref_tanggal',$key);
        if ($proses) 
            message($this->_judul.' Berhasil Dihapus','success');
        else
        {
            $error = $this->db->error();
            message($this->_judul.' Gagal Dihapus, '.$error['code'].': '.$error['message'],'error');
        }
    }
}
