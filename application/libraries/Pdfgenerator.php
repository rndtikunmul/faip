<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

use Dompdf\Dompdf;
use Dompdf\Options;
// use Mpdf\Mpdf;
// use Mpdf\Config\ConfigVariables;
// use Mpdf\Config\FontVariables;
class PdfGenerator
{
  public function generate($html,$filename,$orientation,$size='A4')
  { 
  	ini_set('max_execution_time', 0);
  	ini_set('memory_limit','256M');
    $options = new Options();
    $options->set('isRemoteEnabled', true);
    $options->set('isHtml5ParserEnabled', true);
    $dompdf = new Dompdf($options);
    $dompdf->loadHtml($html);
    $dompdf->setPaper($size, $orientation);
    $dompdf->render();
    $dompdf->stream($filename.'.pdf',array("Attachment"=>0));	

    //  ob_start();
    //     ini_set('max_execution_time', 0);
    //     ini_set('memory_limit', '402M');
    //     $mpdf = new \Mpdf\Mpdf(['format' => 'A4', 'orientation' => 'L']);
    //     $mpdf->shrink_tables_to_fit = 1;
    //     if (is_array((string) $html)) {
    //         foreach ($html as $content) {
    //             $mpdf->AddPage('L');
    //             $mpdf->WriteHTML((string) $content);
    //         }
    //     } else
    //     ob_clean(); 
    //     error_reporting(E_ALL & ~E_NOTICE);
    //     $mpdf->WriteHTML((string) $html);
    //     $mpdf->Output($filename . '.pdf', 'I');
  }

}