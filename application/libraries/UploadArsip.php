<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class UploadArsip
{

	public function arsip($konfig, $input_name = 'berkas')
	{
		$config['upload_path'] = $konfig['url']; //path folder
		$config['allowed_types'] = $konfig['type']; //type yang dapat diakses bisa anda sesuaikan
		$config['max_size'] = $konfig['size']; //maksimum besar file 15M
		$config['file_name'] = $konfig['namafile']; //nama yang terupload nantinya
		$config['overwrite'] = TRUE;
		
		$CI = get_instance();

		if (!file_exists($konfig['url']))
			mkdir($konfig['url'], 0755, true);

		if (!empty($filename)) {
			if (file_exists($konfig['url'] . $konfig['namafile']))
				@unlink($konfig['url'] . $konfig['namafile']);
		}

		$CI->load->library('upload');
		$CI->upload->initialize($config);
		if (!$CI->upload->do_upload($input_name))
			echo message(strip_tags($CI->upload->display_errors()), 'error');
		else {
			$file = $CI->upload->data();
			return $file['file_name'];
		}
	}
}
