<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Aksesapi
{
	public function GetAccesss($userid, $password) {	
	
		$url = 'https://apitelkomsel.unmul.ac.id/member/login';	
		$data = json_encode(
        			array(
        				'userid' => $userid,
        				'password' => $password,
        				'usertype' => 'MHS' 
        				)
        		);

		
		$curl_handle = curl_init();
	    curl_setopt($curl_handle, CURLOPT_URL,$url);
	    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($curl_handle, CURLOPT_POST, 1);
	    curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $data);
	    
	    $buffer = curl_exec($curl_handle);
	    curl_close($curl_handle);
	     
	    $result = json_decode($buffer);
   
	   return  $result ;
	   

		}

		public function Posttoken()
	{
		$url = 'https://osm.unmul.ac.id/auth';
		$data = json_encode(
			array(
				  'username' => 'eirumppi.ft',
           		  'password' => 'lCMGA68NEm'
			)
		);


		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_URL, $url);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl_handle, CURLOPT_POST, 1);
		curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $data);
		curl_setopt_array($curl_handle, array(
			
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			 
			CURLOPT_HTTPHEADER => array(
			'Content-Type: application/json',
			 'apikey: 2jNGzLHbaByROJ9CEx9031HUqWRr3mQ3'
			),
			));

		$buffer = curl_exec($curl_handle);
		curl_close($curl_handle);

		$result = json_decode($buffer);

		return  $result;
	}

	 public function GetAccess($username,$password,$token)
	{
		$url = 'https://osm.unmul.ac.id/login';
		$data = json_encode(
			array(
				  'username' => $username,
           		  'password' => $password,
           		  'usertype'=> 'MHS'
			)
		);


		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_URL, $url);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl_handle, CURLOPT_POST, 1);
		curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $data);
		curl_setopt_array($curl_handle, array(
			
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			 
			CURLOPT_HTTPHEADER => array(
			'Content-Type: application/json',
			 'apikey: 2jNGzLHbaByROJ9CEx9031HUqWRr3mQ3',
			 'Authorization: Bearer '.$token,
			),
			));

		$buffer = curl_exec($curl_handle);
		curl_close($curl_handle);

		$result = json_decode($buffer);

		return  $result;
	}		

		public function GetDosen($userid, $password) {	
	
		$url = 'https://apitelkomsel.unmul.ac.id/member/login';	
		$data = json_encode(
        			array(
        				'userid' => $userid,
        				'password' => $password,
        				'usertype' => 'DSN' 
        				)
        		);

	
		$curl_handle = curl_init();
	    curl_setopt($curl_handle, CURLOPT_URL,$url);
	    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($curl_handle, CURLOPT_POST, 1);
	    curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $data);
	    
	    $buffer = curl_exec($curl_handle);
	    curl_close($curl_handle);
	     
	    $result = json_decode($buffer);
   
	   return  $result ;
	   

		}

	
	public function Getsks($userid, $sessid) {
		$url = 'https://apitelkomsel.unmul.ac.id/academic/value';
		$data = json_encode(
        			array(
        				'id' => $userid,
        				'sessid' => $sessid
        				)
        		);

		
		$curl_handle = curl_init();
	    curl_setopt($curl_handle, CURLOPT_URL,$url);
	    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($curl_handle, CURLOPT_POST, 1);
	    curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $data);
	    curl_setopt($curl_handle, CURLOPT_COOKIE,'ci_session='.$sessid);	
	    curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, FALSE);
     
	    $json_response = curl_exec($curl_handle);
	    curl_close($curl_handle);
		$response = json_decode($json_response);

		return $response;

		}	

	public function loginAkunSidak($susrNama, $password)
			{

		$curl = curl_init();

		$data = 
			array(
				'susrNama' => $susrNama,
				'susrPassword' => $password,
				'RND-API-KEY' => 'simpresmawa'
		);

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://sidak.unmul.ac.id/api/absensi/login/",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $data,
		  CURLOPT_HTTPHEADER => array(
		    "authorization: Basic dGVhbV9ybmQ6Uk5EVW5tdWwyMDIw",
		    "cache-control: no-cache",
		    "content-type: multipart/form-data;",

		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		$result = json_decode($response);

		return  $result;
	}	
}

?>