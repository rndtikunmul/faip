<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Apii
{	

	public function PostLogin($username, $password)
	{
		// $url = 'http://updmember.pii.or.id:9000/api/auth/login';
		$url = 'https://updmember.pii.or.id/api/auth/login';
		$data =
		array(
			'username' => $username,
			'password' => $password		
		);
		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_URL, $url);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl_handle, CURLOPT_POST, 1);
		curl_setopt($curl_handle, CURLOPT_POSTFIELDS, http_build_query($data));

		$buffer = curl_exec($curl_handle);
		curl_close($curl_handle);

		$result = json_decode($buffer);

		return  $result;
	}

	public function GetUserId($token,$field,$email)
	{
		$headers = [
			'content_type' => 'application/x-www-form-urlencoded',
			'Authorization: Bearer '.$token
		];

		// $url = 'http://updmember.pii.or.id:9000/api/main/'.$field;
		$url = 'https://updmember.pii.or.id/api/main/'.$field;
		$data = implode('&',array(
			'email='.$email
		));
		$url = $url.'?'.$data;
		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl_handle, CURLOPT_URL, $url);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
		$buffer = curl_exec($curl_handle);
		curl_close($curl_handle);
		$result = json_decode($buffer);
		return  $result;
	}

	public function Get($token,$field,$kta)
	{
		$headers = [
			'content_type' => 'application/x-www-form-urlencoded',
			'Authorization: Bearer '.$token
		];

		// $url = 'http://updmember.pii.or.id:9000/api/main/'.$field;
		$url = 'https://updmember.pii.or.id/api/main/'.$field;
		$data = implode('&',array(
			'user_id='.$kta
		));
		$url = $url.'?'.$data;
		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl_handle, CURLOPT_URL, $url);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
		$buffer = curl_exec($curl_handle);
		curl_close($curl_handle);
		$result = json_decode($buffer);
		return  $result;
	}

	public function Getfaip($token,$field,$faip_id)
	{
		$headers = [
			'content_type' => 'application/x-www-form-urlencoded',
			'Authorization: Bearer '.$token
		];

		// $url = 'http://updmember.pii.or.id:9000/api/main/'.$field;
		$url = 'https://updmember.pii.or.id/api/main/'.$field;
		$data = implode('&',array(
			'id='.$faip_id
		));
		$url = $url.'?'.$data;
		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl_handle, CURLOPT_URL, $url);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
		$buffer = curl_exec($curl_handle);
		curl_close($curl_handle);
		$result = json_decode($buffer);
		return  $result;
	}


	function Insert($token,$field,$data)
	{
		$headers = [
			'content_type' => 'application/x-www-form-urlencoded',
			'Authorization: Bearer '.$token
		];
		

		// $url = 'http://updmember.pii.or.id:9000/api/main/'.$field;
		$url = 'https://updmember.pii.or.id/api/main/'.$field;
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($curl);
		curl_close($curl);
		return $response;
	}



	

	public function Getfaip_id($token,$field,$kta)
	{
		$headers = [
			'content_type' => 'application/x-www-form-urlencoded',
			'Authorization: Bearer '.$token
		];

		// $url = 'http://updmember.pii.or.id:9000/api/main/'.$field;
		$url = 'https://updmember.pii.or.id/api/main/'.$field;
		$data = implode('&',array(
			'email='.$kta
		));
		$url = $url.'?'.$data;
		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl_handle, CURLOPT_URL, $url);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
		$buffer = curl_exec($curl_handle);
		curl_close($curl_handle);
		$result = json_decode($buffer);
		return  $result;
	}

	public function Getdetail($token,$field,$kta)
	{
		$headers = [
			'content_type' => 'application/x-www-form-urlencoded',
			'Authorization: Bearer '.$token
		];

		// $url = 'http://updmember.pii.or.id:9000/api/main/'.$field;
		$url = 'https://updmember.pii.or.id/api/main/'.$field;
		$data = implode('&',array(
			// 'faip_id='.$faip_id,
			'no_kta='.$kta
		));
		$url = $url.'?'.$data;
		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl_handle, CURLOPT_URL, $url);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
		$buffer = curl_exec($curl_handle);
		curl_close($curl_handle);
		$result = json_decode($buffer);
		return  $result;
	}
	
		public function Updateuser($token,$userID,$data)
	{

		$headers = [
			'content_type' => 'application/x-www-form-urlencoded',
			'Authorization: Bearer '.$token,
			'X-HTTP-Method-Override: PUT'
		];
		// $url = 'http://updmember.pii.or.id:9000/api/main/users/';
		$url = 'https://updmember.pii.or.id/api/main/users/';
		$curl_handle = curl_init($url);
		curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($curl_handle, CURLOPT_POSTFIELDS,http_build_query($data));
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
		$buffer = curl_exec($curl_handle);
		curl_close($curl_handle);
		return  $buffer;
	}


}

?>