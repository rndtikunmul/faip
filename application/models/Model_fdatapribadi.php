
<?php
class Model_fdatapribadi extends Model_Master
{
    protected $table = 'f_datapribadi';


    public function __construct()
    {
        parent::__construct();
    }       
    function all($id,$nim)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_kota','fdpKota = kotaKode','LEFT');
        // $this->db->where($id);
        if(!empty($id))
            $this->db->where("SUBSTRING(fdpNim,1,2) =  SUBSTRING($id,3,2)");
        if($nim == false) {
            $this->db->or_where('fdpNim',$nim); 
        }else{
            $this->db->where('fdpNim',$nim); 
        }
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_kota','fdpTempatLahir = kotaKode','LEFT');
        $this->db->where($id);
        $qr=$this->db->get();
        if($qr->num_rows()==1)
            return $qr->row();
        else
            return false;
    }
    function idsingkron($id)
    {
        $this->db->select('* ,b.kotaNama as birthplace,c.kotaNama as city,d.kotaNama as citywork, cc.propNama as provinsirumah,dd.propNama as provinsiwork');
        $this->db->from($this->table);
        $this->db->join('ref_kota b','fdpTempatLahir = b.kotaKode','LEFT');
        $this->db->join('ref_kota c','fdpKota = c.kotaKode','LEFT');
        $this->db->join('ref_kota d','fdpKotaLembaga = d.kotaKode','LEFT');
        $this->db->join('ref_propinsi cc','c.kotaPropKode = cc.propKode','LEFT');
        $this->db->join('ref_propinsi dd','d.kotaPropKode = dd.propKode','LEFT');

        $this->db->where($id);
        $qr=$this->db->get();
        if($qr->num_rows()==1)
            return $qr->row();
        else
            return false;
    }


    function edu($key)
    {
        $this->db->select('*');
        $this->db->from('tb_aktivitas');
        $this->db->join('ref_jenjang','aktivitasLembaga = jenjangId','LEFT');
        $this->db->join('ref_kota','aktivitasKota = kotaKode','LEFT');
        $this->db->join('ref_negara','aktivitasNegara = negKode','LEFT');
        $this->db->join('ref_propinsi','kotaPropKode = propKode','LEFT');
        $this->db->where('aktivitasIdForm','I.2');
        $this->db->where($key);
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function exp($key)
    {
        $this->db->select('*');
        $this->db->from('tb_aktivitas');
        $this->db->join('ref_kota','aktivitasKota = kotaKode','LEFT');
        $this->db->join('ref_propinsi','kotaPropKode = propKode','LEFT');
        $this->db->join('ref_negara','aktivitasNegara = negKode','LEFT');
        $this->db->where($key);
        $this->db->where("(
            aktivitasIdForm = 'III.1' 
            OR aktivitasIdForm = 'III.3'
            OR aktivitasIdForm = 'III.4'
            OR aktivitasIdForm = 'III.5'   
        )");
        
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function organisasi($key,$form)
    {
        $this->db->select('*');
        $this->db->from('tb_aktivitas');
        $this->db->join('ref_kota','aktivitasKota = kotaKode','LEFT');
        $this->db->join('ref_negara','aktivitasNegara = negKode','LEFT');
        $this->db->join('ref_propinsi','kotaPropKode = propKode','LEFT');
        $this->db->join('ref_bahasa','aktivitasNama = bahasaId','LEFT');
        $this->db->join('ref_lvlatih','aktivitasTahunAwal = lvlatihId','LEFT');
        $this->db->where('aktivitasIdForm',$form);
        $this->db->where($key);
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function kompetensifaip3($key,$aktiv)
    {

        $this->db->select('uraianKompetensiNama');
        $this->db->from('tb_kompetensi');
        $this->db->join('tb_aktivitas','kompetensiAktivitasId = aktivitasId','LEFT');
        $this->db->join('ref_uraiankompetensi','kompetensiUraianKompetensiId = uraianKompetensiId','LEFT');
        $this->db->where('aktivitasId',$aktiv);
        $this->db->where($key);
        $subquery = $this->db->get_compiled_select();

        $this->db->select('GROUP_CONCAT(uraianKompetensiNama, " ")  As komNama');
        $this->db->from('('.$subquery.') datas');

        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }


    function kompetensifaip($key,$form,$aktiv)
    {

        $this->db->select('uraianKompetensiNama');
        $this->db->from('tb_kompetensi');
        $this->db->join('tb_aktivitas','kompetensiAktivitasId = aktivitasId','LEFT');
        $this->db->join('ref_uraiankompetensi','kompetensiUraianKompetensiId = uraianKompetensiId','LEFT');
        $this->db->where('aktivitasIdForm',$form);
        $this->db->where('aktivitasId',$aktiv);
        $this->db->where($key);
        $subquery = $this->db->get_compiled_select();

        $this->db->select('GROUP_CONCAT(uraianKompetensiNama, " ")  As komNama');
        $this->db->from('('.$subquery.') datas');

        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function lampiran($key)
    {
        $this->db->select('*');
        $this->db->from('tb_lampiran');
        $this->db->where('lampiranNim',$key);
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }
    function periode($key)
    {
        $this->db->select('*');
        $this->db->from('tb_aktivitas');
        $this->db->where('aktivitasFdpId',$key);
        $this->db->where('aktivitasIdForm','I.2');
        $this->db->where('aktivitasLembaga','1');
        $qr=$this->db->get();
        if($qr->num_rows()==1)
            return $qr->row();
        else
            return false;
    }


}
