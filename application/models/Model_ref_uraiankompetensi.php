<?php
    class Model_ref_uraiankompetensi extends Model_Master
    {
        protected $table = 'ref_uraiankompetensi';


        public function __construct()
        {
            parent::__construct();
        }       
    function all()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_elemenkompetensi','uraianelemenKompetensiId = elemenKompetensiId','LEFT');
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_elemenkompetensi','uraianelemenKompetensiId = elemenKompetensiId','LEFT');
        $this->db->where($id);
        $qr=$this->db->get();
        if($qr->num_rows()==1)
            return $qr->row();
        else
            return false;
    }



            }
            