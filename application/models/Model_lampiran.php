
<?php
class Model_lampiran extends Model_Master
{
    protected $table = 'tb_lampiran';


    public function __construct()
    {
        parent::__construct();
    }       
    

     function all($id,$nim)
    {
        $this->db->select('*');
        $this->db->from($this->table);
         if(!empty($id))
            $this->db->where("SUBSTRING(lampiranNim,1,2) =  SUBSTRING($id,3,2)");
        if($nim == false) {
            $this->db->or_where('lampiranNim',$nim); 
        }else{
            $this->db->where('lampiranNim',$nim); 
        }
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }


    function bimbingan($nip)
    {
        $this->db->select('*');
        $this->db->from('ref_bimbingan');
        $this->db->join('f_datapribadi','bimbinganFdpId = fdpNim','LEFT');
        $this->db->where('bimbinganNip', $nip);
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($id);
        $qr=$this->db->get();
        if($qr->num_rows()==1)
            return $qr->row();
        else
            return false;
    }

    function cetak($id,$uraian1)
    {
        $this->db->select('*');
        $this->db->from('tb_aktivitas');
        $this->db->join('ref_kota','aktivitasKota = kotaKode','LEFT');
        $this->db->join('ref_negara','aktivitasNegara = negKode','LEFT');
        $this->db->join('ref_lvlatih','aktivitasTahunAwal = lvlatihId','LEFT');
         $this->db->join('ref_bahasa','aktivitasNama = bahasaId','LEFT');
        $this->db->where($id);
        $this->db->where("(aktivitasIdForm = '$uraian1')"); 
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function cetak2($uraian1)
    {
        $this->db->select('*');
        $this->db->from('tb_kompetensi');
        $this->db->join('ref_uraiankompetensi',' uraianKompetensiId = kompetensiUraianKompetensiId','LEFT');
        $this->db->where('kompetensiAktivitasId',$uraian1);
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

 function cetakformulir($id,$uraian1,$uraian2,$uraian3)
    {
        $this->db->select('*');
        $this->db->from('tb_aktivitas');
        $this->db->join('tb_kompetensi','aktivitasId = kompetensiAktivitasId and aktivitasFdpId = kompetensiFdpId','LEFT');
        $this->db->join('ref_uraiankompetensi',' uraianKompetensiId = kompetensiUraianKompetensiId','LEFT');
        $this->db->join('ref_jenjang','aktivitasLembaga = jenjangId','LEFT'); 
        $this->db->join('ref_kota','aktivitasKota = kotaKode','LEFT');
        $this->db->join('ref_negara','aktivitasNegara = negKode','LEFT');

        $this->db->where($id);
        $this->db->where("(aktivitasIdForm = '$uraian1' OR aktivitasIdForm = '$uraian2' OR 
         aktivitasIdForm = '$uraian3')"); 
        $this->db->Group_by('jenjangId');
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function insinyur($t,$id)
    {
        $this->db->select('*,a.kotanama as kota, b.kotanama as tempatlahir, c.kotanama as KotaLembaga');
        $this->db->from($t);
        $this->db->join('ref_kota a','fdpKota = a.kotaKode','LEFT');
        $this->db->join('ref_kota b','fdpTempatLahir = b.kotaKode','LEFT');
        $this->db->join('ref_kota c','fdpKotaLembaga = c.kotaKode','LEFT');
        $this->db->where($id);
        $qr=$this->db->get();
        if($qr->num_rows()==1)
            return $qr->row();
        else
            return false;
    }

     function lampiran($id)
    {
       $this->db->select('*');
        $this->db->from('tb_lampiran');
        $this->db->where($id);
        $qr=$this->db->get();
        if($qr->num_rows()==1)
            return $qr->row();
        else
            return false;
    }

    




}
