<?php
class Model_tb_kompetensi extends Model_Master
{
    protected $table = 'tb_kompetensi';


    public function __construct()
    {
        parent::__construct();
    }    

  function insinyur($t,$id)
    {
        $this->db->select('*,a.kotanama as kota, b.kotanama as tempatlahir, c.kotanama as KotaLembaga');
        $this->db->from($t);
        $this->db->join('ref_kota a','fdpKota = a.kotaKode','LEFT');
        $this->db->join('ref_kota b','fdpTempatLahir = b.kotaKode','LEFT');
        $this->db->join('ref_kota c','fdpKotaLembaga = c.kotaKode','LEFT');
        $this->db->where($id);
        $qr=$this->db->get();
        if($qr->num_rows()==1)
            return $qr->row();
        else
            return false;
    }

    
    function bimbingan($nip)
    {
        $this->db->select('*');
        $this->db->from('ref_bimbingan');
        $this->db->join('f_datapribadi','bimbinganFdpId = fdpNim','LEFT');
        $this->db->where('bimbinganNip', $nip);
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    } 
     function ttd($fdpNim)
    {
        $this->db->select('susrProfil,susrNama');
        $this->db->from('f_datapribadi');
        $this->db->join('ref_bimbingan',' fdpNim = bimbinganFdpId','LEFT');
        $this->db->join('s_user',' bimbinganNip = susrProfil','LEFT');
        $this->db->where('fdpNim', $fdpNim);
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->row();
        else
            return false;
    } 
       
    function all()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('tb_aktivitas','kompetensiAktivitasId = aktivitasId','LEFT');
        $this->db->join('ref_uraiankompetensi','kompetensiUraianKompetensiId = uraianKompetensiId','LEFT');
        $this->db->limit(5);
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function nilai($id,$uraian1)
    {
        $this->db->select('*');
        $this->db->from('tb_aktivitas');
        $this->db->join('tb_kompetensi','aktivitasId = kompetensiAktivitasId and aktivitasFdpId = kompetensiFdpId','LEFT');
        $this->db->join('ref_uraiankompetensi',' uraianKompetensiId = kompetensiUraianKompetensiId','LEFT');
        $this->db->join('ref_jenjang','aktivitasLembaga = jenjangId','LEFT'); 
        $this->db->join('ref_kota','aktivitasKota = kotaKode','LEFT');
        $this->db->join('ref_negara','aktivitasNegara = negKode','LEFT');
        $this->db->join('ref_bahasa','aktivitasNama = bahasaId','LEFT');
        $this->db->where($id);
        $this->db->where("(aktivitasIdForm = '$uraian1')"); 
        $this->db->where("kompetensiId IS NOT NULL"); 
        $this->db->order_by('aktivitasIdForm');
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function penilai($id,$uraian1,$uraian2,$uraian3)
    {
        $this->db->select('*');
        $this->db->from('tb_aktivitas');
        $this->db->join('tb_kompetensi','aktivitasId = kompetensiAktivitasId and aktivitasFdpId = kompetensiFdpId','LEFT');
        $this->db->join('ref_uraiankompetensi',' uraianKompetensiId = kompetensiUraianKompetensiId','LEFT');
        

        $this->db->where($id);
        $this->db->where("(aktivitasIdForm = '$uraian1' OR aktivitasIdForm = '$uraian2' OR 
         aktivitasIdForm = '$uraian3')"); 
        $this->db->where("kompetensiId IS NOT NULL"); 
        $this->db->order_by('aktivitasIdForm');
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function cetakformulir($id,$uraian1,$uraian2,$uraian3)
    {
        $this->db->select('*');
        $this->db->from('tb_aktivitas');
        $this->db->join('tb_kompetensi','aktivitasId = kompetensiAktivitasId and aktivitasFdpId = kompetensiFdpId','LEFT');
        $this->db->join('ref_uraiankompetensi',' uraianKompetensiId = kompetensiUraianKompetensiId','LEFT');
        $this->db->join('ref_jenjang','aktivitasLembaga = jenjangId','LEFT'); 
        $this->db->join('ref_kota','aktivitasKota = kotaKode','LEFT');
        $this->db->join('ref_negara','aktivitasNegara = negKode','LEFT');

        $this->db->where($id);
        $this->db->where("(aktivitasIdForm = '$uraian1' OR aktivitasIdForm = '$uraian2' OR 
         aktivitasIdForm = '$uraian3')"); 
        $this->db->Group_by('jenjangId');
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }


    function cetakformulir2($id,$uraian1)
    {
        $this->db->select('*');
        $this->db->from('tb_aktivitas');
        $this->db->join('tb_kompetensi','aktivitasId = kompetensiAktivitasId and aktivitasFdpId = kompetensiFdpId','LEFT');
        $this->db->join('ref_uraiankompetensi',' uraianKompetensiId = kompetensiUraianKompetensiId','LEFT');
        $this->db->join('ref_kota','aktivitasKota = kotaKode','LEFT');
        $this->db->join('ref_negara','aktivitasNegara = negKode','LEFT');
        $this->db->where($id);
        $this->db->where("(aktivitasIdForm = '$uraian1')"); 
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }


    function cetak($id,$uraian1)
    {
        $this->db->select('*');
        $this->db->from('tb_aktivitas');
        $this->db->join('ref_kota','aktivitasKota = kotaKode','LEFT');
        $this->db->join('ref_negara','aktivitasNegara = negKode','LEFT');
        $this->db->join('ref_lvlatih','aktivitasTahunAwal = lvlatihId','LEFT');
         $this->db->join('ref_bahasa','aktivitasNama = bahasaId','LEFT');
        $this->db->where($id);
        $this->db->where("(aktivitasIdForm = '$uraian1')"); 
        $this->db->limit(25);
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function cetak2($uraian1)
    {
        $this->db->select('*');
        $this->db->from('tb_kompetensi');
        $this->db->join('ref_uraiankompetensi',' uraianKompetensiId = kompetensiUraianKompetensiId','LEFT');
        $this->db->where('kompetensiAktivitasId',$uraian1);
        
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }


    function penilaiprofesionalisme($id)
    {
        $this->db->select('*');
        $this->db->from('tb_aktivitas');
        $this->db->join('tb_kompetensi','aktivitasId = kompetensiAktivitasId and aktivitasFdpId = kompetensiFdpId','LEFT');
        $this->db->join('ref_uraiankompetensi',' uraianKompetensiId = kompetensiUraianKompetensiId','LEFT');
        $this->db->where($id);
        $this->db->where("(aktivitasIdForm = 'I.2' OR aktivitasIdForm = 'I.5' OR 
        ( aktivitasIdForm = 'III.1' AND uraianelemenKompetensiId = '6')  
        OR (aktivitasIdForm = 'IV.3' AND uraianelemenKompetensiId = '6'))");
        $this->db->where("kompetensiId IS NOT NULL"); 
      
        $this->db->order_by('aktivitasIdForm');
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function penilaisk($id)
    {
        $this->db->select('*');
        $this->db->from('tb_aktivitas');
        $this->db->join('tb_kompetensi','aktivitasId = kompetensiAktivitasId and aktivitasFdpId = kompetensiFdpId','LEFT');
        $this->db->join('ref_uraiankompetensi',' uraianKompetensiId = kompetensiUraianKompetensiId','LEFT');
        $this->db->where($id);
        $this->db->where("(aktivitasIdForm = 'III.2' OR aktivitasIdForm = 'III.3' OR aktivitasIdForm = 'III.4' OR aktivitasIdForm = 'III.5' OR aktivitasIdForm = 'IV.4' OR 
        ( aktivitasIdForm = 'IV.1' AND uraianelemenKompetensiId NOT IN ('23','24'))
        OR 
        ( aktivitasIdForm = 'IV.2' AND uraianelemenKompetensiId NOT IN ('23','24'))
        OR 
        ( aktivitasIdForm = 'IV.3' AND uraianelemenKompetensiId NOT IN ('6'))

        OR (aktivitasIdForm = 'III.1' AND uraianelemenKompetensiId NOT IN ('6')))");
        $this->db->where("kompetensiId IS NOT NULL"); 
      
        $this->db->order_by('aktivitasIdForm');
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }


    function all_id($id)
    {
        $this->db->select('*, (kompetensiNilaiQ * kompetensiNilaiR) AS bobotipk');
        $this->db->from($this->table);
        $this->db->join('tb_aktivitas','kompetensiAktivitasId = aktivitasId','LEFT');
        $this->db->join('ref_uraiankompetensi','kompetensiUraianKompetensiId = uraianKompetensiId','LEFT');
        $this->db->where($id);
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('tb_aktivitas','kompetensiAktivitasId = aktivitasId','LEFT');
        $this->db->join('ref_uraiankompetensi','kompetensiUraianKompetensiId = uraianKompetensiId','LEFT');
        $this->db->join('tb_lampiran','aktivitasFdpId = lampiranNim','LEFT');
      
        $this->db->where($id);
        $qr=$this->db->get();
        if($qr->num_rows()==1)
            return $qr->row();
        else
            return false;
    }

     function lampiran($id)
    {
       $this->db->select('*');
        $this->db->from('tb_lampiran');
        $this->db->where($id);
        $qr=$this->db->get();
        if($qr->num_rows()==1)
            return $qr->row();
        else
            return false;
    }

    

    function total($id)
    {
        $hsl=$this->db->query(" SELECT 

            aktivitasFdpId, matakuliah,
            CASE
            WHEN matakuliah = 'Kode Etik' THEN '2'
            WHEN matakuliah = 'Profesionalisme' THEN '2'
            WHEN matakuliah = 'K3LH' THEN '2'
            WHEN matakuliah = 'Seminar' THEN '2'
            WHEN matakuliah = 'Studi Kasus & Praktek' THEN '16'
            ELSE 'Tidak Ada matkul'
            END AS sks ,
            W1, W2,W3,W4,P,nilaiip,

            CASE
            WHEN nilaiip >= '10' THEN 'A' 
            WHEN nilaiip >= '2' THEN 'B' 
            WHEN nilaiip >= '1' THEN 'B'     
            ELSE ''
            END AS nilaihuruf,
            CASE
            WHEN nilaiip >= '10' THEN '4' 
            WHEN nilaiip >= '5' THEN '3.5'  
            WHEN nilaiip >= '1' THEN '3'
            ELSE '0'
            END AS nilaiangka ,nilairpl

            FROM (
            SELECT 
            aktivitasFdpId, IF(aktivitasIdForm = 'II.1' OR 'I.3'  OR 'I.4' ,'Kode Etik' ,'Kode Etik') AS matakuliah
            ,SUM(kompetensiNilaiT) AS W1 , SUM(0) AS W2, SUM(0) AS W3, SUM(0) AS W4,SUM(0) AS P, 
            (SUM(( kompetensiNilaiQ * kompetensiNilaiR )) / 

                (
                SELECT COUNT(kompetensiNilaiT) AS jumlah FROM tb_kompetensi 
                LEFT JOIN `tb_aktivitas` ON `aktivitasId` = `kompetensiAktivitasId` 
                WHERE `aktivitasFdpId` = '$id' AND (kompetensiNilaiT NOT IN (' ')) 
                AND (kompetensiNilaiT NOT IN ('0')) AND (`aktivitasIdForm` = 'II.1'  OR `aktivitasIdForm` = 'I.3' OR `aktivitasIdForm` = 'I.4' )
                  ) 

             ) nilaiip,SUM(kompetensiNilaiT) AS nilairpl

            FROM
            `tb_aktivitas`  
            LEFT JOIN `tb_kompetensi` 
            ON `kompetensiAktivitasId` = `aktivitasId` 
            LEFT JOIN `ref_uraiankompetensi` 
            ON `kompetensiUraianKompetensiId` = `uraianKompetensiId` 
            WHERE `aktivitasFdpId` = '$id' 
            AND (`aktivitasIdForm` = 'II.1' 
            OR `aktivitasIdForm` = 'I.3' 
            OR `aktivitasIdForm` = 'I.4' )
            UNION

            SELECT 
            aktivitasFdpId, IF(aktivitasIdForm = 'I.2'  OR 'I.5' OR 'IV.3' OR 'III.1','Profesionalisme' ,'Profesionalisme') AS matakuliah ,
            SUM(0) AS W1, SUM(kompetensiNilaiT) AS  W2, SUM(0) AS W3, SUM(0) AS W4,SUM(0) AS P,
            (SUM(( kompetensiNilaiQ * kompetensiNilaiR )) /

            (SELECT COUNT(kompetensiNilaiT) AS jumlah
                FROM tb_kompetensi
                 LEFT JOIN `tb_aktivitas`  ON `aktivitasId` = `kompetensiAktivitasId` 
                 LEFT JOIN `ref_uraiankompetensi` ON `kompetensiUraianKompetensiId` = `uraianKompetensiId` 
                  WHERE `aktivitasFdpId` = '$id' 
                  AND (kompetensiNilaiT NOT IN (' ')) 
                  AND (kompetensiNilaiT NOT IN ('0'))
                  AND (`aktivitasIdForm` = 'I.2'  OR `aktivitasIdForm` = 'I.5' OR (`aktivitasIdForm` = 'IV.3' AND uraianelemenKompetensiId = '6') OR (`aktivitasIdForm` = 'III.1'  AND uraianelemenKompetensiId = '6' ) )
            )  

            ) nilaiip,SUM(kompetensiNilaiT) AS nilairpl

            FROM
            `tb_aktivitas`  
            LEFT JOIN `tb_kompetensi` 
            ON `kompetensiAktivitasId` = `aktivitasId` 
            LEFT JOIN `ref_uraiankompetensi` 
            ON `kompetensiUraianKompetensiId` = `uraianKompetensiId` 
            WHERE `aktivitasFdpId` = '$id' 
            AND ( `aktivitasIdForm` = 'I.2' 
            OR `aktivitasIdForm` = 'I.5' 
            OR (`aktivitasIdForm` = 'IV.3' 
            AND uraianelemenKompetensiId = '6')
            OR (`aktivitasIdForm` = 'III.1' 
            AND uraianelemenKompetensiId = '6'))

            UNION

            SELECT 
            aktivitasFdpId,  IF(aktivitasIdForm = 'II.2' ,'K3LH' ,'K3LH' ) AS matakuliah ,SUM(kompetensiNilaiT) AS W1,SUM(0) AS W2,SUM(0) AS  W3,SUM(0) AS W4,SUM(0) AS P, 
            (SUM(( kompetensiNilaiQ * kompetensiNilaiR )) / 

            (SELECT COUNT(kompetensiNilaiT) AS jumlah FROM tb_kompetensi
             LEFT JOIN `tb_aktivitas` ON `aktivitasId` = `kompetensiAktivitasId` 
             WHERE `aktivitasFdpId` = '$id'   AND (kompetensiNilaiT NOT IN (' ')) 
             AND (kompetensiNilaiT NOT IN ('0')) AND `aktivitasIdForm` = 'II.2') 

             ) nilaiip,SUM(kompetensiNilaiT) AS nilairpl

            FROM
            `tb_aktivitas`  
            LEFT JOIN `tb_kompetensi` 
            ON `kompetensiAktivitasId` = `aktivitasId` 
            LEFT JOIN `ref_uraiankompetensi` 
            ON `kompetensiUraianKompetensiId` = `uraianKompetensiId` 
            WHERE `aktivitasFdpId` = '$id' 
            AND `aktivitasIdForm` = 'II.2'

            UNION

            SELECT 
            aktivitasFdpId, IF(aktivitasIdForm = 'IV.1'  OR 'IV.2'  OR 'V' ,'Seminar' ,'Seminar' ) AS matakuliah ,
            SUM(0) AS W1,SUM(0) AS W2,SUM(0) AS W3,  SUM(kompetensiNilaiT) AS  W4, SUM(0) AS P, 
            (SUM(( kompetensiNilaiQ * kompetensiNilaiR )) /


          (SELECT COUNT(kompetensiNilaiT) AS jumlah FROM tb_kompetensi 
              LEFT JOIN `tb_aktivitas` ON `aktivitasId` = `kompetensiAktivitasId` 
              LEFT JOIN `ref_uraiankompetensi` ON `kompetensiUraianKompetensiId` = `uraianKompetensiId`

               WHERE `aktivitasFdpId` = '$id' 
              AND (kompetensiNilaiT NOT IN (' '))  AND (kompetensiNilaiT NOT IN ('0'))
              AND ( 
                     (`aktivitasIdForm` = 'IV.1' AND (uraianelemenKompetensiId = '23' OR  uraianelemenKompetensiId = '24') )
                 OR  (`aktivitasIdForm` = 'IV.2' AND (uraianelemenKompetensiId = '23' OR  uraianelemenKompetensiId = '24') )
                 OR `aktivitasIdForm` = 'V' ) 
             )

             ) nilaiip,SUM(kompetensiNilaiT) AS nilairpl

            FROM
            `tb_aktivitas`  
            LEFT JOIN `tb_kompetensi` 
            ON `kompetensiAktivitasId` = `aktivitasId` 
            LEFT JOIN `ref_uraiankompetensi` 
            ON `kompetensiUraianKompetensiId` = `uraianKompetensiId` 
            WHERE `aktivitasFdpId` = '$id' 
            AND (
                    (`aktivitasIdForm` = 'IV.1' AND (uraianelemenKompetensiId = '23' OR  uraianelemenKompetensiId = '24') )
                OR  (`aktivitasIdForm` = 'IV.2' AND (uraianelemenKompetensiId = '23' OR  uraianelemenKompetensiId = '24') )
                OR `aktivitasIdForm` = 'V') 


            UNION
            SELECT 
            aktivitasFdpId, IF(aktivitasIdForm = 'I.6'  OR 'III.1'  OR 'III.2'   OR 'III.3'  OR 'III.4' OR 'III.5'  OR 'IV.1'  OR 'IV.2'  OR 'IV.3' OR 'IV.4' ,'Studi Kasus & Praktek' ,'Studi Kasus & Praktek') AS matakuliah, 

            SUM(IF(unitKompetensiNama = 'W.1.',  kompetensiNilaiT ,0)) AS W1, 
            SUM(IF(unitKompetensiNama = 'W.2.',  kompetensiNilaiT ,0)) AS W2, 
            SUM(IF(unitKompetensiNama = 'W.3.',  kompetensiNilaiT ,0)) AS W3, 
            SUM(IF(unitKompetensiNama = 'W.4.',  kompetensiNilaiT ,0)) AS W4,
            SUM(IF(unitKompetensiId = '5' OR unitKompetensiId = '6' OR unitKompetensiId = '7' OR unitKompetensiId = '8' OR unitKompetensiId = '9' OR unitKompetensiId = '10' OR unitKompetensiId = '11' OR unitKompetensiId = '12',  kompetensiNilaiT ,0)) AS P,
            (SUM(( kompetensiNilaiQ * kompetensiNilaiR )) /

            (SELECT COUNT(kompetensiNilaiT) AS jumlah FROM tb_kompetensi LEFT JOIN `tb_aktivitas` 
              ON `aktivitasId` = `kompetensiAktivitasId`  WHERE `aktivitasFdpId` = '$id' 
              AND (kompetensiNilaiT NOT IN (' '))  AND (kompetensiNilaiT NOT IN ('0'))
              AND (`aktivitasIdForm` = 'I.6' OR `aktivitasIdForm` = 'III.2' OR `aktivitasIdForm` = 'III.3'  OR `aktivitasIdForm` = 'III.4' OR `aktivitasIdForm` = 'III.5' 
                  OR (`aktivitasIdForm` = 'IV.1' AND uraianelemenKompetensiId NOT IN ('23', '24')) 
                  OR (`aktivitasIdForm` = 'IV.2' AND uraianelemenKompetensiId NOT IN ('23', '24')) 
                  OR (`aktivitasIdForm` = 'IV.3' AND uraianelemenKompetensiId NOT IN ('6'))  OR `aktivitasIdForm` = 'IV.4' 
                  OR (`aktivitasIdForm` = 'III.1' AND uraianelemenKompetensiId NOT IN ('6')))
             ) ) nilaiip,SUM(kompetensiNilaiT) AS nilairpl
            FROM
            `tb_aktivitas` 
            LEFT JOIN `tb_kompetensi` 
            ON `kompetensiAktivitasId` = `aktivitasId` 
            LEFT JOIN `ref_uraiankompetensi` 
            ON `kompetensiUraianKompetensiId` = `uraianKompetensiId`    
            LEFT JOIN `ref_elemenkompetensi` 
            ON `uraianelemenKompetensiId` = `elemenKompetensiId` 
            LEFT JOIN `ref_unitkompetensi` 
            ON `elemenunitKompetensiId` = `unitKompetensiId`

            WHERE `aktivitasFdpId` = '$id' 
            AND ( `aktivitasIdForm` = 'I.6' 
            OR `aktivitasIdForm` = 'III.2' 
            OR `aktivitasIdForm` = 'III.3' 
            OR `aktivitasIdForm` = 'III.4'
            OR `aktivitasIdForm` = 'III.5' 
            OR (`aktivitasIdForm` = 'IV.1' AND uraianelemenKompetensiId NOT IN ('23','24'))  
            OR (`aktivitasIdForm` = 'IV.2' AND uraianelemenKompetensiId NOT IN ('23','24')) 
            OR (`aktivitasIdForm` = 'IV.3' AND uraianelemenKompetensiId NOT IN ('6'))
            OR `aktivitasIdForm` = 'IV.4'
            OR (`aktivitasIdForm` = 'III.1' AND uraianelemenKompetensiId NOT IN ('6')))
            ) datas 

            ");

if ($hsl->num_rows() > 0) 
    return $hsl->result();
else
    return false;
}



}
