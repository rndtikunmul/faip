
<?php
class Model_referensikodeetik extends Model_Master
{
    protected $table = 'tb_aktivitas';


    public function __construct()
    {
        parent::__construct();
    }       
    function all($key)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_kota','aktivitasKota = kotaKode','LEFT');
        $this->db->join('ref_negara','aktivitasNegara = negKode','LEFT');
        $this->db->where('aktivitasIdForm','II.1');
        $this->db->where($key);
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function allp3s($key)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_kota','aktivitasKota = kotaKode','LEFT');
        $this->db->join('ref_negara','aktivitasNegara = negKode','LEFT');
        $this->db->where('aktivitasIdForm','II.2');
        $this->db->where($key);
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function bimbingan($nip)
    {
        $this->db->select('*');
        $this->db->from('ref_bimbingan');
        $this->db->join('f_datapribadi','bimbinganFdpId = fdpNim','LEFT');
        $this->db->where('bimbinganNip', $nip);
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_kota','aktivitasKota = kotaKode','LEFT');
        $this->db->join('ref_negara','aktivitasNegara = negKode','LEFT');
        $this->db->where($id);
        $qr=$this->db->get();
        if($qr->num_rows()==1)
            return $qr->row();
        else
            return false;
    }

    function cekaktivitas($aktivitasFdpId,$aktivitasIdForm,$aktivitasBerkas)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('aktivitasFdpId',$aktivitasFdpId);
        $this->db->where('aktivitasIdForm',$aktivitasIdForm);
        $this->db->where('aktivitasTahunakhir',$aktivitasBerkas);
        $qr=$this->db->get();
        if($qr->num_rows()==1)
            return $qr->row();
        else
            return false;
    }


    function uraiankompetensiid($id,$uraian1,$uraian2,$uraian3,$uraian4)
    {
        $this->db->select('*');
        $this->db->from('ref_uraiankompetensi');
        $this->db->join('ref_elemenkompetensi','uraianelemenKompetensiId = elemenKompetensiId','LEFT');
        $this->db->join('ref_unitkompetensi','elemenunitKompetensiId = unitKompetensiId','LEFT');
        $this->db->join('tb_kompetensi',"kompetensiUraianKompetensiId = uraianKompetensiId AND kompetensiAktivitasId ='$id'",'LEFT');
        $this->db->where('uraianelemenKompetensiId',$uraian1);
        $this->db->or_where('uraianelemenKompetensiId',$uraian2);
        $this->db->or_where('uraianelemenKompetensiId',$uraian3);
        $this->db->or_where('uraianelemenKompetensiId',$uraian4);

        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function uraiannilaikompetensiid($id)
    {
        $this->db->select('*');
        $this->db->from('tb_kompetensi');
        $this->db->join('ref_uraiankompetensi',"uraianKompetensiId = kompetensiUraianKompetensiId AND kompetensiAktivitasId ='$id'",'LEFT');
        $this->db->where('kompetensiAktivitasId',$id);

        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

    function by_all($key,$uraian)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('ref_kota','aktivitasKota = kotaKode','LEFT');
        $this->db->join('ref_negara','aktivitasNegara = negKode','LEFT');
        $this->db->join('tb_kompetensi','aktivitasId = kompetensiAktivitasId','LEFT');
        $this->db->join('ref_uraiankompetensi','kompetensiUraianKompetensiId = uraianKompetensiId','LEFT');
        $this->db->where('aktivitasIdForm',$uraian);
        $this->db->where($key);
        $sqr = $this->db->get_compiled_select();
        $this->db->select('*');
        $this->db->from('('.$sqr.') datas');
        $this->db->group_by('kompetensiId',FALSE);
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }




}
