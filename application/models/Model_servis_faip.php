<?php
class Model_servis_faip extends CI_Model
{
    protected $table = 'f_datapribadi';
    public function __construct()
    {
        parent::__construct();
    }       
    function all()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        // $this->db->where('fdpTahunAwal','2020');
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }

     function by_nim($nim)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('fdpNim',$nim);
        $qr=$this->db->get();
        if($qr->num_rows()>0)
            return $qr->result();
        else
            return false;
    }
}