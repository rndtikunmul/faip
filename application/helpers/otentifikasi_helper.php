<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('getUserAPI')) {
    function getUserAPI($username, $password)
    {
        try {

            $client = new Client([
                'base_uri' => 'https://sidak.unmul.ac.id/api/absensi/',
                'auth'  => ['team_rnd', 'RNDUnmul2020']
            ]);
            $data = [
                'susrNama' => $username,
                'susrPassword' => $password,
                'RND-API-KEY' => 'testing'
            ];
            $response = $client->request('POST', 'login', [
                'form_params' => $data
            ]);

            $result = json_decode($response->getBody()->getContents(), true);

            return $result;
        } catch (RequestException $e) {

            $result = ['status'=> FALSE, 'message' => json_decode($e->getResponse()->getBody()->getContents(), true)];

            return $result;
        } catch (\Exception $e) {

            return $result['status'] = FALSE;
        }
    }
}
