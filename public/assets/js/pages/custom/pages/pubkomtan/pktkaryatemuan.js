"use strict";

// Class Definition
var FormCustom = function() {

    var handleSubmit = function(form) {
        $('#response').html('');
        var button = $('#btn_save');
        var button_text = button.text();
        button.prop( "disabled", true );
        button.addClass('disabled');
        button.text('Sedang Memproses...');
        $.ajax({
            type: $(form).attr('method'),
            url: $(form).attr('action'),
            data: $(form).serialize(),
            success: function(data) {
                try {
                    var res = $.parseJSON(data);
                    $('#response').fadeIn('slow').html(res.response);
                    swal.fire({
                        position: "top-right",
                        type: res.status,
                        title: res.message,
                        showConfirmButton: !1,
                        timer: 1500
                    });
                } catch(err)
                {
                    $('#response').fadeIn('slow').html(data);
                }
                button.prop( "disabled", false );
                button.removeClass('disabled');
                button.text(button_text);  
                FormCustom.init();
            }
        })
    }

    var handleSubmitFormShow = function() {
        $("#form_show").validate({
            rules: {
                pegmNIP: {
                    required: true
                }
                
            },
            submitHandler: function(e) {
                handleSubmit(e);
                return false
            }
        });
    }

    var handleSubmitForm = function() {
        $("#form_data").validate({
            rules: {
                 
               aktivitasNama: {
                    required: true
                },
                 
                 aktivitasUraian: {
                    required: true
                },
                 aktivitasTahunAwal: {
                    required: true
                },
                
                 aktivitasLembaga: {
                    required: true
                },
                
            },
            submitHandler: function(form) {
                 var forms = $("#form_data")[0];
                var form_data = new FormData(forms);
                
                
              $('#response').html('');
                var button = $('#btn_save');
                var button_text = button.text();
                button.prop( "disabled", true );
                button.addClass('disabled');
                button.text('Sedang Memproses...');
                $.ajax({
                   type: $("#form_data").attr('method'),
                    url: $("#form_data").attr('action'),
                    processData:false,
                    contentType:false,
                    cache:false,
                    async:false,
                    data: form_data,

                
                    success: function(data) {
                        try {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal.fire({
                                position: "top-right",
                                type: res.status,
                                title: res.message,
                                showConfirmButton: !1,
                                timer: 1500
                            });
                        } catch(err)
                        {
                            $('#response').fadeIn('slow').html(data);
                        }
                        button.prop( "disabled", false );
                        button.removeClass('disabled');
                        button.text(button_text);  
                        FormCustom.init();
                    }
                });
                return false
            }
        });
    }
    
    var handleClickDelete = function() {
        $(".ts_remove_row").click(function(e) {
            e.preventDefault();
            var idLink = '#'+$(this).attr('id');
            
            swal.fire({
                title: "Apakah Anda Yakin Akan Hapus Data?",
                text: "Data Tidak Dapat Dikembalikan!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Hapus!"
            }).then(function(e) {
                e.value && 
                    $.ajax(
                    {
                        url:$(idLink).attr('href'),
                        success:function(data) 
                        {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal.fire({title: "Deleted!", text: res.message, type: res.status}).then(
                                function(){ 
                                    FormCustom.init();
                                }
                            ) ;                   
                        }
                    });
            })    
        });
    }


     var handleClickvalidasi = function() {
        $(".ts_validasi_row").click(function(e) {
            e.preventDefault();
            var idLink = '#'+$(this).attr('id');
            
            swal.fire({
                title: "Apakah Anda Yakin Akan Validasi?",
                text: "Data Tidak Dapat Dikembalikan!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Validasi!"
            }).then(function(e) {
                e.value && 
                    $.ajax(
                    {
                        url:$(idLink).attr('href'),
                        success:function(data) 
                        {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal.fire({title: "Validasi!", text: res.message, type: res.status}).then(
                                function(){ 
                                    location.reload();
                                }
                            ) ;                   
                        }
                    });
            })    
        });
    }

    
    var arrows;
    if (KTUtil.isRTL()) 
    {
        arrows = 
        {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else 
    {
        arrows = 
        {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    
    var tanggal = function () 
    { 
        $('#m_datetimepicker_8,#m_datetimepicker_6,#kt_datetimepicker_2').datepicker(
        {

              todayHighlight: true,
              pickerPosition: 'bottom-left',
              format: "yyyy-mm-dd ",
              rtl: KTUtil.isRTL(),
              todayHighlight: true,
              autoclose: true,
            // orientation: "bottom left",
             templates: arrows
        }); 

         $('#kt_datetimepicker_2_modal,#kt_datepicker_2,#m_datetimepicker_2').datepicker({
            todayHighlight: true,
            autoclose: true,
            pickerPosition: 'bottom-left',
            format: 'yyyy-mm-dd'
        });

          $('#kt_daterangepicker_4').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',

            timePicker: true,
            timePicker24Hour: true,
            timePickerIncrement: 1,
            locale: {
                format: 'YYYY-MM-DD HH:mm:ss'
            }
        }, function(start, end, label) {
            $('#kt_daterangepicker_4 .form-control').val( start.format('YYYY-MM-DD HH:mm:ss') + ' / ' + end.format('YYYY-MM-DD HH:mm:ss'));
        });
        
 
    }
    
    var pilih=function(){
            $(".m-select2").select2({
                placeholder: "Pilih...",
                allowClear: !0
        });
    }

    $(".form-control").change(function() {
            var id = $(".form-control").val();
            $.ajax({
                type:'post',
                url:'/rwjadwal/tanggal_check/'+id,
                data:'id='+id,
                success:function(data)
                {
                    if (data == true) {
                        swal.fire(
                        'Maaf',
                        'Ruangan Sudah Di Gunakan untuk Acara + Persiapan Acara. Silakan Atur Ulang Jadwal :)',
                        'error'
                    );
                    } else {
                        // alert('Ruangan bisa Di Pakai');
                    }                 
                    FormCustom.init();
                }
            });
             
        });

    $("#kt_datepicker_2").change(function() {
            var id = $("#kt_datepicker_2").val();
            $.ajax({
                type:'post',
                url:'/rwjadwal/tanggalselesai_check/'+id,
                data:'id='+id,
                success:function(data)
                {
                    if (data == true) {
                        swal.fire(
                        'Maaf',
                        'Tanggal Selesai Ruangan Sudah Di Gunakan untuk Acara + Persiapan Acara. Silakan Atur Ulang Jadwal :)',
                        'error'
                    );
                    } else {
                        // alert('Ruangan bisa Di Pakai');
                    }                 
                    FormCustom.init();
                }
            });
             
        });
        

    
        
    return {
        // public functions
        init: function() {
            handleSubmitFormShow();
            handleSubmitForm();
            handleClickDelete();
            pilih();
            tanggal();
            handleClickvalidasi();
        }
    };
}();

jQuery(document).ready(function() {
    FormCustom.init()
});