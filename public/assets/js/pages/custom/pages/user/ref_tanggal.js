"use strict";

// Class Definition
var FormCustom = function() {

    var handleSubmit = function(form) {
        $('#response').html('');
        var button = $('#btn_save');
        var button_text = button.text();
        button.prop( "disabled", true );
        button.addClass('disabled');
        button.text('Sedang Memproses...');
        $.ajax({
            type: $(form).attr('method'),
            url: $(form).attr('action'),
            data: $(form).serialize(),
            success: function(data) {
                try {
                    var res = $.parseJSON(data);
                    $('#response').fadeIn('slow').html(res.response);
                    swal.fire({
                        position: "top-right",
                        type: res.status,
                        title: res.message,
                        showConfirmButton: !1,
                        timer: 1500
                    });
                } catch(err)
                {
                    $('#response').fadeIn('slow').html(data);
                }
                button.prop( "disabled", false );
                button.removeClass('disabled');
                button.text(button_text);
                
                FormCustom.init();
            }
        })
    }


var table =  function(){
   var span = 1;
   var prevTD = "";
   var prevTDVal = "";
   $(".myTable tbody tr td:nth-child(2)").each(function() { 
    var $this = $(this);
    if ($this.text() == prevTDVal) {
     span++;
     if (prevTD != "") {
      prevTD.attr("rowspan", span); 
      $this.remove();
    }
  } else {
   prevTD     = $this;
   prevTDVal  = $this.text();
   span       = 1;
 }
});
   $(".myTable tbody tr td:nth-child(1)").each(function() { 
    var $this = $(this);
    if ($this.text() == prevTDVal) {
     span++;
     if (prevTD != "") {
      prevTD.attr("rowspan", span); 
      $this.remove();
    }
  } else {
   prevTD     = $this;
   prevTDVal  = $this.text();
   span       = 1;
 }
});
 };

    var handleSubmitFormShow = function() {
        $("#form_show").validate({
            rules: {
               
                
            },
            submitHandler: function(e) {
                handleSubmit(e);
                return false
            }
        });
    }

    var handleSubmitForm = function() {
        $("#form_custom").validate({
            rules: {
                
            },
            submitHandler: function(e) {
                handleSubmit(e);
                return false
            }
        });
    }
    var handleClickDelete = function() {
        $(".ts_remove_row").click(function(e) {
            e.preventDefault();
            var idLink = '#'+$(this).attr('id');
            
            swal.fire({
                title: "Apakah Anda Yakin Akan Hapus Data?",
                text: "Data Tidak Dapat Dikembalikan!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Hapus!"
            }).then(function(e) {
                e.value && 
                    $.ajax(
                    {
                        url:$(idLink).attr('href'),
                        success:function(data) 
                        {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal.fire({title: "Deleted!", text: res.message, type: res.status}).then(
                                function(){ 
                                    FormCustom.init();
                                }
                            ) ;                   
                        }
                    });
            })    
        });
    }


     var handleClickvalidasi = function() {
        $(".ts_validasi_row").click(function(e) {
            e.preventDefault();
            var idLink = '#'+$(this).attr('id');
            
            swal.fire({
                title: "Apakah Anda Yakin Akan Validasi?",
                text: "Data Tidak Dapat Dikembalikan!!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, Validasi!"
            }).then(function(e) {
                e.value && 
                    $.ajax(
                    {
                        url:$(idLink).attr('href'),
                        success:function(data) 
                        {
                            var res = $.parseJSON(data);
                            $('#response').fadeIn('slow').html(res.response);
                            swal.fire({title: "Validasi!", text: res.message, type: res.status}).then(
                                function(){ 
                                    location.reload();
                                }
                            ) ;                   
                        }
                    });
            })    
        });
    }

    
    var arrows;
    if (KTUtil.isRTL()) 
    {
        arrows = 
        {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else 
    {
        arrows = 
        {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }
    
    var tanggal = function () 
    { 
        
         $('#kt_datetimepicker_2_modal,#kt_datepicker_2,#m_datetimepicker_2').datepicker({
            todayHighlight: true,
            autoclose: true,
            pickerPosition: 'bottom-left',
            format: 'yyyy-mm-dd'
        });        
 
    }
    
    var pilih=function(){
            $(".m-select2").select2({
                placeholder: "Pilih...",
                allowClear: !0
        });
    }

        
    return {
        // public functions
        init: function() {
            handleSubmitFormShow();
            handleSubmitForm();
            handleClickDelete();
            pilih();
            tanggal();
            handleClickvalidasi();
            table(); 
            
        }
    };
}();

jQuery(document).ready(function() {
    FormCustom.init()
});